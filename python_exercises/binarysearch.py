"""Python function to find the element in the list using binary search."""


# %%
def binary_search(lst, v, l1, r):
    """Implement binary search."""
    while(r >= l1):
        middle = (r+l1)//2
        if v == lst[middle]:
            return middle
        elif v < lst[middle]:
            r = middle - 1
        else:
            l1 = middle + 1
    return -1


lst = [2, 3, 4, 5, 40, 60]
binary_search(lst, 40, 0, len(lst))
