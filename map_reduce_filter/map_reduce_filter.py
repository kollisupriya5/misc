"""python functions."""


# %%
def even(n):
    """Even."""
    if n % 2 == 0:
        return n


even(11)


def filter(func, lst):
    """Filter function."""
    res = []
    for i in lst:
        if(func(i)):
            res.append(i)
    return res


filter(even, [1, 2, 3, 4, 5, 6])


# %%
def map(func, lst):
    """Map function."""
    res = []
    for i in lst:
        res.append(func(i))
    return res


def inc(n):
    """increment."""
    return n+1


map(inc, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])


# %%
def reduce(func, x):
    """Reduce function."""
    def reduced(lst):
        result = x
        for e in lst:
            result = func(result, e)
        return result
    return reduced


def add(x, y):
    """Add function."""
    return x + y


reduce(add, 2)([1, 2, 3, 4, 5])
