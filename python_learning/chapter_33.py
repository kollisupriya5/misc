"""Python learning book chapter33."""


# %%
# Default exception handler
def fetcher(obj, index):
    """Exception Handler."""
    return obj[index]


x = 'spam'
fetcher(x, 3)
fetcher(x, 4)

# %%
# Catching exceptions
try:
    fetcher(x, 4)
except IndexError:
    print('got exception')


def catcher():
    """Catch exception."""
    try:
        fetcher(x, 4)
    except IndexError:
        print('got exception')
    print('continuing')


catcher()


# %%
# Raising exceptions
try:
    raise IndexError
except IndexError:
    print('got ecxeption')

raise IndexError

assert False, 'Nobody expects the Spanish Inwuisition'


# %%
# User defined Exceptions

class AlreadyGotOne(Exception):
    """User defined exceptions."""

    pass


def grail():
    """Raise an instance."""
    raise AlreadyGotOne()


try:
    grail()
except AlreadyGotOne:
    print('got exception')


class Carrer(Exception):
    def __str__(self):
        return 'So I become a waiter...'


raise Carrer()

# %%
# Termination actions
try:
    fetcher(x, 3)
finally:
    print('after fetch')


def after():
    try:
        fetcher(x, 3)
    finally:
        print('after fetch')
    print('after try?')


after()


with open('lumberjack.txt', 'w') as file:
    file.write('The larch!\n')
