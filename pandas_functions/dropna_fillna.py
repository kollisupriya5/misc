"""Practicing pandas drop anf filling nan values."""

import numpy as np
import pandas as pd

# DROPNA
# %%
df = (pd.DataFrame({"name": ['Alfred', 'Batman', 'Catwoman'],
                    "toy": [np.nan, 'Batmobile', 'Bullwhip'],
                    "born": [pd.NaT, pd.Timestamp("1940-04-25"), pd.NaT]}))
df
df.dropna()
df.dropna(axis='columns')
df.dropna(how='all')
df.dropna(thresh=2)
df.dropna(subset=['name', 'born'])
df.dropna(inplace=True)

# %%
df1 = (pd.DataFrame([[np.nan, 2, np.nan, 0], [3, 4, np.nan, 1],
                    [np.nan, np.nan, np.nan, 5], [3, 4, np.nan, 1],
                    [3, 4, 0, 1]], columns=list('ABCD')))
df1
df1.dropna()
df1.dropna(how='all')
df1.dropna(how='any')
df1.dropna(axis='columns')
df1.dropna(thresh=2)
df1.dropna(inplace=True)
df1.dropna(subset=['A', 'B'])
df1

# FILLNA
# %%
df1
df1.fillna(0)
df1.fillna(method='ffill')
df1.fillna(method='bfill')
values = {'A': 0, 'B': 1, 'C': 2, 'D': 3}
df1.fillna(value=values)
df1.fillna(value=values, limit=1)

# %%
df
df.fillna(0)
df1.fillna(method='ffill')
df1.fillna(method='bfill')
df
values = {'born': 0, 'name': 1, 'toy': 3}
df.fillna(value=values)
df.fillna(value=values, limit=1)
