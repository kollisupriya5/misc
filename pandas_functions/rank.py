"""Practicing pandas function rank."""


import pandas as pd


# %%
data = ({'name': ['Jason', 'Molly', 'Tina', 'Jake', 'Amy'],
         'year': [2012, 2012, 2013, 2014, 2014],
         'reports': [4, 24, 31, 2, 3],
         'coverage': [25, 94, 70, 62, 70]})
df = (pd.DataFrame(data, index=['Cochice', 'Pima', 'Santa cruz',
                                'maricopa', 'Yuma']))

df['coveragedrank(asc)'] = df['coverage'].rank(ascending=1)
df

df['coverege_rank(desc)'] = df['coverage'].rank(ascending=0)
df

df['mincoveragedrank'] = df['coverage'].rank(ascending=0, method='min')
df

df['maxcoveragedrank'] = df['coverage'].rank(ascending=0, method='max')
df

df['avgcoveragedrank'] = df['coverage'].rank(ascending=0, method='average')
df

df['dense_rank'] = df['coverage'].rank(ascending=0, method='dense')
df

df['reportedranks'] = df['reports'].rank(ascending=True)
df
# %%

df1 = ({'Name': ['Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine',
                 'Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine'],
        'Subject': ['Mathematics', 'Mathematics', 'Mathematics', 'Science',
                    'Science', 'Science', 'History', 'History', 'History',
                    'Economics', 'Economics', 'Economics'],
        'Score': [62, 47, 55, 74, 31, 77, 85, 63, 42, 62, 89, 85]})
df2 = pd.DataFrame(df1, columns=['Name', 'Subject', 'Score'])
df2

df2['scoreranked'] = df2['Score'].rank(ascending=1)
df2

df2['score_ranked'] = df2['Score'].rank(ascending=0)
df2

df2['min_score'] = df2['Score'].rank(ascending=0, method='min')
df2

df2['max_score'] = df2['Score'].rank(ascending=0, method='max')
df2

df2['dense_score'] = df2['Score'].rank(ascending=0, method='dense')
df2

df2['average_score'] = df2['Score'].rank(ascending=0, method='average')
df2

df2['group_rank'] = (df2.groupby('Subject')['Score'].rank(ascending=0,
                                                          method='dense'))
df2
