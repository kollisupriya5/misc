"""Chapter-2 exercises."""

import numpy as np


# %%
def time(n):
    """Time calculation."""
    count = 0
    for i in range(n):
        for j in range(n):
            for k in range(n):
                count += 1
    return count


time(10)


# %%
def time1(n):
    """Time calculation."""
    total_count = pow(10, 9)
    count = 0
    while (count < total_count):
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    count += 1
    return count


time1(10)


# %%
def time2():
    """Time to count upto 1 billion."""
    res = 0
    for i in range(pow(10, 9)):
        res += 1


# %%
def harmonic_number(n):
    """Harmonic number."""
    first_value = np.log(n)
    second_value = 0.57721
    third_value = 1/(12 * n)
    harmonic_value = first_value + second_value + third_value
    return harmonic_value


harmonic_number(3)


# %%
def sequential_search(lst, v):
    """Implement sequential search."""
    for i in range(1, len(lst)):
        if lst[i] == v:
            return i
    return -1


sequential_search([10, 20, 80, 30, 60, 50], 30)
sequential_search([10, 20, 80, 30, 60, 50], 100)


# %%
def binary_search(lst, v, l1, r):
    """Implement binary search."""
    while(r >= l1):
        middle = (r+l1)//2
        if v == lst[middle]:
            return middle
        elif v < lst[middle]:
            r = middle - 1
        else:
            l1 = middle + 1
    return -1


lst = [2, 3, 4, 5, 40, 60]
binary_search(lst, 10, 0, len(lst))
