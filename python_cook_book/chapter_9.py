"""Metaprogramming."""

import time
from functools import wraps
import logging


# %%
# Putting a wrapper around a function
def timethis(func):
    """Decporator that reports the execution time."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


@timethis
def countdown(n):
    """Count down."""
    while n > 0:
        n -= 1


countdown(100000)


@timethis
def add(*args):
    return sum(*args)


add([1, 2])


# %%
# Preserving function metadata when writting decorators
countdown.__name__
countdown.__doc__
countdown.__annotations__


def timethis(func):
    """Decporator that reports the execution time."""
    # @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(func.__name__, end-start)
        return result
    return wrapper


@timethis
def countdown(n):
    """Count down."""
    while n > 0:
        n -= 1


countdown(10)
countdown.__name__
countdown.__doc__
countdown.__annotations__

countdown.__wrapped__(100000)


# %%
# Unwrapping a decorator
@timethis
def add(x, y):
    """Add function."""
    return x + y


orig_add = add.__wrapped__
orig_add(3, 4)


def decorator1(func):
    """Test."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Decorator1')
        return func(*args, **kwargs)
    return wrapper


def decorator2(func):
    """Test."""
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Decorator2')
        return func(*args, **kwargs)
    return wrapper


@decorator1
@decorator2
def add(x, y):
    """Add."""
    return x + y


add(2, 3)
add.__wrapped__(2, 3)


# %%
# Defining a decorator that takes arguments
def logged(level, name=None, message=None):
    """Add logging to a function."""
    def decorate(func):
        logname = name if name else func.__module__
        log = logging.getLogger(logname)
        logmsg = message if message else func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs):
            log.log(level, logmsg)
            return func(*args, **kwargs)
        return wrapper
    return decorate


@logged(logging.DEBUG)
def add(x, y):
    """Add two elements."""
    return x + y


add(1, 2)


@logged(logging.CRITICAL, 'example')
def spam():
    """Spam function."""
    print('Spam!')


spam()
