"""Python learning textbook chapter_7."""

# %%
# Single- and Double-Quoted Strings Are the Same
'shrubbery', "shrubbery"
'knight"s', "knight's"
title = "Meaning " 'of' " life"
title
'knight\'s', 'knight\"s'
s = 'a\nb\tc'
s
print(s)
len(s)
s1 = 'a\nbc'
print(s1)
len(s1)
s1 = 'a\nb\vc'
print(s1)
s = 'a\0b\0c'
print(s)
len(s)
s = '\001\002\x03'
print(s)
len(s)
S = "s\tp\na\x00m"
S
print(S)
len(S)
x = "C:\py\code"
print(x)
len(x)
path = r'C:\new\text.dat'
path
print(path)
len(path)
mantra = """Always look
        on the bright
            side of the life."""
mantra
print(mantra)
# %%
# Strings in action
len('abc')
'abc'+'def'
'abc' * 4
print('---------------------------------')
print('-'*80)
'abc'+9
myjob = 'hacker'
'c' in myjob

#%%
# Indexing and slicing
S='spam'
S[0],S[-4]
S[1:3],S[1:],S[:]
S = 'abcdefghijklmnopqrstuvwxyz'
S[1:10:2]
S[::2]
S[::-1]
S[5:1:-1]
'spam'[1:3]
'spam'[slice(1, 3)]
'spam'[::-1]
'spam'[slice(None, None, -1)

# %%
# String conversion tools
42 + 1
"42" + 1 #(eeror)
int("42"),str(42)
repr(42)
print(str('spam'), repr('spam'))
str('spam'), repr('spam')
S = "42"
I = 1
int(S)+I  #(addition)
S+str(I) #(concatenattion)
text = "1.234E-10"
float(text)
ord('g')
chr(103)
s = '5'
chr(ord(s)+4)
ord('5')
ord('0')
ord('5') - ord('0')
int('1111', 2)
bin(15)
S='spam'
S = S + 'SPAM!'
S
S = S[:4]+'Burger'+S[-1]
S
s = 'spam'
s = s.replace('pa', 'pam1')
s

# %%
# methods of Strings
S = 'xxxxSPAMxxxxSPAMxxxx'
S.replace('SPAM', 'EGGS')
S.replace('SPAM', 'EGGS', 1)

line = "i'mSPAMaSPAMlumberjack"
line.split("SPAM")
line = "The knights who say Ni!\n"
line.rstrip()
line.startswith('The')
line.endswith('Ni!')
line.upper()
line.isalpha()
line.isdigit()
'who' in line
sub = 'Ni!\n'
line[-len(sub):]
line[-len(sub):] == sub
S = 'a+b+c+'
x = S.replace('+', 'spam')
x
'%(qty)d more %(food)s' % {'qty': 1, 'food': 'spam'}

# %%
# Formatting Method Basics
template = '{0}, {1} and {2}'
template.format('spam', 'ham', 'eggs')
template = '%s, %s and %s'
template % ('spam', 'ham', 'eggs')

# %%
# Adding Keys, Attributes, and Offsets
import sys
'My {1[kind]} runs {0.platform}'.format(sys, {'kind': 'laptop'})
'My {map[kind]} runs {0.platform}'.format(sys, map={'kind': 'laptop'})
somelist = list('SPAM')
somelist
'first={0[0]},third={0[3]}'.format(somelist)
'{0:10} = {1:10}'.format('spam', 123.4567)
'{0.platform:>10} = {1[kind]:<10}'.format(sys, dict(kind='laptop'))
print('%s=%s' % ('spam', 42))
print('{}={}' .format('spam', 42))
'{0:b}'.format((2 ** 16) − 1)
'%s' %bin((2**16)-1)
bin((2**16)-1)
'{:,d}'.format(999999999999)
