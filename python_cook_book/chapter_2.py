"""Strings and text."""

import re
import os
from fnmatch import fnmatch, fnmatchcase
from calendar import month_abbr
import unicodedata
import textwrap
import html
from html.parser import HTMLParser
from xml.sax.saxutils import unescape
import collections
from collections import namedtuple


# Splitting strings on any of multiple delimiters
line = 'asdf fjdk; fjek,asdf,    foo'
re.split(r'[;,\s]\s*', line)

fields = re.split(r'(:|,|\s)\s*', line)
fields

values = fields[::2]
values
delimiters = fields[1::2] + ['']
delimiters

# Reform the line using the same delimiters
''.join(v+d for v, d in zip(values, delimiters))
re.split(r'(?:,|;|\s)\s*', line)

# %%
# Matching the text at start or end of a string
filename = 'spam.txt'
filename.endswith('.txt')
filename.startswith('file:')
url = 'http://www.python.org'
url.startswith('http:')

filenames = os.listdir('.')
filenames
[name for name in filenames if name.endswith('.txt')]
any(name.endswith('.py') for name in filenames)

# %%
# Matching strings using shell wildcard patterns
fnmatch('foo.txt', '*.txt')
fnmatch('foo.txt', '?oo.txt')
fnmatch('Dat45.csv', 'Dat[0-9]*')
names = ['Dat1.csv', 'Dat2.csv', 'config.ini', 'foo.py']
[name for name in names if fnmatch(name, 'Dat*.csv')]
fnmatch('foo.txt', '*.TXT')

addresses = ['5412 N CLARK ST',
             '2122 N CLARK ST',
             '1060 W ADDISION ST',
             '4801 N BROADWAY',
             '1039 W GRANVILLE AVE']
[addr for addr in addresses if fnmatchcase(addr, '* ST')]
[addr for addr in addresses if fnmatchcase(addr, '54[0-9][0-9] *CLARK*')]

# %%
# Matching and searching for string patterns
text = 'yeah, but no, but yeah, but no, but yeah'
text == 'yeah'  # Exact match
text.startswith('yeah')
text.endswith('no')
text.find('no')
text.find('yeah')

text1 = '11/27/2012'
text2 = 'Nov 27, 2012'

if re.match(r'\d+/\d+/\d+', text1):
    print('yes')
else:
    print('no')

if re.match(r'\d+/\d+/\d+', text2):
    print('yes')
else:
    print('no')

datepat = re.compile(r'\d+/\d+/\d+')
if datepat.match(text1):
    print('yes')
else:
    print('no')

if datepat.match(text2):
    print('yes')
else:
    print('no')

text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
datepat.findall(text)

datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
m = datepat.match('11/27/2012')
m.group(0)
m.group(1)
m.groups()

month, day, year = m.groups()
text
datepat.findall(text)
for month, day, year in datepat.findall(text):
    print('{}-{}-{}'.format(year, month, day))

# %%
# Searching and replacing text
text = 'yeah, but no, but yeah, but no, but yeah'
text.replace('yeah', 'yep')

text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2', text)

datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
datepat.sub(r'\3-\1-\2', text)


def change_date(m):
    mon_name = month_abbr[int(m.group(1))]
    return '{} {} {}'.format(m.group(2), mon_name, m.group(3))


datepat.sub(change_date, text)

# %%
# Searching and replacing case-insensitive text
text = 'UPPER PYTHON, lower python, Mixed Python'
re.findall('python', text, flags=re.IGNORECASE)
re.sub('python', 'snake', text, flags=re.IGNORECASE)


def matchcase(word):
    """Matchcase function."""
    def replace(m):
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
        else:
            return word
    return replace


re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE)

# %%
# Specifing a regular expression for the shortest match
str_pat = re.compile(r'\"(.*)\"')
text1 = 'Computer says "no."'
str_pat.findall(text1)
text2 = 'Computer says "no." Phone says "yes."'
str_pat.findall(text2)

str_pat = re.compile(r'\"(.*?)\"')
str_pat.findall(text1)
str_pat.findall(text2)

# %%
# Writing a Regular Expression for Multiline Patterns
comment = re.compile(r'/\*(.*?)\*/')
text1 = '/* this is a comment */'
text2 = '''/* this is a
            multiline comment */
   '''
comment.findall(text1)
comment.findall(text2)

comment = re.compile(r'/\*((?:.|\n)*?)\*/')
comment.findall(text2)

comment = re.compile(r'/\*(.*?)\*/', re.DOTALL)
comment.findall(text2)

# %%
# Normalizing Unicode Text to a Standard Representation
s1 = 'Spicy Jalape\u00f1o'
s2 = 'Spicy Jalapen\u0303o'
s1, s2
s1 == s2

t1 = unicodedata.normalize('NFC', s1)
t2 = unicodedata.normalize('NFC', s2)
t1 == t2
print(ascii(t1))
t3 = unicodedata.normalize('NFD', s1)
t4 = unicodedata.normalize('NFD', s2)
t3 == t4
print(ascii(t3))

# %%
# Working with Unicode Characters in Regular Expressions
num = re.compile('\d+')
num.match('123')
num.match('\u0661\u0662\u0663')
arabic = re.compile('[\u0600-\u06ff\u0750-\u077f\u08a0-\u08ff]+')

pat = re.compile('stra\u00dfe', re.IGNORECASE)
s = 'straße'
pat.match(s)
pat.match(s.upper())
s.upper()

# %%
# Stripping Unwanted Characters from Strings
# Whitespace stripping
s = 'hello world \n'
s.strip()
s.lstrip()
s.rstrip()

# Character Stripping
t = '-------hello========'
t.lstrip('-')
t.strip('=')

s = '  hello     world   \n'
s = s.strip()
s
s.replace(' ', '')
re.sub('\s+', ' ', s)

# %%
# Aligining text strings
text = 'Hello World'
text.ljust(20)
text.rjust(20)
text.center(20)
text.rjust(20, '=')
text.center(20, '*')
format(text, '>20')
format(text, '<20')
format(text, '^20')
format(text, '=>20s')
format(text, '*^20s')
'{:>10s} {:>10s}'.format('Hello', 'World')

# %%
# Combining and concatenating strings
parts = ['Is', 'Chicago', 'Not', 'Chicago?']
' '.join(parts)
','.join(parts)
''.join(parts)

a = 'Is Chicago'
b = 'Not Chicago?'
a + ' ' + b
print('{} {}'.format(a, b))
print(a + ' ' + b)

a = 'Hello' 'World'
a

data = ['ACME', 50, 91.1]
','.join(str(d) for d in data)
c = 'Is America'
print(a + ':' + b + ':' + c)
print(':'.join([a, b, c]))
print(a, b, c, sep=':')

# %%
# Interpolating variables in strings
s = '{name} has {n} messages.'
s.format(name='Gudio', n=37)

name = 'Gudio'
n = 37
s.format_map(vars())


class Info:
    """Implementing class."""

    def __init__(self, name, n):
        self.name = name
        self.n = n


a = Info('Gudio', 37)
s.format_map(vars(a))

# %%
# Reformatting text to a fixed number of columns
s = "Look into my eyes, look into my eyes, the eyes, the eyes, \
the eyes, not around the eyes, don't look around the eyes, \
look into my eyes, you're under."

print(textwrap.fill(s, 70))
print(textwrap.fill(s, 40))
print(textwrap.fill(s, 40, initial_indent='  '))
print(textwrap.fill(s, 40, subsequent_indent='  '))

# %%
# Handling HTML and XML Entities in Text
s = 'Elements are written as "<tag>text</tag>".'
print(s)
print(html.escape(s))

# Disabling escaping quotes
print(html.escape(s, quote=False))

s = 'Spicy Jalapeño'
s.encode('ascii', errors='xmlcharrefreplace')

s = 'Spicy &quot;Jalape&#241;o&quot.'
p = HTMLParser()
p.unescape(s)

t = 'The prompt is &gt;&gt;&gt;'
unescape(t)

# %%
# Performing text operations on byte strings
data = b'Hello World'
data[0:5]
data.startswith(b'Hello')
data.split()
data.replace(b'Hello', b'Hello Cruel')
data = bytearray(b'Hello World')
data[0:5]
data.startswith(b'Hello')
data.split()
data.replace(b'Hello', b'Hello Cruel')

data = b'FOO:BAR,SPAM'
re.split(b'[:,]', data)

a = 'Hello World'
a[0]

b = b'Hello World'
b[0]

print(b)
print(b.decode('ascii'))

'{:10s} {:10d} {:10.2f}'.format('ACME', 100, 490.1).encode('ascii')
