"""Practicing pandas filter function."""

import pandas as pd

# %%
data = pd.DataFrame([[1, 4, 3], [2, 5, 6]], index=['mouse', 'rabbit'],
                    columns=['one', 'two', 'three'])
data

# select columns by names
data.filter(items=['one', 'two'])

data.filter(items=['three'])

data.filter(items=['two', 'three'])

# select columns by regular expressions
data.filter(regex='e$', axis=1)

data.filter(regex='^t', axis=1)

data.filter(regex='^r', axis=0)

# select rows containing 'bbi'
data.filter(like='bbi', axis=0)

data.filter(like='ou', axis=0)

data.filter(like='ree')

# %%
data1 = pd.DataFrame([[0, 1], [2, 3]], index=['cat', 'dog'],
                     columns=['weight', 'height'])
data1

# %%
df1 = pd.DataFrame({'key': ['b', 'b', 'a', 'a', 'c'], 'data1': range(5)})
df1

data1.filter(['weight'])

data1.filter(['height'])

data1.filter(regex='t$')

data1.filter(regex='^w', axis=1)

data1.filter(like='a', axis=0)

data1.filter(like='gh', axis=1)
