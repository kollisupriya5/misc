"""Practicing matplotlib."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter

# %%
# Intro to pyplot
plt.plot([1, 2, 3, 4, 5])
plt.ylabel('some numbers')
plt.show()

plt.plot([1, 2, 3, 4, 5], [1, 4, 9, 16, 25])

# %%
# Formatting the style of your plot
plt.plot([1, 2, 3, 4, 5], [1, 4, 9, 16, 25], 'b^')
plt.axis([0, 6, 0, 30])  # [xmin, xmax, ymin, ymax]
plt.show()

t = np.arange(0., 5., 0.2)
plt.plot(t, t, 'r--', t, t ** 2, 'b-', t, t ** 3, 'g^')

# %%
# Plotting with keyword strings
data = {'a': np.arange(50),
        'c': np.random.randint(0, 50, 50),
        'd': np.random.randn(50)}
data['b'] = data['a'] + 10 * np.random.randn(50)
data['d'] = np.abs(data['d']) * 100
plt.scatter('a', 'b', data=data, c='c',s='d')
plt.xlabel('a entry')
plt.ylabel('b entry')

# %%
# Plotting with categorical variables
subjects = ['Telugu', 'Hindi', 'English', 'Maths']
marks = [92, 84, 62, 98]

plt.figure(1, figsize=(9, 3))
plt.subplot(1, 3, 1)
plt.bar(subjects, marks)
plt.subplot(1, 3, 2)
plt.scatter(subjects, marks)
plt.subplot(1, 3, 3)
plt.plot(subjects, marks)
plt.suptitle('categorical plotting')

# %%
# Controlling line properties
x = [1, 2, 3, 4]
y = [2, 1, 4, 3]
plt.plot(x, y, linewidth=2.0)
plt.plot(x, y, '--')
line,  = plt.plot(x, y)
line.set_antialiased(False)

lines = plt.plot([1, 2, 3])
plt.setp(lines)
x1 = [5, 6, 7, 8]
y1 = [5, 6, 7, 8]
multi = plt.plot(x, y, x1, y1)
plt.setp(multi, color='r', linewidth=2.0)
plt.setp(multi, 'color', 'r', 'linewidth', 2.0)


# %%
# Working with multiple figures and Axes
def f(t):
    return np.exp(-t) * np.cos(2*np.pi*t)


t1 = np.arange(0.0, 5.0, 0.1)
t2 = np.arange(0.0, 5.0, 0.02)

plt.subplot(2, 1, 1)
plt.plot(t1, f(t1), 'bo', t2, f(t2), 'black')

plt.subplot(2, 1, 2)
plt.plot(t2, np.cos(2*np.pi*t2), 'r--')

# %%
plt.figure(1)
plt.subplot(2, 1, 1)
plt.plot([1, 2, 3])
plt.subplot(2, 1, 2)
plt.plot([4, 5, 6])

plt.figure(2)
plt.plot([4, 5, 6])

plt.figure(1)
plt.subplot(2, 1, 1)
plt.title('Easy as 1, 2, 3')

# %%
# Working with texts

mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)
n, bins, patches = plt.hist(x, 50, density=1, facecolor='g', alpha=0.75)
plt.xlabel('Smarts')
#plt.xlabel('my data', fontsize=14, color='red')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
plt.text(60, 0.025, r'$\mu=100, \ \sigma=15$')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)
plt.show()

# %%
# Using mathematical expressions in text
plt.title(r'$\sigma_i=15$')

# %%
# Annotating text
t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2*np.pi*t)
line = plt.plot(t, s, lw=2)
(plt.annotate('locolmax', xy=(2, 1), xytext=(3, 1.5),
              arrowprops=dict(facecolor='black', shrink=0.05)))
plt.ylim(-2, 2)
plt.show()

# %%
# Logarithamic and other nonlinear axes
np.random.seed(19680801)
y = np.random.normal(loc=0.5, scale=0.4, size=1000)
y = y[(y > 0) & (y < 1)]
y.sort()
x = np.arange(len(y))
plt.figure(1)
plt.subplot(2, 2, 1)
plt.plot(x, y)
plt.yscale('linear')
plt.title('linear')
plt.grid(True)

plt.subplot(2, 2, 2)
plt.plot(x, y)
plt.yscale('log')
plt.title('log')
plt.grid(True)

plt.subplot(2, 2, 3)
plt.plot(x, y - y.mean())
plt.yscale('symlog', linthreshy=0.01)
plt.title('symlog')
plt.grid(True)

plt.subplot(2, 2, 4)
plt.plot(x, y)
plt.yscale('logit')
plt.title('logit')
plt.grid(True)

plt.gca().yaxis.set_minor_formatter(NullFormatter())
plt.subplots_adjust(top=0.92, bottom=0.08, left=0.10, right=0.95, hspace=0.25, wspace=0.35)
plt.show()
