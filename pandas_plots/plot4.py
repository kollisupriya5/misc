"""Practicing matplotlib."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure()
fig, ax_lst = plt.subplots(2, 2)
fig.suptitle('No axes in this figure')

# %%
# Type of inputs to plotting functions
a = pd.DataFrame(np.random.rand(4, 5), columns=list('abcde'))
a_asndarray = a.values  # Covert dataframe into array
a_asndarray

b = np.matrix([[1, 2], [3, 4]])
b_asarray = np.asarray(b)
b_asarray

# %%
# Matplotlib, pyplot and pylab: how are they related?
x = np.linspace(0, 2, 100)
plt.plot(x, x, label='linear')
plt.plot(x, x ** 2, label='quadratic')
plt.plot(x, x ** 3, label='cubic')
plt.xlabel('x label')
plt.ylabel('y label')
plt.title('simple plot')
plt.legend()
plt.show()

# %%
# Coding Styles
x = np.arange(0, 10, 0.2)
y = np.sin(x)
plt.plot(x, y)
plt.show()

# %%
data1, data2, data3, data4 = np.random.randn(4, 100)


def my_plotter(ax, data1, data2, param_dict):
    out = ax.plot(data1, data2, **param_dict)
    return out


fig, ax = plt.subplots(1, 1)
my_plotter(ax, data1, data2, {'marker': 'x'})
plt.show()

# %%
fig, (ax1, ax2) = plt.subplots(1, 2)
my_plotter(ax1, data1, data2, {'marker': 'x'})
my_plotter(ax2, data3, data4, {'marker': 'o'})
plt.show()
