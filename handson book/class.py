import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, column_names):
        self.column_names = column_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.column_names].values

rooms_col, bedrooms_col, population_col, households_col = 3, 4, 5, 6

class AttributeAdder(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self
    def transform(self, X, y=None):
        rph = X[:, rooms_col] / X[:, households_col]
        bph = X[:, bedrooms_col] / X[:, households_col]
        bpr = X[:, bedrooms_col] / X[:, rooms_col]
        pph = X[:, population_col] / X[:, households_col]
        return np.c_[X, rph, bph, bpr, pph]
