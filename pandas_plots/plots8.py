"""Visualizing linear relationships."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(color_codes=True)
plt.plot
# %%
# loading data
tips = sns.load_dataset('tips')

# %%
# Functions to draw linear regression models
sns.regplot(x='total_bill', y='tip', data=tips)

sns.lmplot(x='total_bill', y='tip', data=tips)

sns.lmplot(x='size', y='tip', data=tips)

sns.lmplot(x='size', y='tip', data=tips, x_jitter=0.05)

sns.lmplot(x='size', y='tip', data=tips, x_estimator=np.mean)

# %%
# Fitting different kinds of models
anscombe = sns.load_dataset("anscombe")

sns.lmplot(x='x', y='y', data=anscombe.query("dataset == 'I'"),
           ci=None, scatter_kws={"s": 80})

sns.lmplot(x='x', y='y', data=anscombe.query("dataset == 'II'"),
           ci=None, scatter_kws={"s": 80})

sns.lmplot(x='x', y='y', data=anscombe.query("dataset == 'II'"),
           order=2, ci=None, scatter_kws={"s": 80})

sns.lmplot(x='x', y='y', data=anscombe.query("dataset == 'III'"),
           ci=None, scatter_kws={"s": 80})

sns.lmplot(x='x', y='y', data=anscombe.query("dataset == 'III'"),
           robust=True,ci=None, scatter_kws={"s": 80})

# %%
tips["big_tip"] = (tips.tip / tips.total_bill) > .15
sns.lmplot(x="total_bill", y="big_tip", data=tips,
           y_jitter=.03)

sns.lmplot(x="total_bill", y="big_tip", data=tips,
           logistic=True, y_jitter=.03)

sns.lmplot(x="total_bill", y="tip", data=tips,
           lowess=True)

# %%
sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'I'"),
              scatter_kws={"s": 80})

sns.residplot(x="x", y="y", data=anscombe.query("dataset == 'II'"),
              scatter_kws={"s": 80})

# %%
# Conditioning on other variables
sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips)

sns.lmplot(x="total_bill", y="tip", hue="smoker", data=tips,
           markers=['o', 'x'], palette='Set1')

sns.lmplot(x="total_bill", y="tip", hue="smoker", col='time', data=tips)

sns.lmplot(x="total_bill", y="tip", hue="smoker",
           col="time", row="sex", data=tips)

# %%
# Controlling the size and shape of the plot
f, ax = plt.subplots(figsize=(5, 6))
sns.regplot(x="total_bill", y="tip", data=tips, ax=ax)

sns.lmplot(x="total_bill", y="tip", col="day", data=tips,
           col_wrap=2, size=3)

sns.lmplot(x="total_bill", y="tip", col="day", data=tips,
           aspect=.5)

# %%
# Plotting a regression in other contexts
sns.jointplot(x="total_bill", y="tip", data=tips, kind="reg")

sns.pairplot(tips, x_vars=["total_bill", "size"], y_vars=["tip"],
             size=5, aspect=.8, kind="reg")

sns.pairplot(tips, x_vars=["total_bill", "size"], y_vars=["tip"],
             hue="smoker", size=5, aspect=.8, kind="reg")
