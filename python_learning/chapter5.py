"""Python learning textbook chapter_5."""

import math
import random
from decimal import Decimal
import decimal
# %%
# Python expression operators +
40 + 3.14
float(4)
int(3.14)
a = 3
b = 4
b / 2 + a
b / (2.0 + a)
num = 1/3.0
num
print(num)
'%e' % num
'%4.2f' % num
'{0:4.2f}'.format(num)

# %%
# Comparisons: Normal and Chained
1 > 2
2.0 >= 1
x = 2
y = 4
z = 6
a = 3
x < y < z < a
x < a < y < z
x < y and y < z
1 < 2 < 3 < 5 < 6
1.1 + 2.2 == 3.3
int(1.1 + 2.2) == int(3.3)
10 / 4
10 // 4
10 / 4.0
10 // 4.0
math.floor(2.5)
math.floor(5.66)
math.floor(-2.5)
math.trunc(-2.5)
5 / 2, 5 / -2
5 // 2, 5 // -2
5 / -2
5 // -2
math.trunc(5 / float(-2))

# %%
# Complex Numbers
1j * 1j
(2 + 1j) * 3

# %%
# Hex, Octal, Binary: Literals and Conversions
0o1, 0o20, 0o377
oct(64)
64, 0o100, 0x40, 0b1000000
eval('64'), eval('0o100'), eval('0x40'), eval('0b1000000')
'{0:o}, {1:x}, {2:b}'.format(64, 64, 64)
'%o, %x, %x, %X' % (64, 64, 255, 255)

# %%
# Bitwise Operations
x = 1
x << 2 #(shift left 2 bits:1000)
x.bit_length()
random.randint(1, 5)
suits = ['hearts', 'clubs', 'diamonds', 'spades']
random.shuffle(suits)
suits

# %%
# Decimal Basics
0.1 + 0.1 + 0.1 - 0.3
print(0.1 + 0.1 + 0.1 - 0.3)
Decimal('0.1') + Decimal('0.1') + Decimal('0.1') - Decimal('0.3')
Decimal(0.1) + Decimal(0.1) + Decimal(0.1) - Decimal(0.3)
decimal.getcontext().prec = 4
decimal.Decimal(1) / decimal.Decimal(7)

# %%
# Fraction type
from fractions import Fraction
x = Fraction(2, 3)
y = Fraction(1, 3)
x
print(x)
x + y
print(x + y)
Fraction('.25')
(2.5).as_integer_ratio()

# %%
# sets
x = set('abcde')
y = set('bdefg')
x - y
z = x.intersection(y)
z.add('SPAM')
z.update(set(['X', 'Y']))
z
z.remove('b')
