"""Function Basics."""


def times(x, y):
    """Create and assign function."""
    return x * y


times(3, 4)


def times1(a, b):
    """Create and assign function."""
    return a // b


times1(10, 5)
times(3.14, 4)
x = times(4, 5)
x
times('NI', 4)


# %%
def intersect(seq1, seq2):
    res = []
    for x in seq1:
        if x in seq2:
            res.append(x)
    return res


intersect([1, 4, 2, 8, 5], [2, 4, 6, 8])


#  %%
s1 = "SPAM"
s2 = "SCAM"
intersect(s1, s2)

# %%
[x for x in s1 if x in s2]

# %%
x = intersect([1, 2, 3], (1, 4))
x
