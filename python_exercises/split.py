"""Function to split the list into two parts."""


def split(n, list):
    """Spliting the list into two parts."""
    res = []
    if len(list) > 1:
        if len(list) > n:
            res.append(list[:n])
        res.append(list[n:])
        return res
    return None


def test_split1():
    """Test for spliting the list."""
    assert split(3, [1, 2, 3, 4, 5, 6, 7]) == [[1, 2, 3], [4, 5, 6, 7]]


def test_split2():
    """Test for spliting the list."""
    assert split(3, ['a', 'b', 'l', 'g', 'h']) == [['a', 'b', 'l'], ['g', 'h']]


def test_split3():
    """Test with empty list."""
    assert split(3, []) is None


def test_split4():
    """Test with single element."""
    assert split(3, [1]) is None


def test_split5():
    """Test for spliting the list."""
    assert split(2, [123, 456, '1bc']) == [[123, 456], ['1bc']]


def test_split6():
    """Test for spliting the list."""
    assert split(4, ['a', 1, 'b', 'c', 2]) == [['a', 1, 'b', 'c'], [2]]
