"""Practicing python cook book chapter1."""

from collections import deque
import heapq
from collections import defaultdict
from collections import OrderedDict
import json
from collections import Counter
from operator import itemgetter
from operator import attrgetter
from itertools import groupby
from itertools import compress
from collections import namedtuple
from collections import ChainMap

# %%
# Unpacking a sequence into separate variables
p = (4, 5)
x, y = p
x
y
data = ['ACME', 50, 91.1, (2012, 12, 21)]
name, share, price, date = data
name
date
name, share, price, (year, month, date) = data
date
month
price
share
s = 'Hello'
a, b, c, d, e = s
a
data = ['ACME', 50, 91.1, (2012, 12, 21)]
_, shares, prices, _ = data
shares


# %%
# Unpacking Elements from Iterables of Arbitrary Length
def avg(lst):
    """Average of a list."""
    return sum(lst)/len(lst)


def drop_first_last(grades):
    """Droping first and last elements."""
    first, *middle, last = grades
    return avg(middle)


grades = [10, 24, 55, 66, 77, 88, 99]
drop_first_last(grades)

record = ('Dave', 'dave@example.com', '773-555-1212', '847-555-1212')
name, email, *phone_numbers = record
name
phone_numbers

sales_record = [1, 2, 3, 4, 5, 6, 7, 8]
*trailing_qtrs, current_qtr = sales_record
trailing_qtrs
current_qtr
trailing_avg = sum(trailing_qtrs)/len(trailing_qtrs)
trailing_avg

# %%
records = [('foo', 1, 2), ('bar', 'hello'), ('foo', 3, 4)]


def do_foo(x, y):
    print('foo', x, y)


def do_bar(s):
    print('bar', x)


for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

line = 'nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false'
uname, *fields, homedir, sh = line.split(':')
uname
sh
fields

record = ('ACME', 50, 123.45, (12, 18, 2012))
name, *_, (*_, year) = record
name
year

# %%
items = [1, 10, 7, 4, 5, 9]


def sum(items):
    """Sum of elements in a list."""
    head, *tail = items
    return head + sum(tail) if tail else head


sum(items)


# %%
# Keeping the Last N Items


# %%
def search(lines, pattern, history=5):
    """Search lines in a file."""
    previous_lines = deque(maxlen=history)
    for line in lines:
        if pattern in line:
            yield line, previous_lines
        previous_lines.append(line)


q = deque(maxlen=3)
q.append(1)
q.append(2)
q.append(4)
q.append(6)

deque([1, 2, 3])
q.appendleft(4)
q.pop()
q.popleft()


# %%
# Finding the Largest or Smallest N Items
nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
print(heapq.nlargest(3, nums))
print(heapq.nsmallest(3, nums))

portfolio = [{'name': 'IBM', 'shares': 100, 'price': 91.1},
             {'name': 'AAPL', 'shares': 50, 'price': 543.22},
             {'name': 'FB', 'shares': 200, 'price': 21.09},
             {'name': 'HPQ', 'shares': 35, 'price': 31.75},
             {'name': 'YHOO', 'shares': 45, 'price': 16.35},
             {'name': 'ACME', 'shares': 75, 'price': 115.65}]

cheap = heapq.nsmallest(3, portfolio, key=lambda s: s['price'])
expensive = heapq.nlargest(3, portfolio, key=lambda s: s['price'])
cheap
expensive


nums = [1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
heap = list(nums)
heapq.heapify(heap)
heap
heapq.heappop(heap)


# %%
# Implementing a Priority Queue
class PriorityQueue:
    """Impelementing the class."""

    def __init__(self):
        self._queue = []
        self._index = 0

    def push(self, item, priority):
        """Push function."""
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1

    def pop(self):
        """Pop function."""
        return heapq.heappop(self._queue)[-1]


# %%
class Item:
    """Implementing class."""

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'Item({!r})'.format(self.name)


q = PriorityQueue()
q.push(Item('foo'), 1)
q.push(Item('bar'), 2)
q.push(Item('spam'), 5)
q.pop()
q.pop()

# %%
# Mapping keys to multiple values in a Dictionary
d = {'a': [1, 2, 3], 'b': [4, 5]}
e = {'a': {1, 2, 3}, 'b': {4, 5}}

d = defaultdict(list)
d['a'].append(1)
d['b'].append(2)
d['b'].append(3)
d['b'].append(3)
d

d = defaultdict(set)
d['a'].add(1)
d['b'].add(2)
d['a'].add(4)
d

d = {}
d.setdefault('a', []).append(1)
d.setdefault('a', []).append(2)
d.setdefault('b', []).append(4)
d


# %%
d = {}
for key, values in pairs:
    if key not in d:
        d[key] = []
    d[key].append(values)

d = defaultdict(list)
for key, value in pairs:
    d[key].append(value)

# %%
# Keeping Dictionaries in Order
d = OrderedDict()
d['foo'] = 1
d['bar'] = 2
d['spam'] = 3
d
for key in d:
    print(key, d[key])

json.dumps(d)

# %%
# Calculating with Dictionaries

prices = {'ACME': 45.23, 'AAPL': 612.78, 'IBM': 205.55,
          'HPQ': 37.20, 'FB': 10.75}

min_price = min(zip(prices.values(), prices.keys()))
min_price
max_price = max(zip(prices.values(), prices.keys()))
max_price
prices_sorted = sorted(zip(prices.values(), prices.keys()))
prices_sorted

prices_and_names = zip(prices.values(), prices.keys())
print(min(prices_and_names))
print(max(prices_and_names))

min(prices)
max(prices)
min(prices.values())
max(prices.values())

min(prices, key=lambda k: prices[k])
max(prices, key=lambda k: prices[k])

min_value = prices[min(prices, key=lambda k: prices[k])]
min_value

prices = {'AAA': 45.23, 'ZZZ': 45.23}
min(zip(prices.values(), prices.keys()))
max(zip(prices.values(), prices.keys()))

# %%
# Finding commonalities in two dictionaries
a = {'x': 1, 'y': 2, 'z': 3}
b = {'w': 10, 'x': 11, 'y': 2}
a.keys() & b.keys()
a.keys() - b.keys()
b.keys() - a.keys()
a.items() & b.items()
a.items() - b.items()

c = {key: a[key] for key in a.keys() - {'z', 'w'}}
c


# %%
# Removing Duplicates from a Sequence while Maintaining Order
def dedupe(items):
    """Remove duplicates in a list."""
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)


a = [1, 5, 2, 1, 9, 1, 5, 2, 1, 9, 1, 5, 10, 2, 20]
list(dedupe(a))


def dedupe1(items, key=None):
    """Remove duplicates in a list."""
    seen = set()
    for item in items:
        val = item if key is None else key(item)
        if val not in seen:
            yield item
            seen.add(val)


a = [{'x': 1, 'y': 2}, {'x': 1, 'y': 3}, {'x': 1, 'y': 2}, {'x': 2, 'y': 4}]
list(dedupe1(a, key=lambda d: (d['x'], d['y'])))
list(dedupe1(a, key=lambda d: d['x']))
list(dedupe1(a, key=lambda d: d['y']))

# %%
# Naming a Slice
record = '0123456789012345678901234567890123456789012345678901234567890'
cost = int(record[20:32]) * float(record[40:48])
cost

shares = slice(20, 32)
price = slice(40, 48)

cost = int(record[shares]) * float(record[40:48])
cost

a = slice(10, 50, 2)
a.start
a.stop
a.step

s = 'HelloWorld'
a.indices(len(s))

for i in range(*a.indices(len(s))):
    print(s[i])

# %%
# Determining the Most Frequently Occurring Items in a Sequence
words = ['look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
         'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
         'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
         'my', 'eyes', "you're", 'under']
word_counts = Counter(words)
word_counts
top_three = word_counts.most_common(3)
top_three

word_counts['not']
word_counts['around']


morewords = ['why', 'are', 'you', 'not', 'looking', 'in', 'my', 'eyes']
for word in morewords:
    word_counts[word] += 1

word_counts['why']

word_counts.update(morewords)
a = Counter(words)
b = Counter(morewords)
c = a + b
d = a - b

# %%
# Sorting a List of Dictionaries by a Common Key
rows = [{'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
        {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
        {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
        {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}]

rows_by_fname = sorted(rows, key=itemgetter('fname'))
rows_by_fname
rows_by_uid = sorted(rows, key=itemgetter('uid'))
rows_by_uid
rows_by_lfname = sorted(rows, key=itemgetter('lname', 'fname'))
rows_by_lfname

rows_by_fname1 = sorted(rows, key=lambda r: r['fname'])
rows_by_fname1

min(rows, key=itemgetter('uid'))
max(rows, key=itemgetter('uid'))


# %%
# Sorting Objects Without Native Comparison Support
class User:
    """Implementing class."""

    def __init__(self, user_id):
        """Function."""
        self.user_id = user_id

    def __repr__(self):
        """Function."""
        return 'User({})'.format(self.user_id)


users = [User(23), User(4), User(99)]
users
sorted(users, key=lambda u: u.user_id)
sorted(users, key=attrgetter('user_id'))

min(users, key=attrgetter('user_id'))
max(users, key=attrgetter('user_id'))

# %%
# Grouping records together based on a field
rows = [{'address': '5412 N CLARK', 'date': '07/01/2012'},
        {'address': '5148 N CLARK', 'date': '07/01/2012'},
        {'address': '5800 E 58TH', 'date': '07/02/2012'},
        {'address': '2122 N CLARK', 'date': '07/03/2012'},
        {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
        {'address': '1060 W ADDISON', 'date': '07/02/2012'},
        {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
        {'address': '1039 W GRANVILLE', 'date': '07/04/2012'}]

rows.sort(key=itemgetter('date'))
rows

for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
        print('  ', i)

# %%
# Filtering sequence elements
mylist = [1, 4, -5, 10, -7, 2, 3, -1]
[n for n in mylist if n > 0]
[n for n in mylist if n < 0]
pos = (n for n in mylist if n > 0)
for x in pos:
    print(x)

values = ['1', '2', '-3', '-', '4', 'N/A', '5']


def is_int(val):
    try:
        x = int(val)
        return True
    except ValueError:
        return False


ivals = list(filter(is_int, values))
print(ivals)

clip_neg = [n if n > 0 else 0 for n in mylist]
clip_neg

addresses = ['5412 N CLARK',
             '5148 N CLARK',
             '5800 E 58TH',
             '2122 N CLARK',
             '5645 N RAVENSWOOD',
             '1060 W ADDISION',
             '4801 N BROADWAY',
             '1039 W GRANVILLE']

counts = [0, 3, 10, 4, 1, 7, 6, 1]
more5 = [n > 5 for n in counts]
more5
list(compress(addresses, more5))


# %%
# Extracting a subsequent of a dictionary
prices = {'ACME': 45.23,
          'AAPL': 612.78,
          'IBM': 205.55,
          'HPQ': 37.20,
          'FB': 10.75}
p1 = {key: value for key, value in prices.items() if value > 200}
p1
tech_names = {'AAPL', 'IBM', 'HPQ', 'MSFT'}
p2 = {key: value for key, value in prices.items() if key in tech_names}
p2

# %%
# Mapping nmaes to sequence elements
Subscriber = namedtuple('Subscriber', ['addr', 'joined'])
sub = Subscriber('jonesy@example.com', '2012-10-09')
sub
sub.addr
sub.joined


def compute_cost(records):
    total = 0.0
    for rec in records:
        total += rec[1] * rec[2]
    return total


records = [1, 2, 3, 4, 5]
compute_cost(records)

# %%
# Transfering and reducing data at the same time
nums = [1, 2, 3, 4, 5]
s = sum(x * x for x in nums)
s

# %%
# Combining multiple mappings into a single mapping
a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
c = ChainMap(a, b)
print(c['x'])
print(c['z'])

values = ChainMap()
values['x'] = 1
# To add new mapping
values = values.new_child()
values['x'] = 2
values = values.new_child()
values['x'] = 3
values = values.new_child()
values['x']
# To discard last mapping
values = values.parents
values['x']
values = values.parents
values['x']
values = values.parents
values['x']
values

a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
merged = dict(b)
merged.update(a)
merged['x']
merged['y']
merged['z']
a['x'] = 13
merged['x']

a = {'x': 1, 'z': 3}
b = {'y': 2, 'z': 4}
merged = ChainMap(a, b)
merged['x']
a['x'] = 42
merged['x']
