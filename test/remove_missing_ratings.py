def remove_missing_ratings(v1,v2):
    res = []
    res1 = []
    for e in range(len(v1)):
        if v1[e] == 0 or v2[e] == 0:
            continue
        res.append(v1[e])
        res1.append(v2[e])
    return res, res1

remove_missing_ratings([1,2,0,4,5],[3,0,5,6,7])

import numpy as np
def pearson(a1,a2):
    n=len(a1)
    p=np.array(a1)
    q=np.array(a2)
    r=n*(np.sum(p*q))-(np.sum(p)*np.sum(q))
    d1=(n*(np.sum(p**2))-(np.sum(p)*np.sum(p)))
    d2=(n*(np.sum(q**2))-(np.sum(q)*np.sum(q)))
    d=np.sqrt(d1*d2)
    a=r/d
    b=round(a,3)
    return b
pearson([3.5,4,4,4],[1,0.3,0.6,1])


def pearson1(p,q):
    v1,v2=remove_missing_ratings(p,q)
    return pearson(v1,v2)
pearson1([3.5,4,1,4,4,0],[1,0.3,0,0.6,1,6])

