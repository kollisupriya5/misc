
import re

#isphonenumber function
def isphonenumber(number):
    regex= "1?[\s-]?\(?(\d{3})\)?[\s-]?\d{3}[\s-]?\d{4}"
    if re.search(regex, number):
        return True
    return False

#isnumber function
def isnumber(number):
    regex = '^[+-]?e?(\d+,?)(.\d+)*(e\d+)?(e-\d+)?$'
    if re.search(regex, number):
        return True
    return False

#isemail function
def isemail(mail):
    regex = '^[a-z]([\w\.]*)(@\w+)?(.\w+)?([\w\.]*)?(@\w+)?([a-z])$'
    if re.search(regex,mail):
        return True
    return False

#matching specific filenames
def isfilename(file):
    regex = '(\w+)\.(jpg|png|gif)$'
    if re.search(regex,file):
        return True
    return False

#Trimming whitespaces
def iswhitespace(file):
    regex = '^\s*(.*)\s*$'
    a = re.search(regex,file)
    return file.strip()

#Extracting data from log entries
def isdata(file):
    regex = '(\w+)\(([\w\.]+):(\d+)\)'
    a = re.search(regex,file)
    return a.groups()

#Extracting data from url
def url(file):
    regex = '(\w+)://([\w\-\.]+)(:(\d+))?'
    a = re.search(regex,file)
    return a.group(1),a.group(2),a.group(4)
