"""Plotting with categorical data."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style='whitegrid', color_codes=True)

# %%
# Loading the data
titanic = sns.load_dataset('titanic')
iris = sns.load_dataset('iris')
tips = sns.load_dataset('tips')
titanic.head()
iris.head()
tips.head()
# %%
# Categorical scatterplots
sns.stripplot(x='day', y='total_bill', data=tips)
sns.stripplot(x='day', y='total_bill', data=tips, jitter=True)
sns.swarmplot(x='day', y='total_bill', data=tips)
sns.swarmplot(x='day', y='total_bill', data=tips, hue='sex')
sns.swarmplot(x='size', y='total_bill', data=tips)
sns.swarmplot(x='total_bill', y='day', data=tips, hue='sex')

# %%
# Distributions of observations within categories
sns.boxplot(x='day', y='total_bill', data=tips, hue='time')

tips["weekend"] = tips["day"].isin(["Sat", "Sun"])
sns.boxplot(x="day", y="total_bill", hue="weekend", data=tips, dodge=False)

# %%
# Violin plots
sns.violinplot(x='total_bill', y='day', hue='time', data=tips)
sns.violinplot(x='total_bill', y='day', hue='time', data=tips, bw=.1,
               scale='count', scale_hue=False)
sns.violinplot(x='day', y='total_bill', hue='sex', data=tips, split=True)
sns.violinplot(x='day', y='total_bill', hue='sex', data=tips, split=True,
               inner='stick', palette='Set3')

# %%
sns.violinplot(x="day", y="total_bill", data=tips, inner=None)
sns.swarmplot(x="day", y="total_bill", data=tips, color="w", alpha=.5)

# %%
# Statistical estimation within categories
# barplot
sns.barplot(x="sex", y="survived", hue="class", data=titanic)
sns.countplot(x='deck', data=titanic, palette='Greens_d')
sns.countplot(y='deck', data=titanic, palette='Greens_d', hue='class')

# %%
# point plots
sns.pointplot(x="sex", y="survived", hue="class", data=titanic)

# %%
sns.pointplot(x="class", y="survived", hue="sex", data=titanic,
              palette={"male": "g", "female": "m"},
              markers=["^", "o"], linestyles=["-", "--"])

# %%
# Pointing wide from data
sns.boxplot(data=iris, orient="h")
sns.violinplot(x=iris.species, y=iris.sepal_length)

# %%
f, ax = plt.subplots(figsize=(7, 3))
sns.countplot(y='deck', data=titanic, color='c')

# %%
# Drawing multi-panel categorical plots
sns.factorplot(x='day', y='total_bill', hue='smoker', data=tips)

sns.factorplot(x='day', y='total_bill', hue='smoker', data=tips, kind='bar')

sns.factorplot(x='day', y='total_bill', hue='smoker', col='time',
               data=tips, kind='swarm')

sns.factorplot(x='time', y='total_bill', hue='smoker', col='day',
               data=tips, kind='box', size=4, aspect=.5)


# %%
g = sns.PairGrid(tips, x_vars=['smoker', 'time', 'sex'],
                 y_vars=['total_bill', "tip"], aspect=.75, size=3.5)
g.map(sns.violinplot, palette="pastel")
