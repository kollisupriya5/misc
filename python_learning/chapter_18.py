"""Practicing python learning textbook chapter 18."""


# %%
# Arguments and shared references
def f(a):
    a = 99


b = 88
f(b)
print(b)


def changer(a, b):
    a = 0
    b[0] = 'spam'


x = 1
L = [1, 2]
changer(x, L)
x, L

# %%
x = 1
a = x
a = 2
print(x)

# %%
L = [1, 2]
b = L
b[0] = 'spam'
b
print(L)

# %%
L = [1, 2]
changer(x, L[:])
x, L


def changer(a, b):
    b = b[:]
    a = 2
    b[0] = 'spam'


L = [1, 2]
changer(x, tuple(L))


# %%
# Simulating output parameters and multi[ple results
def multiple(x, y):
    x = 2
    y = [3, 4]
    return x, y


x = 1
L = [1, 2]
x, L = multiple(x, L)
x, L


# %%
# Keyword and default arguments
def f(a, b, c):
    print(a, b, c)

f(1, 2, 3)

f(c=3, b=2, a=1)

f(1, c=3, b=2)

func(name='Bob', age=40, job='dev')


# Defaults
def f(a, b=2, c=3):
    print(a, b, c)


f(1)
f(a=1)
f(1, 4)
f(5, 6, 3)
f(1, c=6)


# %%
def func(spam, eggs, toast=0, ham=0):
    print((spam, eggs, toast, ham))


func(1, 2)
func('a', 'b')
func(1, ham=1, eggs=0)
func(1, 2, 3, 4)
func(toast=1, eggs=2, spam=3)


# %%
# Arbitary arguments example
def f(*args):
    print(args)


f()
f(1)
f(1, 2)
f(1, 2, 3, 4)


# %%
def f(**args):
    print(args)


f()
f(a=1, b=2, c=3)


# %%
def f(a, *pargs, **kargs): print(a, pargs, kargs)


f(1, 2, 3, x=1, y=2)


# %%
# Calls:Unpacking arguments
def func(a, b, c, d):
    print(a, b, c, d)


args = (1, 2)
args = (3, 4)
args += (5, 6)
func(*args)

args = {'a': 1, 'b': 2, 'c': 3}
args['d'] = 4
func(**args)

# %%
func(*(1, 2), **{'d': 4, 'c': 3})
func(1, *(2, 3), **{'d': 4})
func(1, c=3, *(2,), **{'d': 4})
func(1, *(2, 3), d=4)
func(1, *(2,), c=3, **{'d': 4})


# %%
# Applying functions generically
args = (2, 3)
args += (4,)
args


# %%
def tracer(func, *pargs, **kargs):
    print('calling:', func.__name__)
    return func(*pargs, **kargs)


def func(a, b, c, d):
    return a + b + c + d


print(tracer(func, 1, 2, c=3, d=4))


# %%
def echo(*args, **kwargs): print(args, kwargs)


echo(1, 2, a=3, b=4)
pargs = (1, 2)
kargs = {'a': 3, 'b': 4}
echo(*pargs, **kargs)
echo(0, c=5, *pargs, **kargs)


# %%
# keyword-only Arguments
def kwonly(a, *b, c):
    print(a, b, c)


kwonly(1, 3, c=2)
kwonly(a=1, c=3)
kwonly(1, 2, 3)


# %%
def kwonly(a, *, b, c):
    print(a, b, c)


kwonly(1, c=3, b=2)
kwonly(c=3, b=2, a=1)
kwonly(1, 2, 3)
kwonly(1)


# %%
def kwonly(a, *, b='spam', c='ham'):
    print(a, b, c)


kwonly(1)
kwonly(1, c=3)
kwonly(a=1)
kwonly(c=3, b=2, a=1)


# %%
def kwonly(a, *, b, c='spam'):
    print(a, b, c)


kwonly(1, b='eggs')
kwonly(1, c='eggs')


# %%
def kwonly(a, *, b=1, c, d=2):
    print(a, b, c, d)


kwonly(3, c=4)
kwonly(3, c=4, b=5)
