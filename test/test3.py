import unittest
import remove_missing_ratings as rmr

class testmethod(unittest.TestCase):
    def test1(self):
        v1=[1,2,0,4,5]
        v2=[3,0,5,6,7]
        self.assertEqual(rmr.remove_missing_ratings(v1,v2),([1, 4, 5], [3, 6, 7]))  
    def test2(self):
        v1=[1,3,0,6,7]
        v2=[4,0,8,9,0]
        self.assertEqual(rmr.remove_missing_ratings(v1,v2),([1,6],[4,9]))
    def test3(self):
        v1=[1,2,0,4]
        v2=[3,0,5]
        with self.assertRaises(IndexError):
            rmr.remove_missing_ratings(v1,v2)
    def test4(self):
        v1=[1,2,0,4,5]
        v2=[3,0,5,6]
        with self.assertRaises(IndexError):
            rmr.remove_missing_ratings(v1,v2)
    def test5(self):
        v1=[1,2,0,4,5]
        v2=[3,0,5,6,7]
        self.assertNotEqual(rmr.remove_missing_ratings(v1,v2),([1, 2, 4, 5], [3, 5, 6, 7]))
    def test6(self):
        v1=[1,2,0,4,5]
        v2=[3,0,5,6,7]
        self.assertNotEqual(rmr.remove_missing_ratings(v1,v2),([1, 3, 5], [3, 6, 7])) 
    def test7(self):
        v1=[1,2,0,4,5]
        v2=[3,0,5,6,7]
        actual_value=rmr.remove_missing_ratings(v1,v2)
        self.assertSequenceEqual(actual_value,([1, 4, 5], [3, 6, 7])) 
    def test8(self):
        v1=[1,2,0,4,7]
        v2=[3,0,5,6,8]
        actual_value = rmr.remove_missing_ratings(v1,v2)
        self.assertSequenceEqual(actual_value,([1, 4, 7],  [3, 6, 8]))
    def test9(self):
        p=[1,2,0,4,5]
        q=[3,0,5,6,7]
        self.assertEqual(rmr.pearson1(p,q),1)
    def test10(self):
        p=[3.5,4,1,4,4,0]
        q=[1,0.3,0,0.6,1,6]
        self.assertEqual(rmr.pearson1(p,q),-0.53900000000000003)

import unittest
import distance as d

class testmethod1(unittest.TestCase):
    def test1(self):
        p=(63,68,86,72,73,74)
        q=(87,100,110,111,70,79)
        self.assertEqual(d.pearson1(p,q),0.32961238803118453)
    def test2(self):
        p=(63,68,86,72,73,74,75,76)
        q=(87,100,110,111,70,79,80,81)
        self.assertEqual(d.pearson1(p,q), 0.2268390333418977)
    def test3(self):
        p=(63,68,86,72,73,74,75)
        q=(87,100,110,111,70)
        with self.assertRaises(IndexError):
            d.pearson1(p,q)

import unittest
import normalize as n

class testmethods(unittest.TestCase):
    def test1(self):
        x=[1,2,3,4,5]
        l=0
        u=5
        v= n.normalisation(l,u,x)
        actual_value = v.tolist()
        self.assertSequenceEqual(actual_value,[0.2, 0.4, 0.6,  0.8,  1.0 ])  

if __name__ == '__main__':
    unittest.main()