"""Practicing python learning book chapter 34."""


def gobad(x, y):
    """Try/else."""
    return x / y


def gosouth(x):
    """Try/else."""
    print(gobad(x, 0))


gosouth(1)


def kaboom(x, y):
    """kaboom."""
    print(x + y)


try:
    kaboom([0, 1, 2], 'spam')
except TypeError:
    print('Hello world')
print('resuming here')


# %%
# try/finally statement
class MyError(Exception):
    """Implementing class."""

    pass


def stuff(file):
    """Stuff function."""
    raise MyError()


file = open('data', 'w')

try:
    stuff(file)
finally:
    file.close()
print('not reached')

# Unified try/except/finally
# %%
sep = '-' * 45 + '\n'

print(sep + 'EXCEPTION RALSES AND CAUGHT')
try:
    x = 'spam'[99]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')


print(sep + 'NO EXCEPTION RAISED')
try:
    x = 'spam'[3]
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')


print(sep + 'NO EXCEPTION RAISED, WITH ELSE')
try:
    x = 'spam'[3]
except IndexError:
    print('except run')
else:
    print('else run')
finally:
    print('finally run')
print('after run')


print(sep + 'EXCEPTION RALSES AND CAUGHT')
try:
    x = 1 / 0
except IndexError:
    print('except run')
finally:
    print('finally run')
print('after run')


# %%
# Raising exceptions


# %%
# Scopes and try expect variables
try:
    1/0
except Exception as X:
    print(X)


# %%
try:
    1 / 0
except Exception as X:
    print(X)
    Saveit = X

# %%
# Propagating Exceptions with raise
try:
    raise IndexError('spam')
except IndexError:
    print('propogating')
    raise


try:
    1 / 0
except Exception as E:
    raise TypeError('Bad') from E

# %%
try:
    try:
        raise IndexError()
    except Exception as E:
        raise TypeError() from E
except Exception as E:
    raise SyntaxError() from E


# %%
# Assert statement
def f(x):
    """Assert statement."""
    assert x < 0, 'x must be negative'
    return x ** 2


f(2)
f(-1)


def reciprocal(x):
    """Reciprocal."""
    assert x != 0
    return 1 / x


reciprocal(10)
