"""Python learning book chapter29."""


# %%
# Class Coding Details
class SharedData:
    """Shared Data class."""

    spam = 42


x = SharedData()
y = SharedData()
x.spam, y.spam

SharedData.spam = 99
x.spam, y.spam, SharedData.spam
x.spam = 88
x.spam, y.spam, SharedData.spam


# %%
class MixedNames:
    """Implementing class."""

    data = 'spam'

    def __init__(self, value):
        self.data = value

    def display(self):
        print(self.data, MixedNames.data)


x = MixedNames(1)
y = MixedNames(2)
x.display(); y.display()


# %%
class NextClass:
    """Implementing NextClass."""

    def printer(self, text):
        self.message = text
        print(self.message)


x = NextClass()
x.printer('instance call')
x.message

NextClass.printer(x, 'class call')
x.message
NextClass.printer(x, 'bad call')
x.message
# NextClass.printer('bad call')


# %%
class Super:
    """Implementing super class."""

    def method(self):
        print('in Super.method')


class Sub(Super):
    """Implementing Sub class."""

    def method(self):
        print('starting Sub.method')
        Super.method(self)
        print('ending Sub.method')


x = Super()
x.method()

x = Sub()
x.method()


# %%
class Super():
    """Super constructor."""

    def method(self):
        """Function."""
        print('in Super.method')

    def delegate(self):
        """Function."""
        self.action()


class Inheritor(Super):
    """Inherit method."""

    pass


class Replacer(Super):
    """Replace method."""

    def method(self):
        """Replace function."""
        print('in Replacer.method')


class Extender(Super):
    """Extended metod."""

    def method(self):
        """Ectend function."""
        print('starting Ectender.method')
        Super.method(self)
        print('ending Extender.method')


class Provider(Super):
    """Provider method."""

    def action(self):
        """Function."""
        print('in Provider.action')


if __name__ == '__main__':
    for klass in (Inheritor, Replacer, Extender):
        print('\n' + klass.__name__ + '...')
        klass().method()
    print('\nProvider...')
    x = Provider()
    x.delegate()
