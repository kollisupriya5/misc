"""Practicing pandas join function."""

import pandas as pd

# %%
df1 = pd.DataFrame({'key': ['b', 'b', 'a', 'a', 'c'], 'data1': range(5)})
df1

df2 = pd.DataFrame({'key': ['d', 'e', 'f'], 'data2': range(3)})
df2

df1.join(df2, lsuffix='_df1', rsuffix='_df2')
df1.set_index('key').join(df2.set_index('key'))
df1.join(df2.set_index('key'), on='key')

# %%

df2.join(df1, lsuffix='_df2', rsuffix='_df1')
df2.set_index('key').join(df1.set_index('key'))
df2.join(df1.set_index('key'))

# %%
caller = (pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3', 'K4', 'K5'],
                        'A': ['A0', 'A1', 'A2', 'A3', 'A4', 'A5']}))
caller

other = pd.DataFrame({'key': ['K0', 'K1', 'K2'], 'B': ['B0', 'B1', 'B2']})
other
caller.join(other, lsuffix='_caller', rsuffix='_other')
caller.set_index('key').join(other.set_index('key'))
caller.join(other.set_index('key'), on='key')
