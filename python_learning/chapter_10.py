"""Python learning textbook chapter10."""

# %%
a = 1; b = 2; print(a+b)
mylist = [1111,
          2222,
          3333,
          4444, 55555]
mylist
A = 1; B = 2; C = 3
X = (A+B+C)
X
X1 = (A + B +
      C)
X1
if (A == 1 and
    B == 2 and
    C == 3):
    print('spam' * 3)
X2 = A + B + \
      C
X2
while True:
    reply = input('Enter text:')
    if reply == 'stop': break
    print(reply.upper())
reply = '20'
reply ** 20
int(reply) ** 2
while True:
    reply = input('Enter text:')
    if reply == 'stop': break
    print(int(reply) ** 2)
print('Bye')
s = '123'
t = 'abc'
s.isdigit(), t.isdigit()

# %%
while True:
    reply = input('Enter text:')
    if reply == 'stop':
        break
    elif not reply.isdigit():
        print('Bad!' * 8)
    else:
        print(int(reply) ** 2)
print('Bye')

# %%
while True:
    reply = input('Enter text:')
    if reply == 'stop': break
    try:
        num = int(reply)
    except:
        print('Bad!' * 8)
    else:
        print(num ** 2)
print('Bye')

# %%
while True:
    reply = input('Enter text:')
    if reply == 'stop': break
    try:
        print(int(reply) ** 2)
    except:
        print('Bad!' * 8)
print('Bye')

# %%
while True:
    reply = input('Enter text:')
    if reply == 'stop': break
    try:
        print(float(reply) ** 2)
    except:
        print('Bad!' * 8)
print('Bye')

# %%
while True:
    reply = input('Enter text:')
    if reply == 'stop':
        break
    elif not reply.isdigit():
        print('Bad!' * 8)
    else:
        num = int(reply)
        if num < 20:
            print('low')
        else:
            print(num ** 2)
print('Bye')
