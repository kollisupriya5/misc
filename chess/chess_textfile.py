"""Taking inputs from the text file for chess game."""

import re


# %%
def chess(value):
    """Chess move."""
    regex = "(?:\d\.)?(?:\s)?([a-h\-1-8]{5})"
    if re.search(regex, value):
        return True
    return False


chess('1. e2-e4 e7-e5')


# %%
def file():
    """Read values from text file."""
    path = "/home/supriya/python/atom_notebooks/misc/chess/chess.text.txt"
    with open(path, 'r') as f:
        data = f.read()
    x = data.splitlines()
    res = []
    for i in range(len(x)):
        res.append(x[i])
    return res


# %%
def values():
    """Add values to list."""
    x = file()
    res = []
    for i in x:
        regex = "(?:\d\.)?(?:\s)?([a-h\-1-8]{5})"
        res = res + re.findall(regex, i)
    return res


values()
