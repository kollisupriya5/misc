"""Unzip."""

a = ['a', 'b', 'c']
b = [1, 2, 3]
for (i, j) in zip(a, b):
    print(i, j)

# Method1
a, b = zip(*zip(a, b))
a, b

# Method 2
c = list(zip(a, b))
res = []
res1 = []
for i in c:
    res.append((i)[0])
    res1.append((i)[1])
print(res, res1)
