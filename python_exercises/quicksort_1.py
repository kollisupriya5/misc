"""Implementing Quick sort."""


import math
import random


# %%
def partition(lst, l, r):
    """Quick sort."""
    x = lst[r]
    i = l - 1
    for j in range(l, r):
        if lst[j] <= x:
            i = i + 1
            lst[i], lst[j] = lst[j], lst[i]
    lst[i + 1], lst[r] = lst[r], lst[i + 1]
    return i+1


# %%
def quicksort_recursion(lst, l, r):
    """Quick sort using recursion."""
    if l < r:
        i = partition(lst, l, r)
        quicksort_recursion(lst, l, i-1)
        quicksort_recursion(lst, i+1, r)
    return lst


# %%
def quicksort(lst):
    """Implemneting Quick sort."""
    return quicksort_recursion(lst, 0, len(lst)-1)


quicksort([13, 19, 9, 5, 12, 8, 7, 4, 21, 2, 6, 11])


# %%
# Quick sort in descending order
def partition_reverse(lst, l, r):
    """Quick sort in reverse order."""
    x = lst[r]
    i = l - 1
    for j in range(l, r):
        if lst[j] >= x:
            i = i + 1
            lst[i], lst[j] = lst[j], lst[i]
    lst[i + 1], lst[r] = lst[r], lst[i + 1]
    return i+1


# %%
def quicksort_recursion_reverse(lst, l, r):
    """Quick sort using recursion."""
    if l < r:
        i = partition_reverse(lst, l, r)
        quicksort_recursion_reverse(lst, l, i-1)
        quicksort_recursion_reverse(lst, i+1, r)
    return lst


# %%
def quicksort_reverse(lst):
    """Implemneting Quick sort."""
    return quicksort_recursion_reverse(lst, 0, len(lst)-1)


quicksort_reverse([13, 19, 9, 5, 12, 8, 7, 4, 21, 2, 6, 11])


# %%
# If all the elements in the array are same
def partition_samevalues(lst, l, r):
    """Quick sort."""
    x = lst[r]
    i = l - 1
    for j in range(l, r):
        if lst[j] <= x:
            i = i + 1
            lst[i], lst[j] = lst[j], lst[i]
    lst[i + 1], lst[r] = lst[r], lst[i + 1]
    if all(x == lst[0] for x in lst):
        return math.floor((l+r)/2)
    else:
        return i + 1


# %%
def quicksort_recursion_samevalues(lst, l, r):
    """Quick sort using recursion."""
    if l < r:
        i = partition_samevalues(lst, l, r)
        quicksort_recursion_samevalues(lst, l, i-1)
        quicksort_recursion_samevalues(lst, i+1, r)
    return lst


# %%
def quicksort_samevalues(lst):
    """Implemneting Quick sort."""
    return quicksort_recursion_samevalues(lst, 0, len(lst)-1)


quicksort_samevalues([1, 1, 1, 1, 1])


# Randomized quick sort
# %%
def randomized_partition(lst, l, r):
    """Randomized partition."""
    i = random.randint(l, r)
    lst[r], lst[i] = lst[i], lst[r]
    return partition(lst, l, r)


# %%
def randomized_quicksort_recursion(lst, l, r):
    """Randomized Quick sort using recursion."""
    if l < r:
        q = randomized_partition(lst, l, r)
        randomized_quicksort_recursion(lst, l, q-1)
        randomized_quicksort_recursion(lst, q+1, r)
    return lst


# %%
def randomized_quicksort(lst):
    """Implemneting Quick sort."""
    return randomized_quicksort_recursion(lst, 0, len(lst)-1)


randomized_quicksort([13, 19, 9, 5, 12, 8, 7, 4, 21, 2, 6, 11])
