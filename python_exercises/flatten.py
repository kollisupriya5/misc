"""Function to find the flattening of a list."""


def flatten(lst):
    """Flattening a list."""
    res = []
    if len(lst) != 0:
        for i in lst:
            if type(i) == list:
                res = res+flatten(i)
            else:
                res.append(i)
        return res
    return None


def test_flatten1():
    """Test for flattening a list."""
    assert flatten([[1, 2, 3], [4, 5]]) == [1, 2, 3, 4, 5]


def test_flatten2():
    """Test for flattening a list."""
    assert flatten(['a', ['b', ['c', 'd'], 'e']]) == ['a', 'b', 'c', 'd', 'e']


def test_flatten3():
    """Test with empty list."""
    assert flatten([]) is None


def test_flatten4():
    """Test with single element."""
    assert flatten([1]) == [1]


def test_flatten5():
    """Test for flattening a list."""
    assert flatten([[1, 2, 3], [4, 5]]) == [1, 2, 3, 4, 5]


def test_flatten6():
    """Test for flattening a list."""
    assert (flatten(['a', [1, ['b', 2], 'c']]) ==
            ['a', 1, 'b', 2, 'c'])


def test_flatten7():
    """Test with single element."""
    assert flatten(['a']) == ['a']


def test_flatten8():
    """Test for flattening a list."""
    assert flatten(['a', 'b', 'c', 'd', 'e']) == ['a', 'b', 'c', 'd', 'e']
