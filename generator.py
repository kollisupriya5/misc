           ##map##

def inc (x):
    return x+1

def map(f,gen):
    for e in gen:
        yield f(e)
m=map(inc, [1,2,3])
for i in m:
    print(i)
o/p : 2
      3
      4

------------------------------------------------------------
          ##filter##

def even(x):
    if x%2 == 0:
        print(x)

def filter(pred,gen):
    for e in gen:
        if pred(e):
            yield e
f = filter(even,range(10))
for i in f:
    print(i)

o/p : 2
      4
      6
      8
        
----------------------------------------------------------------
          ##take##
def take(n,gen):
    res=[]
    for e in range(n):
        yield gen[e]
t=take(3,[1,2,3,4,5,6,7,8,9])
for i in t:
    print(i)

o/p : 1
      2
      3
-----------------------------------------------------------------
         ##take_while##
def take_while(pred,gen):
    for e in gen:
        if pred(e):
            yield e
        else:
            return gen
tw = take_while(even,[2,3,4])
for i in tw:
    print(i)

o/p : 2
-------------------------------------------------------------------
        ##dedupe##

def dedupe(gen):
    for e in range(len(gen)):
        if gen[e]!=gen[e-1]:
            yield gen[e]
d = dedupe([1,1,1,2,2,3,4,5,1,1,6,2])
for i in d:
    print(i)

o/p : 1 2 3 4 5 1 6 2

-----------------------------------------------------------------
      ##interleave##

def interleave(gen1,gen2):
    while gen1 and gen2:
        yield(gen1.pop(0),gen2.pop(0))
l=interleave([2,3],[5,6,7])
for i in l:
    print(i)

o/p : (2, 5)
      (3, 6)

------------------------------------------------------------------
       ##concat##

def concat(gen,b):
    for i in gen :
        yield i
    yield b
c=concat([1,2,3],4)  
for e in c:
    print(e)

o/p : 1 2 3 4

---------------------------------------------------------------------
    ##partition##
def partition(n,gen):
    res=[]
    for e in range(len(gen)//n):
        if n == 0:
            yield gen
        else:
            yield (gen[(e*n):((e+1)*n)])
pa=partition(2,list(range(10)))
for i in pa:
    print(i)

o/p : [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]]

------------------------------------------------------------------------
   ##partition_all##
def partition_all(n,gen):
    res=[]
    for e in range(len(gen)//n+1):
        if n == 0:
            yield gen
        else:
            yield (gen[(e*n):((e+1)*n)])
p=partition_all(2,[1,2,3,4,5,6,7,8,9])
for i in p:
    print(i)

o/p : [[1, 2], [3, 4], [5, 6], [7, 8], [9]]

---------------------------------------------------------------------------
     ##reverse##

def reverse(gen):
    res = []
    for e in range(1,len(gen)+1):
        yield (gen[-e])        
r=reverse([1,2,3,4])
for i in r:
    print(i)

o/p : 4
      3
      2
      1

-----------------------------------------------------------------------------
       ##interpose##

def interpose(gen1,gen2):
    for i in range(len(gen2)):
        yield gen2[i]
        if i < len(gen2)-1:
            yield gen1
list(interpose("",[1,2,3,4]))

o/p : [1, '', 2, '', 3, '', 4]

------------------------------------------------------------------------------


