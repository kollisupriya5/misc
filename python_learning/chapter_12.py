"""Practicing python learning textbook chapter12."""

# %%
# If statements
if 1:
    print('true')

if not 1:
    print('true')
else:
    print('false')

# Multiway Branching
x = 'killer rabbit'
if x == 'rogger':
    print('shave and a haircut')
else:
    if x == 'bugs':
        print('whats up docs?')
    else:
        print('Run away! Run away!')

# %%
x = 'killer rabbit'
if x == 'rogger':
    print('shave and a haircut')
elif x == 'bugs':
    print('whats up docs?')
else:
    print('Run away! Run away!')

# %%
choice = 'spam'
(print({'spam': 1.25, 'ham': '1.99',
        'eggs': 0.99, 'bacon': 1.10}[choice]))

# %%
choice = 'eggs'
if choice == 'spam':
    print(1.25)
elif choice == 'ham':
    print(1.99)
elif choice == 'eggs':
    print(0.99)
elif choice == 'bacon':
    print('1.10')
else:
    print('bad choice')

# %%
# Handling switch defaults
branch = {'spam': 1.25, 'ham': '1.99', 'eggs': 0.99}
print(branch.get('spam', 'bad choice'))
print(branch.get('bacon', 'bad choice'))

# Using if statements
choice = 'eggs'
if choice in branch:
    print(branch[choice])
else:
    print('Bad choice')

# %%
try:
    print(branch[choice])
except KeyError:
    print('bad choice')

# %%
# Block Delimiters: Indentation Rules
x = 1
if x:
    y = 2
    if y:
        print('block2')
    print('block1')
print('block0')

# %%
x = 'SPAM'
if 'rubbery' in 'shrubbery':
    print(x * 8)
    x += 'NI'
    if x.endswith('NI'):
        x *= 2
    print(x)

# %%
x = 'SPAM'
if 'rubbery' in 'shrubbery':
    print(x * 8)
    x += 'NI'
    if x.endswith('NI'):
        x *= 2
        print(x)

# %%
# A few special cases
L = ['Good',
     'Bad',
     'Ugly']
a = b = c = d = e = f = g = 1
if a == b and c == d and \
   d == e and f == g:
    print('old')
if (a == b and c == d and
   d == e and f == g):
    print('new')
x = 1 + 2 + 3 \
    + 4
x = 1; y = 2; print(x)
s = """
abc
def
ghi
"""
if 1: print('hello')

# Truth values and boolean tests
2 < 3, 3 < 2
2 or 3, 3 or 2
2 and 3, 3 and 2
{} or []
{} and []
{} or 3
[] and 3

# %%
# The if/else Ternary Expression
A = 't' if 'spam' else 'f'
A
A = 't' if '' else 'f'
A
['f', 't'][bool('')]
['f', 't'][bool('spam')]
