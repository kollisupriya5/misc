"""Function for insertion sort."""


def insertion_sort(list):
    """Insertion sort."""
    if len(list) == 0:
        return None
    for i in range(1, len(list)):
        i = i+1
        j = i-1
        while j > 0:
            if list[j-1] > list[j]:
                list[j-1], list[j] = list[j], list[j-1]
            j = j-1
    return list


insertion_sort([1, 5, 2, 4, 3, 7, 6, 2])
insertion_sort([10, 33, 22, 11, 1])
insertion_sort([27, 13, 42, 10, 1])


def test_insertionsort1():
    """Test for sorting elements in the list."""
    assert insertion_sort([1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_insertionsort2():
    """Test for sorting elements in the list."""
    assert insertion_sort([1, 1]) == [1, 1]


def test_insertionsort3():
    """Test with empty list."""
    assert insertion_sort([]) is None


def test_insertionsort4():
    """Test for sorting elements in the list."""
    assert insertion_sort([1, 5, 2, 4, 3, 7, 6, 2]) == [1, 2, 2, 3, 4, 5, 6, 7]


def test_insertionsort5():
    """Test with negative values."""
    assert insertion_sort([1, 2, -4, 5, 0, -6]) == [-6, -4, 0, 1, 2, 5]
