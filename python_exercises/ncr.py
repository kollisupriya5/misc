"""Function to find the ncr."""


def factorial(n):
    """Factorial of a number."""
    fact = 1
    for i in range(1, n+1):
        fact = fact * i
    return fact


def ncr(n, r):
    """Function of ncr."""
    return factorial(n)/(factorial(n-r)*factorial(r))


def test_ncr1():
    """Test for ncr."""
    assert ncr(5, 3) == 10


def test_ncr2():
    """Test for ncr."""
    assert ncr(10, 5) == 252


def test_ncr3():
    """Test for ncr."""
    assert ncr(10, 6) == 210
