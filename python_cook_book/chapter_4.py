"""Iterators and generators."""

import itertools
from itertools import permutations
from itertools import combinations
from itertools import zip_longest
from itertools import chain
from collections import Iterable
import heapq

# %%
# Manually consuming an iteration
items = [1, 2, 3, 4]
it = iter(items)
next(it)
next(it)
next(it)


# %%
# Delegating iteration
class Node:
    """Implementing class."""

    def __init__(self, value):
        """Function."""
        self._value = value
        self._children = []

    def __repr__(self):
        """Function."""
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        """Add children."""
        self._children.append(node)

    def __iter__(self):
        """Function."""
        return iter(self._children)


if __name__ == '__main__':
    root = Node(0)
    child1 = Node(1)
    child2 = Node(2)
    root.add_child(child1)
    root.add_child(child2)
    for ch in root:
        print(ch)


# %%
# Creating new iteration patterns with generstors
def frange(start, stop, increment):
    """Range function."""
    x = start
    while x < stop:
        yield x
        x += increment


for n in frange(0, 4, 0.5):
    print(n)

list(frange(0, 4, 0.5))


def countdown(n):
    """Countdown function."""
    print('starting to count from', n)
    while n > 0:
        yield n
        n -= 1
    print('Done!')


c = countdown(3)
next(c)
next(c)
next(c)
next(c)


# %%
# Implementing the iterator protocol
class Node:
    """Implementing class."""

    def __init__(self, value):
        """Function."""
        self._value = value
        self._children = []

    def __repr__(self):
        """Function."""
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        """Add children."""
        self._children.append(node)

    def __iter__(self):
        """Function."""
        return iter(self._children)

    def depth_first(self):
        """Traverse a node."""
        yield(self)
        for c in self:
            yield from c.depth_first()


if __name__ == '__main__':
    root = Node(0)
    child1 = Node(1)
    child2 = Node(2)
    root.add_child(child1)
    root.add_child(child2)
    child1.add_child(Node(3))
    child1.add_child(Node(4))
    child1.add_child(Node(5))
    for ch in root.depth_first():
        print(ch)

# %%
# Iterating in Reverse
a = [1, 2, 3, 4, 5]
for x in reversed(a):
    print(x)

f = 'Reversed iteration only works if the object in question has a size'
for line in reversed(list(f)):
    print(line, end='')


class Countdown:
    """Implementing class."""

    def __init__(self, start):
        """Function."""
        self.start = start

    def __iter__(self):
        """Function."""
        n = self.start
        while n > 0:
            yield n
            n -= 1

    def __reversed__(self):
        """Function."""
        n = 1
        while n <= self.start:
            yield n
            n += 1


# %%
# Taking a Slice of an Iterator
def count(n):
    """Count function."""
    while True:
        yield n
        n += 1


c = count(0)
for x in itertools.islice(c, 10, 20):
    print(x)


# %%
# Iterating Over All Possible Combinations or Permutations
items = ['a', 'b', 'c']
for p in permutations(items):
    print(p)

for p in permutations(items, 2):
    print(p)

for p in combinations(items, 2):
    print(p)

for p in combinations(items, 3):
    print(p)

for p in combinations(items, 1):
    print(p)

for c in itertools.combinations_with_replacement(items, 3):
    print(c)


# %%
# Iterating Over the Index-Value Pairs of a Sequence
for idx, val in enumerate(items):
    print(idx, val)

for idx, val in enumerate(items, 10):
    print(idx, val)

# %%
# Iterating Over Multiple Sequences Simultaneously
x = [1, 2, 3, 4, 5]
y = [6, 7, 8, 9, 10]
for i, j in zip(x, y):
    print(i, j)

a = [1, 2, 3]
b = ['a', 'b', 'c', 'd']
for i, j in zip(a, b):
    print(i, j)

for i in zip_longest(a, b):
    print(i)

for i in zip_longest(a, b, fillvalue=0):
    print(i)

headers = ['name', 'shares', 'price']
values = ['ACME', 100, 490.1]
s = dict(zip(headers, values))
s

for name, val in zip(headers, values):
    print(name, val)

for name, val in zip(headers, values):
    print(name, '=', val)

a = [1, 2, 3]
b = [4, 5, 6]
c = [7, 8, 9]
for i in zip(a, b, c):
    print(i)

zip(a, b)
list(zip(a, b))
list(zip(a, b, c))

# %%
# Iterating on Items in Separate Containers
a = [1, 2, 3]
b = ['a', 'b', 'c', 'd']
for x in chain(a, b):
    print(x)


def flatten(items, ignore_types=(str, bytes)):
    """Flattening a nested list."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from flatten(x)
        else:
            yield x


items = [1, 2, [3, 4, [5, 6], 7], 8]
for x in flatten(items):
    print(x)


def flatten1(items, ignore_types=(str, bytes)):
    """Flattening a nested list."""
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            for i in flatten1(x):
                yield i
        else:
            yield x


for x in flatten1(items):
    print(x)

# %%
# Iterating in Sorted Order Over Merged Sorted Iterables
a = [1, 4, 7, 10]
b = [2, 5, 6, 11]
for c in heapq.merge(a, b):
    print(c)
