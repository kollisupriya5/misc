"""Python learning textbook chapter_9."""

from collections import namedtuple
import pickle
# %%
# Tuples in Action
(1, 2)+(3, 4)  # concatenattion
(1, 2)*4  # Repetition
T = (1, 2, 3, 4)
T[0]
T[1:3]

# %%
# Conversions, methods, and immutability
T = ('cc', 'aa', 'dd', 'bb')
type(T)
s = list(T)
s.sort()
s
sorted(T)
T = (1, 2, 3, 4, 5)
L = [x + 20 for x in T]
L
T
T.index(2)
T.count(2)
T = (1, [2, 3], 4)
T[1][0] = 'spam'
T

# %%
# Records Revisited: Named Tuples
bob = ('Bob', 40.5, ['dev', 'mgr'])
bob[0]
bob[2][0]
bob = dict(name='Bob', age=40.5, jobs=['dev', 'mgr'])
bob
bob['name'],bob['jobs']
bob.values()
tuple(bob.values())
list(bob.items())
Rec = namedtuple('Rec', ['name', 'age', 'jobs'])  # make a generated class
bob = Rec('Bob', age=40.5, jobs=['dev', 'mgr'])
bob
bob[2]
bob.name,bob.jobs
o = bob._asdict()  # coverting into a dictonary
o['name'],o['jobs']
o
bob = Rec('Bob', 40.5, ['dev', 'mgr'])
name, age, jobs = bob
name, age, jobs
for x in bob: print(x)

# %%
# Files
myfile = open('myfile.txt', 'w')
myfile.write('hello text file\n')
myfile.write('goodbye text file\n')
myfile.close()
myfile = open('myfile.txt')
myfile.readline()
myfile.readline()
open('myfile.txt').read()
print(open('myfile.txt').read())
for line in open('myfile.txt'):
    print(line, end='')

# %%
# Storing Python Objects in Files: Conversions
X, Y, Z = 43, 44, 45
S = 'Spam'
D = {'a': 1, 'b': 2}
L = [1, 2, 3]
F = open('datafile.txt', 'w')
F.write(S + '\n')
F.write('%s,%s,%s\n' % (X, Y, Z))
F.write(str(L) + '$' + str(D) + '\n')
F.close()
open('datafile.txt').read()
print(open('datafile.txt').read())
F = open('datafile.txt')
F.readline()
F.readline()
F.readline()
line = F.readline()
line.rstrip()
line = F.readline()
line1 = line.split(',')
int(line1[1])
numbers = [int(P) for P in line1]
numbers
line = F.readline()
line
parts = line.split('$')
L = ['abc', [(1, 2), ([3], 4)], 5]eval(parts[1])
objects = [eval(P) for P in parts]
objects

# %%
# Storing Native Python Objects: pickle
D = {'a': 1, 'b': 2}
F = open('datafile.pkl', 'wb')
pickle.dump(D, F)
F.close()
F = open('datafile.pkl', 'rb')
E = pickle.load(F)
E
open('datafile.pkl', 'rb').read()
L = ['abc', [(1, 2), ([3], 4)], 5]
L
L[1][1]
L[1][1][0]
L[1][1][0][0]
X = [1, 2, 3]
L = ['a', X, 'b']
D = {'x':X, 'y':2}
X[1] = 'surprise'
L
D
L = [1,2,3]
D = {'a':1, 'b':2}
A = L[:]
B = D.copy()
A[1] = 'Ni'
B['c'] = 'spam'
L, D
X = [1, 2, 3]
L = ['a', X[:], 'b']
D = {'x':X[:], 'y':2}
L1 = [1, ('a', 3)]
L2 = [1, ('a', 3)]
L1 == L2
L2 is L1
11 == '11'
11 >= '11'
['11', '22'].sort()
[11, '11'].sort()
11 > 9.123
str(11) >= '11', 11 >= int('11')
D1 = {'a':1, 'b':2}
D2 = {'a':1, 'b':3}
D1 == D2
list(D1.items())
sorted(D1.items())
sorted(D1.items()) < sorted(D2.items())
sorted(D1.items()) > sorted(D2.items())
bool(1)
bool('spam')
bool({})
L = [1, 2, 3]
M = ['X', L, 'Y']
M
L[1] = 0
M
M = ['X', L[:], 'Y']
M
L[1] = 0
M
L = [4, 5, 6]
X = L * 4
Y = [L] * 4
Y = [list(L)] * 4
Y
L = ['grail']
L.append(L)
L
