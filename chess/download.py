"""Read only moves from the downloaded file."""

import re
import os
import uuid


# %%
def write_moves(x):
    """Write into file."""
    input1 = ''
    j = 1
    for i in range(0, len(x), 2):
        if len(x[i:i+2]) == 2:
            x1 = '{}. {} {}\n'.format(j, *x[i:i+2])
        else:
            x1 = '{}. {}'.format(j, *x[i:])
        input1 = input1 + x1
        j += 1
    return input1


# %%
def file_read():
    """Read values from the file."""
    read = "/home/supriya/python/atom_notebooks/misc/chess/Berliner.pgn"
    with open(read, 'r') as readfile:
        res1 = {}
        i = 0
        for line in readfile:
            if ((line.startswith('[') or line.startswith(' ') or
                 line.startswith('\n'))):
                continue
            if line.startswith("1."):
                i = i + 1
                x = str(i)
                res1[x] = ''
            res1[x] = res1[x] + line
        return res1


file_read()


# %%
def write_file():
    """Write values."""
    res2 = {}
    x = file_read()
    for i, j in x.items():
        regex = "(?:\d\.)?([a-z\-1-8\+]{2,5})\s"
        reg = re.compile(regex, re.MULTILINE | re.I)
        res2[i] = re.findall(reg, j)
        filename = str(uuid.uuid4())
        file_name = filename+'.pgn'
        print(file_name)
        b = os.getcwd()
        path = os.path.join(b, filename+'.pgn')
        with open(path, 'wt') as f:
            f.write(write_moves(res2[i]))


write_file()
