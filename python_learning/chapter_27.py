"""Python learning textbook chapter27."""


# %%
class FirstClass:

    def setdata(self, value):
        self.data = value

    def display(self):
        print(self.data)


x = FirstClass()
y = FirstClass()

x.setdata("king")
y.setdata(3.14159)

x.display()
y.display()

x.data = "New value"
x.display()

x.anothername = "spam"
x.display()


# %%

class SecondClass(FirstClass):

    def display(self):
        print('Current value = "%s"' % self.data)


z = SecondClass()
z.setdata(42)
z.display()

x.display()


# %%
class ThirdClass(SecondClass):
    """ThirdClass function."""

    def __init__(self, value):
        self.data = value

    def __add__(self, other):
        return ThirdClass(self.data + other)

    def __str__(self):
        return '[ThirdClass: %s]' %self.data

    def mul(self, other):
        self.data *= other


a = ThirdClass('abc')
a.display()
print(a)

b = a + 'xyz'
b.display()
print(b)

a.mul(3)
print(a)


# %%
class rec: pass


rec.name = 'Bob'
rec.age = 40
print(rec.name)
x = rec()
y = rec()
x.name, y.name
x.name = 'sue'
rec.name, x.name, y.name

# %%
list(rec.__dict__.keys())
list(name for name in rec.__dict__ if not name.startswith('__'))
list(x.__dict__.keys())
list(y.__dict__.keys())

x.name, x.__dict__['name']
x.age
x.__dict__['age']


# %%
def uppername(obj):
    return obj.name.upper()

uppername(x)

rec.method = uppername
x.method()
uppername(y)
y.method()
rec.method(x)
rec.method(y)

# %%
# Classes versus Dictionaries
rec = ('Bob', 40.5, ['dev', 'mgr'])
rec[0]

rec = {}
rec['name'] = 'Bob'
rec['age'] = 40.5
rec['jobs'] = ['dev', 'mgr']

rec['name']


class rec: pass

rec.name = 'Bob'
rec.age = 40.5
rec.jobs = ['dev', 'mgr']

rec.name

# %%
class rec: pass

pers1 = rec()
pers1.name = 'Bob'
pers1.jobs = ['dev', 'mgr']
pers1.age = 40.5

pers2 = rec()
pers2.name = 'Sue'
pers2.jobs = ['dev', 'cto']

pers1.name, pers2.name
pers1.jobs, pers2.jobs


# %%
class person:

    def __init__(self, name, jobs, age=None):
        self.name = name
        self.jobs = jobs
        self.age = age

    def info(self):
        return (self.name, self.jobs)


rec1 = person('Bob', ['dev', 'mgr'], 40.5)
rec2 = person('Sue', ['dev', 'cto'])

rec1.jobs, rec2.info()
rec2.name, rec1.info()
