"""Functions."""

from functools import partial
import math


# %%
# Function that accepts any number of arguments
def avg(first, *rest):
    """Funtion that accepts any no of arguments."""
    return (first + sum(rest)) / (1 + len(rest))


avg(1, 2)
avg(1, 2, 3, 4, 5)


def anyargs(*args, **kwargs):
    """Function."""
    print(args)
    print(kwargs)


# %%
# Function that only accept keyword arguments
def recv(maxsize, *, block):
    """Function."""
    'Receives a message'
    pass


recv(1024, True)
recv(1024, block=True)


def minimum(*values, clip=None):
    """Minimum number in a list."""
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m


minimum(1, 2)
minimum(1, 5, 2, -5, 6, 0, 10)
minimum(1, 5, 2, -5, 6, 0, 10, clip=0)
minimum(1, 5, 2, -5, 6, 0, 10, clip=1)

help(recv)


# %%
# REturning multiple values from a function
def myfunc():
    """Return multiple values from a function."""
    return 1, 2, 3


a, b, c = myfunc()
a
b
c

x = myfunc()
x


# %%
# Defining function with default arguments
def spam(a, b=42):
    """Function."""
    print(a, b)


spam(1)
spam(1, 2)
spam(2)

_no_value = object()


def spam(a, b=_no_value):
    """Function."""
    if b is _no_value:
        print('No b value supplied')


spam(1)
spam(1, 2)


def spam(a, b=[]):
    """Function."""
    print(b)
    return b


x = spam(1)
x
spam(1, 3)
x.append(99)
x.append(1)
x.append('yow!')
x

# %%
# Defining anonymous or inline functions
add = lambda x, y: x + y
add(2, 3)
add('hello', 'world')


def add(x, y):
    return x + y


add(2, 3)
add('hello', 'world')

names = ['David Beazley', 'Brian Jones', 'Raymond Hettinger', 'Ned Batchelder']
sorted(names, key=lambda name: name.split()[-1].lower())


# %%
# capturing variables in anonymous functions
x = 10
a = lambda y: x + y
x = 20
b = lambda y : x + y
a(10)
b(10)

x = 15
a(10)
x = 20
a(10)

x = 10
a = lambda y, x=x: x + y
x = 20
b = lambda y, x=x: x + y
a(10)
b(10)

funcs = [lambda x: x+n for n in range(5)]
for f in funcs:
    print(f(0))


funcs = [lambda x, n=n: x+n for n in range(5)]
for f in funcs:
    print(f(0))


# %%
# Making an N-Argument Callable Work As a Callable with Fewer Arguments
def spam(a, b, c, d):
    """Function."""
    print(a, b, c, d)


s1 = partial(spam, 1)
s1(2, 3, 4)
s1(4, 5, 6)
s2 = partial(spam, d=42)
s2(1, 2, 3)
s2(4, 5, 5)
s3 = partial(spam, 1, 2, d=42)
s3(3)
s3(5)

points = [(1, 2), (3, 4), (5, 6), (7, 8)]


def distance(p1, p2):
    """Distance function."""
    x1, y1 = p1
    x2, y2 = p2
    return math.hypot(x2 - x1, y2 - y1)


pt = (4, 3)
points.sort(key=partial(distance, pt))
points


# %%
# Carying extra state with callback functions
def apply_sync(func, args, *, callback):
    result = func(*args)
    callback(result)


def print_result(result):
    print('Got:', result)


def add(a, b):
    return a + b


apply_sync(add, (2, 3), callback=print_result)
apply_sync(add, ('hello', 'world'), callback=print_result)


def make_handler():
    sequence = 0

    def handler(result):
        nonlocal sequence
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))
    return handler


handler = make_handler()
apply_sync(add, (2, 3), callback=handler)
apply_sync(add, ('hello', 'world'), callback=handler)


def make_handler():
    sequence = 0
    while True:
        result = yield
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))


handler = make_handler()
next(handler)
apply_sync(add, (2, 3), callback=handler.send)
