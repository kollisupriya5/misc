                ##split_at##

def take(n,lst):
    res=[]
    for e in range(n):
        res.append(lst[e])
    return res  
    
def drop(n,lst):
    res=[]
    for e in range(n):
        res=lst[n:]
    return res         
drop(2,[1,2,3,4,5,6]) 

     
def split_at(n,lst):
    return(take(n,lst),drop(n,lst))
   
split_at(2,[1,2,3,4,5,6])

#o/p : ([1, 2], [3, 4, 5, 6])
#-----------------------------------------------------------------------------------------------

           ## split_with##

def take_while(perd, lst):
    res = []
    for e in lst:
        if perd(e):
            res.append(e)
        else:
            return res
    return res   

def drop_while(pred,lst):
    for e in range(0,len(lst)):
        if pred(lst[e]):
            if not pred(lst[e+1]):
                 return (lst[e+1:])
        else:
            return lst  
def split_with(pred,lst):
    return(take_while(pred,lst),drop_while(pred,lst))
split_with(even,[2,4,6,1,3,6,7])

#o/p:([2, 4, 6], [1, 3, 6, 7])

#---------------------------------------------------------------------------------------------------
             ##cycle##

def cycle(n,lst):
    res=[]
    while(len(res)<n):
        for e in lst:
            if len(res)==n:
                return res
            else:
                res.append(e)
cycle(7,[1,2,3])

#o/p : [1, 2, 3, 1, 2, 3, 1]

#----------------------------------------------------------------------------------------------------
               ##partition##

def partition(n,lst):
    res=[]
    for e in range(len(lst)//n):
        if n == 0:
            return lst
        else:
            res.append(lst[(e*n):((e+1)*n)])
    return res

partition_all(2,list(range(20)))

#o/p : [[0, 1],[2, 3],[4, 5],[6, 7],[8, 9],[10, 11],[12, 13], [14, 15],[16, 17],[18, 19]]

#-----------------------------------------------------------------------------------------------------
                  ##partition_all##

def partition_all(n,lst):
    res=[]
    for e in range((len(lst)//n)+1):
        if n == 0:
            return lst
        else:
            res.append(lst[(e*n):((e+1)*n)])
    return res
partition_all(2,[1,2,3,4,5,6,7,8,9])

#o/p : [[1, 2], [3, 4], [5, 6], [7, 8], [9]]

#----------------------------------------------------------------------------------------------------------
                  ##interpose## 
def interpose(lst1,lst2):
    res=[]
    for i in range(len(lst2)):
        res.append(lst2[i])
        if i < len(lst2)-1:
            res.append(lst1)
    return res
interpose("",[1,2,3,4])

#o/p : [1, '', 2, '', 3, '', 4]

#--------------------------------------------------------------------------------------------------------------



