"""Practicing pandas functions stack and unstack a dataframe."""

import numpy as np
import pandas as pd

# %%
# Stack
data = pd.DataFrame({'key': ['b', 'b', 'a', 'a', 'c'], 'data1': range(5)})
data
data.stack()

# %%
# Single level
data1 = pd.DataFrame([[0, 1], [2, 3]], index=['cat', 'dog'],
                     columns=['weight', 'height'])
data1
data1.stack()

# %%
# Multi level
data = pd.MultiIndex.from_tuples([('weight', 'kg'), ('weight', 'pounds')])
data2 = pd.DataFrame([[1, 2], [3, 4]], index=['cat', 'dog'], columns=data)
data2
data2.stack()

# Missing values
data = pd.MultiIndex.from_tuples([('weight', 'kg'), ('height', 'm')])
data3 = pd.DataFrame([[1, 2], [3, 4]], index=['cat', 'dog'], columns=data)
data3
data3.stack()

# %%
# Prescribing the leveels to be stacked
data3.stack(0)
data3.stack([0, 1])

# %%
# Dropping missing values
data = pd.MultiIndex.from_tuples([('weight', 'kg'), ('height', 'm')])
data4 = pd.DataFrame([[None, 1], [2, 3]], index=['cat', 'dog'], columns=data)
data4
data4.stack()
data4.stack(dropna=False)
data4.stack(dropna=True)


# %%
# Unstack
index = pd.MultiIndex.from_tuples([('one', 'a'), ('one', 'b'),
                                   ('two', 'a'), ('two', 'b')])
data1 = pd.Series(np.arange(1.0, 5.0), index=index)
data1
data1.unstack()

# %%
data2 = pd.DataFrame(np.arange(6).reshape((2, 3)),
                     index=pd.Index(['Ohio', 'Colorado'], name='state'),
                     columns=pd.Index(['one', 'two', 'three'],
                     name='number'))
data2
data2.unstack()

# %%
data1.unstack(level=0)
data2.unstack(level=0)
