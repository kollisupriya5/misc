#
# coding: utf-8



1+1




def sub(x):
    def square(y):
        return (x-y) ** 2
    return square






sub(5)(2)




def add(x,y):
    return x+y




add(1,3)




def mean(lst):
    sum = 0
    for e in lst:
        sum = add(sum ,e)
        result = sum/len(lst)
    return result




mean([1,2,3,4,5])




def variance(lst):
    res = 0
    for e in lst:
        res = res + sub(mean(lst))(e)
        result = res/len(lst)
    return result




variance([1,2,3,4,5])




def variance(lst):
    res =0
    for i in lst:
        res = res + sub(mean(lst))(i)
    result = res/len(lst)
    return result




variance([1,2,3,4,5,6,7])




def standard_deviation(lst):
    res = variance(lst)**(.5)
    return res





standard_deviation([1,2,3,4,5,6,7,8])




sorted([1,8,4,10,3,5,0])




def median(lst):
    res = 0
    x = sorted(lst)
    print(x)
    z = len(x)
    if not z%2 == 0:
        return (z+1)//2
    else:
        res = (z)//2
        return (x[res-1]+x[res])/2





median([1,3,2,4,8,7])




def fab(n):
    if n == 1:
        return 1
    result = [1,1]
    while len(result)<n:
        result.append(result[-1]+result[-2])
    return result[:n]




fab(6)




def perfect_numbers1(start,stop):
    for n in range(start,stop):
        sum=0
        for i in range(1,n):
            if n%i == 0:
                sum = sum+i
        if sum == n:
            print(n)




perfect_numbers1(1,10)




def prime_numbers(start,stop):
    for n in range(start,stop):
        k = 0
        for i in range(2,n):
            if n%i == 0:
                k = k + 1
        if k<=0:
            print(n)




prime_numbers(1,10)




def power(x, n):
    num = x
    if n == 0:
        return 1
    elif n==1:
        return x
    else:
        for i in range(1, n):
            x = x * num
        return x




power(2,5)




def no_of_digits(num):
    i=0
    temp = num
    while temp > 0:
        digit = temp % 10
        temp //= 10
        i = i+1
    return i




no_of_digits(1634)




def filter(pred, lst):
    filt = []
    for i in lst:
        if pred(i):
            filt.append(i)
    return filt




def armstrong(num):
    sum=0
    temp = num
    n = no_of_digits(num)
    while temp > 0:
        digit = temp % 10
        sum += power(digit, n)
        temp //= 10
    if num == sum:
        return True
    else:
        return False




armstrong(1634)




def inc(x):
    return x+1
inc(1)




def pipe(first, *rest):
    def piped(*args):
        result=first(*args)
        for f in rest:
            result=f(result)
        return result
    return piped




pipe(inc,inc,inc,str)(100)




def compose2(f,g):
    def composer(x):
        return f(g(x))
    return composer




compose2(inc,inc)(100)
