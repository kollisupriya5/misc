"""Python learning textbook chapter_8."""

# %%
# Basic List Operations
len([1, 2, 3])
[1, 2, 3] + [4, 5, 6]   # concatenattion
['Ni!'] * 4
str([1, 2]) + "34"
[1, 2]+list("34")

# %%
# List Iteration and Comprehensions
3 in [1, 2, 3]
for x in [1, 2, 3]:
    print(x)
for x in [1, 2, 3]:
    print(x, end='')
for c in 'SPAM':
    print(c * 4, end='')
res = [c * 4 for c in 'SPAM']
res
res = []
for c in 'SPAM':
    res.append(c * 4)
res

# %%
# Indexing, Slicing, and Matrixes
L = ['spam', 'Spam', 'SPAM!']
L[1:]
L[:2]
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
matrix[1]
matrix[1][2]

# %%
# Changing Lists in Place
L = ['spam', 'Spam', 'SPAM!']
L[1]
L[1] = 'eggs'
L
L[0:2] = ['eat', 'more']
L
L = [1, 2, 3]
L[1:2] = [4, 5]
L
L[1:1] = [6, 7]
L
L[1:2] = []
L
L = [1]
L[:0] = [2, 3, 4]
L
L[len(L):] = [5, 6, 7]
L.extend([8, 9, 10])
L
L = ['eat', 'more', 'SPAM!']
L.append('please')
L.sort()
L
L = ['abc', 'ABD', 'aBe']
L.sort()
L.sort(key=str.lower)
L
L.sort(key=str.lower, reverse=True)
L
L = ['abc', 'ABD', 'aBe']
sorted(L, key=str.lower, reverse=True)
L = ['abc', 'ABD', 'aBe']
sorted([x.lower() for x in L], reverse=True)
L = [1, 2]
L.extend([3, 4, 5])
L.pop()
L.reverse()
list(reversed(L))
L = []
L.append('a')
L.append('b')
L
L.pop()  # last in first out
L = ['abc', 'ABD', 'aBe']
L.pop(1)  # pop can also delete the item using Indexing
L
L.count('abc')
L = ['spam', 'eggs', 'ham', 'toast']
del L[0]
L[1:] = []
L[0] = []
L

# %%
# Dictionaries
D = {'spam': 2, 'ham': 1, 'eggs': 3}
D
D['ham']
len(D)
'ham' in D
list(D.keys())
D
D['ham'] = ['grill', 'bake', 'fry']
D
D['brunch'] = 'bacon'
D
del D['eggs']
D = {'spam': 2, 'ham': 1, 'eggs': 3}
list(D.values())
list(D.items())
list(D.keys())
D.get('spam')
D
print(D.get('toast'))
D.get('toast', 88)
D2 = {'toast':4, 'muffin':5}
D.update(D2)
D
D.pop('muffin')

# %%
# Movie dataset
table = {'1975': 'Holy Grail', '1979': 'Life of Brian', '1983': 'The Meaning of Life'}
movie = table['1975']
movie
for year in table:
    print('year' + '\t' + table[year])
table = {'Holy Grail' : '1975', 'Life of Brian' : '1979' , 'The Meaning of Life' :'1983'}
movie = table['Holy Grail']
movie
list(table.items())
list(table.values())
list(table.keys())
V = '1975'
[key for (key, value) in table.items() if value == V]
D = {}
D[99] = 'spam'
D[99]
Matrix = {}
Matrix[(2, 3, 4)] = 88
Matrix[(7, 8, 9)] = 99
X = 2; Y = 3; Z = 4
Matrix[(X, Y, Z)]
Matrix
if (2, 3, 6) in Matrix:
    print(Matrix[(2, 3, 6)])
else:
     print(0)
try:
    print(Matrix[(2, 3, 6)])
except KeyError:
    print(0)
Matrix.get((2, 3, 4), 0)
Matrix.get((2, 3, 6), 0)
rec = {}
rec['name'] = 'Bob'
rec['age'] = 40.5
rec['job'] = 'developer/manager'
print(rec['name'])
rec = {'name': 'Bob',
       'jobs': ['developer', 'manager'],
       'web': 'www.bobs.org/ ̃Bob',
       'home': {'state': 'Overworked', 'zip': 12345}}
rec['name']
rec['jobs'][1]
rec['home']['zip']
list(zip(['a', 'b', 'c'], [1, 2, 3]))
D = dict(zip(['a', 'b', 'c'], [1, 2, 3]))
D = {k: v for (k, v) in zip(['a', 'b', 'c'], [1, 2, 3])}
