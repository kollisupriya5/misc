"""Practicing map pandas function."""

import pandas as pd
import numpy as np

x = pd.Series([1, 2, 3], index=['one', 'two', 'three'])
x

y = pd.Series(['foo', 'bar', 'baz'], index=[1, 2, 3])
y

x.map(y)

x1 = pd.Series(['a', 'b', 'c'], index=['foo', 'bar', 'baz'])
x1

y.map(x1)
y

z = {1: 'A', 2: 'B', 3: 'C'}
x.map(z)

s = pd.Series([1, 2, 3, np.nan])
s2 = s.map('this is a string {}'.format, na_action=None)
s
s2

s3 = s.map('this is a string {}'.format, na_action='ignore')
s3

a = pd.Series([0, 1, 2, 3, 4, 5])
a.map(a)

x.map(s)
x.map(a)

b = pd.Series(['one', 'two', 'three', 'four', 'five'], index=[1, 2, 3, 4, 5])
x.map(b)

y1 = pd.Series(['a', 'b', 'c'], index=['one', 'two', 'three'])
y1
b.map(y1)
