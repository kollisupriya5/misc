"""Function to find the last but one element in the list using recursion."""


def lastbutelement(list):
    """Last but one element of a list."""
    if len(list) > 1:
        if len(list) == 2:
            return list[0]
        return lastbutelement(list[1:])
    return None

def test_lastbutelement1():
    """Test2 for Last but one element."""
    assert lastbutelement([1, 2, 3, 4, 5]) == 4


def test_lastbutelement2():
    """Test2 for Last but one element."""
    assert lastbutelement(['a', 'b', 'l', 'g', 'h']) == 'g'


def test_lastbutelement3():
    """Test3 with empty list."""
    assert lastbutelement([]) is None


def test_lastbutelement4():
    """Test with single element."""
    assert lastbutelement([1]) is None


def test_lastbutelement5():
    """Test5 for Last but one element."""
    assert lastbutelement(['a', 'b', 'c', 'd', 'e']) == 'd'


def test_lastbutelement6():
    """Test6 forLast but one element."""
    assert lastbutelement(['a']) is None


def test_lastbutelement7():
    """Test8 forLast but one element."""
    assert lastbutelement([123, 456, '1bc', 'abc', 'cd6']) == 'abc'


def test_lastbutelement8():
    """Test7 for Last but one element."""
    assert lastbutelement(['a', 1, 'b', 'c', 2, 3, 'd']) == 3
