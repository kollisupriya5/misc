"""Practicing pandas drop function."""

import numpy as np
import pandas as pd

# %%
df = (pd.DataFrame(np.arange(12).reshape(3, 4),
                   columns=['A', 'B', 'C', 'D']))
df
df.drop('A', axis=1)
df.drop(['A', 'B'], axis=1)
df.drop(columns=['C', 'D'])  # drop by columns
df.drop([0, 1])  # drop by rows

# %%
data = {'name': ['Jason', 'Molly', 'Tina', 'Jake', 'Amy'],
        'year': [2012, 2012, 2013, 2014, 2014],
        'reports': [4, 24, 31, 2, 3]}
df1 = (pd.DataFrame(data, index=['Cochice', 'Pima',
                    'Santa Cruz', 'Maricopa', 'Yuma']))
df1
df1.drop(['Cochice', 'Pima'])
df1.drop(['year'], axis=1)
df1.drop(df1.index[2])  # Drop a row by row number
df1.drop(df1.index[[2, 3]])
df1.drop(df1.index[-2])
