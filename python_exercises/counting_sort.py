"""Implement sorting algorithms."""

import math


# %%
def counting_sort(lst, k):
    """Sort."""
    lst1 = [0] * len(lst)
    c = []
    c = [0 for i in range(0, k+1)]
    for j in range(0, len(lst)):
        c[lst[j]] = c[lst[j]] + 1
    for i in range(1, k+1):
        c[i] = c[i] + c[i-1]
    print(c)
    for k in range(len(lst)-1, -1, -1):
        lst1[c[lst[k]]-1] = lst[k]
        c[lst[k]] = c[lst[k]] - 1
    return lst1


lst = [6, 0, 2, 0, 1, 3, 4, 6, 1, 3, 2]
a = max(lst)
counting_sort(lst, a)


# %%
def insertion_sort(lst):
    """To sort given list."""
    for j in range(1, len(lst)):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] > key:
            lst[i + 1] = lst[i]
            i = i - 1
        lst[i + 1] = key
    return lst


# %%
def bucket_sort(lst):
    """Implement bucket sort."""
    n = len(lst)
    b = []
    b = [[] for _ in range(n)]
    for i in range(0, n):
        b[math.floor(n * lst[i])].append(lst[i])
    for i in range(0, n-1):
        insertion_sort(b[i])
    result = []
    for i in range(n):
        result = result + b[i]
    return result


bucket_sort([.79, .13, .16, .64, .39, .20, .89, .53, .71, .42])
bucket_sort([0.1, 0.23, 0.12, 0.43, 0.78, 0.76])
