"""Python learning lextbook chapter14."""

import functools, operator
import os
# Iterations and comprehensions
# %%
# Iterations
for x in [1, 2, 3, 4]: print(x ** 2, end=' ')
for x in [1, 2, 3, 4]: print(x ** 3, end=' ')
for x in 'spam': print(x *2, end=' ')

# %%
# File iterators
f = open('script2.py', 'w')
f.write('import sys\n')
f.write('print(sys.path)\n')
f.write('x=2\n')
f.write('print(x ** 32)\n')
f.close()
f = open('script2.py')
text = f.read()
text
print(text)
f = open('script2.py')
f.readline()
f.readline()
f.readline()
f.readline()
f = open('script2.py')
f.__next__()
f.__next__()
f.__next__()
f.__next__()

# %%
for line in open('script2.py'):
    print(line.upper(), end='')

for line in open('script2.py').readlines():
    print(line.upper(), end='')

f = open('script2.py')
while True:
    line = f.readline()
    if not line: break
    print(line.upper(), end='')

# %%
# Manual iteration: iter and next
f = open('script2.py')
f.__next__()
f.__next__()

f = open('script2.py')
next(f)
next(f)

# %%
L = [1, 2, 3]
i = iter(L)
i.__next__()
i.__next__()
i.__next__()
i.__next__()

f = open('script2.py')
iter(f) is f
iter(f) is f.__iter__()
f.__next__()

iter(L) is L
L.__next__()
j = iter(L)
j.__next__()
next(j)

# Manual Iterations
for x in L:
    print(x ** 2, end=' ')

i = iter(L)
while True:
    try:
        x = next(i)
    except StopIteration:
        break
    print(x ** 2, end=' ')

# %%
# Other built in Type iterables
D = {'a': 1, 'b': 2, 'c': 3}
for key in D.keys():
    print(key, D[key])
i = iter(D)
next(i)
next(i)
next(i)

# %%
p = os.popen('dir')
p.__next__()
p.__next__()
next(p)

# %%
# List Comprehensions
L = [1, 2, 3, 4, 5]
for i in range(len(L)):
    L[i] += 10
L
L = [x + 10 for x in L]
L

# List Comprehension Basics
res = []
for x in L:
    res.append(x + 10)
res

# Using list comprehensions in files
f = open('script2.py')
lines = f.readlines()
lines
lines = [line.rstrip() for line in open('script2.py')]
lines
[line.upper() for line in open('script2.py')]
[line.rstrip().upper() for line in open('script2.py')]
[line.split() for line in open('script2.py')]
[line.replace(' ', '!') for line in open('script2.py')]
[('sys' in line, line[:5]) for line in open('script2.py')]

# %%
# Extended List Comprehension Syntax
lines = [line.rstrip() for line in open('script2.py') if line[0] == 'p']
lines

res = []
for line in open('script2.py'):
    if line[0] == 'p':
        res.append(line.rstrip())
res

[line.rstrip() for line in open('script2.py') if line.rstrip()[-1].isdigit()]

# %%
# Nested loops:for
[x + y for x in 'abc' for y in 'lmn']

res = []
for x in 'abc':
    for y in 'lmn':
        res.append(x + y)
res

# %%
# Other Iteration Contexts
uppers = [line.upper() for line in open('script2.py')]
uppers
map(str.upper, open('script2.py'))
list(map(str.upper, open('script2.py')))

# %%
sorted(open('script2.py'))
list(zip(open('script2.py'), open('script2.py')))
list(enumerate(open('script2.py')))
list(filter(bool, open('script2.py')))
functools.reduce(operator.add, open('script2.py'))
list(open('script2.py'))
tuple(open('script2.py'))
'&&'.join(open('script2.py'))
a, b, c, d = open('script2.py')
a, d
a, *b = open('script2.py')
a, b
'y = 2\n' in open('script2.py')
'x = 2\n' in open('script2.py')
L = [11, 22, 33, 44]
L[1:3] = open('script2.py')
L
L = []
L.extend(open('script2.py'))

# %%
# The range Iterable
R = range(10)
R
i = iter(R)
next(i)
next(i)
next(i)
list(R)
len(R)
R[0]
R[-2]
# %%
# The map, zip, and filter Iterables
M = map(abs, (-1, 0, 1))
M
list(M)
next(M)
next(M)
next(M)
for x in M:print(x)

# Zip
Z = zip((1, 2, 3), (10, 20, 30))
next(Z)
next(Z)
next(Z)
for pair in Z:print(pair)
list(Z)

# Filter
filter(bool, ['spam', '', 'ni'])
list(filter(bool, ['spam', '', 'ni']))
[x for x in ['spam', '', 'ni'] if bool(x)]

# %%
# Multiple Versus Single Pass Iterators
R = range(3)
i = iter(R)
next(i)
next(i)
next(i)
next(i)

Z = zip((1, 2, 3), (10, 11, 12))
i = iter(Z)
i1 = iter(Z)
next(i)
next(i)
next(i)

M = map(abs, (-1, 0, 1))
i1 = iter(M)
i2 = iter(M)
print(next(i1), next(i1), next(i2))
next(i2)
# %%
# Dictionary View Iterables
D = dict(a=1, b=2, c=3)
D
k = D.keys()
k
i = iter(k)
next(i)
next(i)
next(i)
for k in D.keys(): print(k, end= ' ')
k = D.keys()
list(k)
v = D.values()
list(v)
list(v)[0]
list(D.items())
for (k, v) in D.items(): print(k, v, end=' ')

D
for k in sorted(D.keys()): print (k, D[k], end=' ')
for k in sorted(D): print (k, D[k], end=' ')
