"""Numbers, Dates and Times."""

from decimal import Decimal
from decimal import localcontext
import math
import struct
import cmath
import numpy as np
from fractions import Fraction
import numpy.linalg
import random
from datetime import timedelta
from datetime import datetime
from dateutil.relativedelta import relativedelta
from dateutil.rrule import*

# %5
# Rounding Numerical Values
round(1.23, 1)
round(1.27, 1)
round(-1.27, 1)
round(1.2567, 3)

a = 1627731
round(a, -1)
round(a, -2)

x = 1.23456
format(x, '0.2f')
format(x, '0.3f')
format(x, '0.4f')
format(x, '0.5f')

'value is {:0.3f}'.format(x)

a = 2.1
b = 4.2
c = a + b
c
c = round(c, 2)
c

# %%
# Performing Accurate Decimal Calculations
a + b == 6.3

a = Decimal('4.2')
b = Decimal('2.1')
a + b
print(a + b)
(a + b) == Decimal('6.3')

c = Decimal('1.3')
d = Decimal('1.7')
print(c / d)

with localcontext() as ctx:
    ctx.prec = 3
    print(c / d)

with localcontext() as ctx:
    ctx.prec = 10
    print(c / d)

nums = [1.23e+18, 1, -1.23e+18]
sum(nums)

math.fsum(nums)

# %%
# Formatting numbers for output
x = 1234.56789

format(x, '0.2f')
format(x, '>10.1f')
format(x, '<10.1f')
format(x, '^10.1f')
format(x, ',')
format(x, '0,.1f')

format(x, 'e')
format(x, '0.2E')
'The value is {:0,.2f}'.format(x)

x
format(x, '0.1f')
format(-x, '0.1f')

swap_seperators = {ord('.'): ',', ord(','): '.'}
format(x, ',').translate(swap_seperators)

'%0.2f' % x
'%10.1f' % x
'%-10.1f' % x

# %%
# Working with binary , octal, and hexadecimal
x = 1234
bin(x)
oct(x)
hex(x)

format(x, 'b')
format(x, '0')
format(x, 'x')

x = -1234
format(x, 'b')
format(x, 'x')

x
format(2 ** 32 + x, 'b')
format(2 ** 32 + x, 'x')

int('4d2', 16)
int('10011010010', 2)

# %%
# Packing and Unpacking Large Integers from Bytes
data = b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
len(data)
int.from_bytes(data, 'little')
int.from_bytes(data, 'big')

x = 94522842520747284487117727783387188
x.to_bytes(16, 'big')
x.to_bytes(16, 'little')

data
hi, lo = struct.unpack('>QQ', data)
(hi << 64) + lo

# %%
# Performing complex valued math
a = complex(2, 4)
a
b = 3 - 5j
a.real
a.imag
a.conjugate()

a + b
a - b
a * b
a / b
abs(a)

cmath.sin(a)
cmath.exp(a)

a = np.array([2+3j, 4+5j, 6-7j, 8+9j])
a + 2
np.sin(a)

cmath.sqrt(-1)

# %%
# Working with infinity and nans
a = float('inf')
b = float('-inf')
c = float('nan')
a, b, c
math.isinf(a)
math.isnan(c)
math.isinf(b)

a + 45
a * 45
10 / a

a/a
a + b

c + 23
c/23
c * 23
math.sqrt(c)

d = float('nan')
c == d
c is d

# %%
# Calculating with fractions
a = Fraction(5, 4)
b = Fraction(7, 16)
print(a+b)
print(a*b)
c = a*b
c.numerator
c.denominator

float(c)
print(c.limit_denominator(8))

x = 3.75
y = Fraction(*x.as_integer_ratio())
y

# %%
# Calculating with large numerical arrays
x = [1, 2, 3, 4]
y = [5, 6, 7, 8]
x * 2
x + y

ax = np.array([1, 2, 3, 4])
ay = np.array([5, 6, 7, 8])
ax * 2
ax + 10
ax + ay
ax / ay


def f(x):
    """Function."""
    return 3*x**2 - 2*x + 7


f(ax)
np.sqrt(ax)
np.sin(ax)

grid = np.zeros(shape=(10000, 10000), dtype=float)
grid += 10
np.sin(grid)

a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
a[1]
a[:, 1]
a[1:3, 1:3] += 10
a
np.where(a < 10, a, 10)

# %%
# Performing Matrix and linear algebra calculations
m = np.matrix([[1, -2, 3], [0, 4, 5], [7, 8, -9]])
m.T  # transpose of a matrix
m.I  # Inverse of a mtrix
v = np.matrix([[2], [3], [4]])
m * v

numpy.linalg.det(m)
numpy.linalg.eigvals(m)
x = numpy.linalg.solve(m, v)
x
m * x
v

# %%
# Picking Things at Random
values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
random.choice(values)
random.choice(values)

random.sample(values, 2)
random.sample(values, 2)

random.shuffle(values)
values
random.shuffle(values)
values

random.randint(0, 10)
random.randint(0, 10)

random.random()
random.random()

random.getrandbits(100)

# %%
# Converting Days to Seconds, and Other Basic Time Conversions
a = timedelta(days=2, hours=6)
b = timedelta(hours=4.5)
c = a + b
c.days
c.seconds
c.seconds / 3600
c.total_seconds() / 3600

a = datetime(2012, 9, 1)
print(a + timedelta(days=10))

b = datetime(2012, 10, 1)
d = b - a
d.days

now = datetime.today()
print(now)
print(now + timedelta(minutes=10))

a = datetime(2012, 3, 1)
b = datetime(2012, 2, 28)
(a-b).days

c = datetime(2013, 3, 1)
d = datetime(2013, 2, 28)
(c-d).days

a = datetime(2012, 9, 23)
a + relativedelta(months=+1)
a + relativedelta(months=+4)

b = datetime(2012, 12, 21)
d = b - a
d = relativedelta(b, a)
d.months
d.days

# %%
# Detetmining last fridays date
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
            'Friday', 'Saturday', 'Sunday']


def get_previous_byday(dayname, start_date=None):
    """Get previous byday function."""
    if start_date is None:
        start_date = datetime.today()
    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7
    if days_ago == 0:
        days_ago = 7
    target_date = start_date - timedelta(days=days_ago)
    return target_date


datetime.today()
get_previous_byday('Monday')
get_previous_byday('Tuesday')
get_previous_byday('Sunday', datetime(2012, 12, 21))

d = datetime.now()
d
print(d + relativedelta(weekday=FR))
print(d + relativedelta(weekday=FR(-1)))
