"""Linked List implementation in Python."""


class LinkedCell:
    """Node implementation for linked list."""

    def __init__(self, v, n=-1):
        """Construct for LinkedCell."""
        self.value = v
        self.next = n
        self.length = 1

    def prepend(self, v):
        """Prepend given value to the linked list."""
        newc = LinkedCell(v)
        newc.next = self
        newc.length = self.length + 1
        return newc

    def len(self):
        """Calculate length of the linked list."""
        length = 0
        n = self
        while n != -1:
            length = length + 1
            n = n.next
        return length

    def seek(self, i):
        """Return the value at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        return n.value

    def traverse(self):
        """Traverse the linked list and print node contents."""
        n = self
        while n != -1:
            (print('value: {}, next: {}, length: {}'
                   .format(n.value, n.next, n.length)))
        n = n.next

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self
        while n != -1:
            print('{} -> '.format(n.value), end='')
            n = n.next
        print('End')

    def assign(self, nv, i):
        """Assign a value at particular index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        n.value = nv
        return self


class TestLinkedCell(object):
    """Test suite for LinkedCell."""

    def test_assign(self):
        """Test assign() functionality."""
        ll = LinkedCell(5)
        ll = ll.prepend(6)
        ll = ll.prepend(12)
        ll = ll.prepend(76)
        ll = ll.prepend(1)
        ll = ll.assign(0, 0)
        ll = ll.assign(1, 1)
        ll = ll.assign(2, 2)
        assert ll.seek(0) == 0
        assert ll.seek(1) == 1
        assert ll.seek(2) == 2
        assert ll.seek(3) == 6
        assert ll.seek(-1) == -1
        assert ll.seek(100) == -1
        assert ll.seek(-5) == -1
