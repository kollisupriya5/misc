"""Function to find the kth element in the list using recursion."""


def kth_element(k, list):
    """Kth element in th list."""
    if len(list) >= 1:
        if len(list) > k:
            return list[k]
        return kth_element(k, list[1:])
    return None


def test_kthelement1():
    """Test for kth element in the list."""
    assert kth_element(2, [1, 2, 3, 4, 5]) == 3


def test_kthelement2():
    """Test for kth element in the list."""
    assert kth_element(2, ['a', 'b', 'c', 'd']) == 'c'


def test_kthelement3():
    """Test for empty list."""
    assert kth_element(2, []) is None


def test_kthelement4():
    """Test with single list."""
    assert kth_element(2, [1]) is None


def test_kthelement5():
    """Test with single list."""
    assert kth_element(2, ['a']) is None


def test_kthelement6():
    """Test for kth element in the list."""
    assert kth_element(2, ['a', 1, 'c', 3]) == 'c'


def test_kthelement7():
    """Test for kth element in the list."""
    assert kth_element(4, [123, 456, '1bc', 'abc', 'cd6']) == 'cd6'


def test_kthelement8():
    """Test for kth element in the list."""
    assert kth_element(4, ['abc', 'a', 'def', 'b', 123, 1, 'hi1']) == 123
