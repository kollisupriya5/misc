"""Function for bubble sort."""


def bubble_sort(list):
    """Bubble sort."""
    if len(list) == 0:
        return None
    for i in range(len(list)):
        for j in range(1, len(list)):
            if list[j-1] > list[j]:
                list[j-1], list[j] = list[j], list[j-1]
    return list


def test_bubblesort1():
    """Test case for bubble sort."""
    assert bubble_sort([1, 5, 2, 3, 7, 8]) == [1, 2, 3, 5, 7, 8]


def test_bubblesort2():
    """Test with empty list."""
    assert bubble_sort([]) is None


def test_bubblesort3():
    """Test case for bubble sort."""
    assert bubble_sort([-1, -5, 2, -3, 7, 8]) == [-5, -3, -1, 2, 7, 8]


def test_bubblesort4():
    """Test with single element."""
    assert bubble_sort([1]) == [1]
