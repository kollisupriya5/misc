"""Python learning book chapter 28."""

# %%
# Step1: Making instances


class person:
    """Implementing class."""

    def __init__(self, name, job, pay):
        """Constuctor."""
        self.name = name
        self.job = job
        self.pay = pay


bob = person('Bob Smith', None, 0)
bob.name, bob.pay
sue = person('Sue Jones', job='dev', pay=100000)
sue.name, sue.pay, sue.job

# %%
# step2:Adding behaviour methods


class person:
    """Implementing class."""

    def __init__(self, name, job=None, pay=0):
        """Constuctor."""
        self.name = name
        self.job = job
        self.pay = pay

    if __name__ == '__main__':
        bob = person('Bob Smith', None, 0)
        sue = person('Sue Jones', job='dev', pay=100000)
        print(bob.name, bob.pay)
        print(sue.name, sue.pay)
        print(bob.name.split()[-1])
        sue.pay *= 1.10
        print('%.2f' % sue.pay)

# %%
# Step3.Operator Overloading


class person:
    """Implementing class."""

    def __init__(self, name, job=None, pay=0):
        """Constuctor."""
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        """LastName."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """GiveRaise."""
        self.pay = int(self.pay * (1 + percent))

    def __repr__(self):
        """Added method."""
        return '[person: %s, %s]' % (self.name, self.pay)


if __name__ == '__main__':
    bob = person('Bob Smith')
    sue = person('Sue Jones', job='dev', pay=100000)
    print(bob)
    print(sue)
    print(bob.lastName(), sue.lastName())
    sue.giveRaise(.10)
    print(sue)


# %%
# Step4.customising Behaviour by Subclassing

class person:
    """Implementing class."""

    def __init__(self, name, job=None, pay=0):
        """Constuctor."""
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        """LastName."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """GiveRaise."""
        self.pay = int(self.pay * (1 + percent))

    def __repr__(self):
        """Added method."""
        return '[person: %s, %s]' % (self.name, self.pay)


class Manager(person):
    """Implementing Manager class."""

    def giveRaise(self, percent, bonus=.10):
        """Added bonus."""
        person.giveRaise(self, percent + bonus)


if __name__ == '__main__':
    bob = person('Bob Smith')
    sue = person('Sue Jones', job='dev', pay=100000)
    print(bob)
    print(sue)
    print(bob.lastName(), sue.lastName())
    sue.giveRaise(.10)
    print(sue)
    tom = Manager('Tom Jones', 'mgr', 50000)
    tom.giveRaise(.10)
    print(tom.lastName())
    print(tom)


# %%
# Step5.Customising constructor , Too

class person:
    """Implementing class."""

    def __init__(self, name, job=None, pay=0):
        """Constuctor."""
        self.name = name
        self.job = job
        self.pay = pay

    def lastName(self):
        """LastName."""
        return self.name.split()[-1]

    def giveRaise(self, percent):
        """GiveRaise."""
        self.pay = int(self.pay * (1 + percent))

    def __repr__(self):
        """Added method."""
        return '[person: %s, %s]' % (self.name, self.pay)


class Manager(person):
    """Implementing Manager class."""

    def __init__(self, name, pay):
        """Constuctor with mgr."""
        person.__init__(self, name, 'mgr', pay)

    def giveRaise(self, percent, bonus=.10):
        """Added bonus."""
        person.giveRaise(self, percent + bonus)


if __name__ == '__main__':
    bob = person('Bob Smith')
    sue = person('Sue Jones', job='dev', pay=100000)
    print(bob)
    print(sue)
    print(bob.lastName(), sue.lastName())
    sue.giveRaise(.10)
    print(sue)
    tom = Manager('Tom Jones',  50000)
    tom.giveRaise(.10)
    print(tom.lastName())
    print(tom)


class Department:
    """Implement department class."""

    def __init__(self, *args):
        """Department constructor."""
        self.members = list(args)

    def addMember(self, person):
        """Implement addmember function."""
        self.members.append(person)

    def giveRises(self, percent):
        """Implement give rise method."""
        for person in self.members:
            person.giveRaise(percent)

    def showAll(self):
        """Implement Showall method."""
        for person in self.members:
            print(person)


if __name__ == '__main__':
    bob = person('Bob Smith')
    sue = person('Sue Jones', job='dev', pay=100000)
    tom = Manager('Tom jones', 50000)

    development = Department(bob, sue)
    development.addMember(tom)
    development.giveRises(0.10)
    development.showAll()
