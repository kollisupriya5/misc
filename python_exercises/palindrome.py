"""Function to check whether the list is a palindrome or not."""


def reverse(list):
    """Reverse of a list."""
    res = []
    for i in range(1, len(list)+1):
        res.append(list[-i])
    return res


def ispalindrome(list):
    """Is palindrome or not."""
    if len(list) > 0:
        if list == reverse(list):
            return True
        return False
    return 'empty list'


def test_ispalindrome1():
    """Test for list is palindrome or not."""
    assert ispalindrome([1, 2, 3, 4, 5]) == False


def test_ispalindrome2():
    """Test with empty list."""
    assert ispalindrome([]) == 'empty list'


def test_ispalindrome3():
    """Test with single element."""
    assert ispalindrome([1]) == True


def test_ispalindrome4():
    """Test for list is palindrome or not."""
    assert ispalindrome([1, 1]) == True


def test_ispalindrome5():
    """Test with single element."""
    assert ispalindrome([1, 1]) == True
