"""Practicing matplotlib."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib
# Using style sheets
plt.style.use('ggplot')
data = np.random.randn(50)
print(plt.style.available)

# Temporary styling
with plt.style.context(('dark_background')):
    plt.plot(np.sin(np.linspace(0, 2 * np.pi)), 'r-o')
plt.show()

# Dynamic rc settings
mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['lines.color'] = 'r'
plt.plot(data)

mpl.rc('lines', linewidth=4, color='g')
plt.plot(data)

matplotlib.matplotlib_fname()
