"""Practicing pandas split function."""

import pandas as pd
import numpy as np

# %%
s = pd.Series(["this is good text", "but this is even better"])
s.str.split()
s.str.split("random")
s.str.split(expand=True)
s.str.split("is", expand=True)

# %%
i = pd.Index(["ba 100 001", "ba 101 002", "ba 102 003"])
i
i.str.split(expand=True)

# %%
s.str.split("is", n=1)
s.str.split("is", n=1, expand=True)

# %%
s = pd.Series(["this is good text", "but this is even better", np.nan])
s.str.split(n=3, expand=True)

# %%
s = pd.Series(['Hai hello good evening', 'good afternoon everyone', 'good'])
s.str.split()
s.str.split(expand=True)
s.str.split(n=2)
