"""Implement doubly_linked list in python."""


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.value = v
        self.next = next
        self.prev = prev


class DoublyLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self, head=None, tail=None, length=0):
        """Construct a doubly-linked list."""
        self.head = head
        self.tail = tail
        self.length = length

    def is_empty(self):
        """Return True if the list is empty; False otherwise."""
        return self.length == 0

    def prepend(self, v):
        """Prepend given value to the linked list."""
        new_cell = Node(v, self.head, None)

        # prepend when the list is empty
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self

        # prepend when the list is not empty
        self.head.prev = new_cell
        self.head = new_cell
        self.length = self.length + 1
        return self

    def seek_from_end(self, i):
        """Return value at index i from the end of the list."""
        n = self.tail
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.prev
        return n.value

    def assign(self, nv, i):
        """Assign a value at particular index."""
        n = self.head
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i-1
            n = n.next
        n.value = nv

    def assign_from_end(self, nv, i):
        """Assign a value at particular index."""
        n = self.tail
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i-1
            n = n.prev
        n.value = nv

    def insert(self, v, i):
        """Insert a value at particular index."""
        n = self.head
        new_cell = Node(v, None, None)
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        new_cell.next = n.next
        new_cell.prev = n
        n.next = new_cell
        new_cell.next.prev = new_cell
        self.length = self.length+1

    def append(self, v):
        """Append for doublelinkedlist."""
        n = self.tail
        new_cell = Node(v, None, n)
        if self.is_empty():
            self.head = new_cell
            self.tail = new_cell
            self.length = 1
            return self
        n.next = new_cell
        self.tail = new_cell
        self.length = self.length+1

    def range(self, i, j):
        """Create a list containing all integers within a given range."""
        ll = DoublyLinkedList()
        for n in range(i, j+1):
            ll.append(n)
        return ll

    def simple_traverse(self):
        """Traverse and print only node values."""
        n = self.head
        print('Head->', end='')
        while n != -1:
            print('{} -> '.format(n.value), end='')
            n = n.next
        print('Tail')

    def simple_traverse_from_end(self):
        """Traverse and print only node values."""
        n = self.tail
        print('tail->', end='')
        while n != -1:
            print('{} -> '.format(n.value), end='')
            n = n.prev
        print('Head')


class TestDoublyLinkedList(object):
    """Test doubly-linked list."""

    def test_creation(self):
        """Test __init()__ functionality."""
        n = Node(5)
        ll = DoublyLinkedList(n, length=1)
        assert ll.length == 1
        ll.prepend(6)
        ll.prepend(19)
        ll.prepend(121)
        assert ll.length == 4
        ll2 = DoublyLinkedList()
        assert ll2 is not None
        assert ll2.length == 0
        assert ll2.head is None
        assert ll2.tail is None
        ll2.prepend(8)
        assert ll2.head is not None
        assert ll2.tail is not None
        assert ll2.length == 1
        assert ll2.head.value == 8
        assert ll2.tail.value == 8
        assert ll2.head.next is None
        assert ll2.tail.next is None
        assert ll2.head.prev is None
        assert ll2.tail.prev is None
        ll2.prepend(9)
        assert ll2.length == 2
        assert ll2.head.value == 9
        assert ll2.tail.value == 8
        assert ll2.head.next is not None
        assert ll2.tail.next is None
        assert ll2.head.prev is None
        assert ll2.tail.prev is not None
        assert ll2.head.next.value == 8
        assert ll2.tail.prev.value == 9

    def test_seek_from_end(self):
        """Test seek_from_end() functionality."""
        ll2 = DoublyLinkedList()
        ll2.prepend(5)
        ll2.prepend(6)
        ll2.prepend(12)
        ll2.prepend(76)
        assert ll2.seek_from_end(0) == 5
        assert ll2.seek_from_end(1) == 6
        assert ll2.seek_from_end(2) == 12
        assert ll2.seek_from_end(3) == 76
        assert ll2.seek_from_end(101) == -1
        assert ll2.seek_from_end(5) == -1

    def test_assign(self):
        """Test assign() functionality."""
        ll2 = DoublyLinkedList()
        ll2.prepend(5)
        ll2.prepend(6)
        ll2.prepend(12)
        ll2.prepend(76)
        ll2.prepend(25)
        ll2.assign(4, 3)
        assert ll2.seek_from_end(1) == 4
        ll2.assign(15, 1)
        assert ll2.seek_from_end(3) == 15
        ll2.assign(20, 0)
        assert ll2.seek_from_end(4) == 20
        assert ll2.seek_from_end(10) == -1
        assert ll2.seek_from_end(-1) == -1

    def test_assign_from_end(self):
        """Test assign_from_end() functionality."""
        ll2 = DoublyLinkedList()
        ll2.prepend(5)
        ll2.prepend(6)
        ll2.prepend(12)
        ll2.prepend(76)
        ll2.prepend(25)
        ll2.assign_from_end(15, 0)
        assert ll2.seek_from_end(0) == 15
        ll2.assign_from_end(66, 1)
        assert ll2.seek_from_end(1) == 66
        assert ll2.seek_from_end(4) == 25
        assert ll2.seek_from_end(-1) == -1
        assert ll2.seek_from_end(100) == -1

    def test_insert(self):
        """Test insert() functionality."""
        ll2 = DoublyLinkedList()
        ll2.prepend(5)
        ll2.prepend(6)
        ll2.prepend(12)
        ll2.prepend(76)
        ll2.prepend(25)
        assert ll2.seek_from_end(2) == 12
        assert ll2.length == 5
        ll2.insert(10, 2)
        assert ll2.length == 6

    def test_range(self):
        """Test range() functionality."""
        ll = DoublyLinkedList()
        ll.range(2, 5)
        assert ll.head.value == 2
        assert ll.tail.value == 5


def main():
    """Try printing functionality."""
    ll = DoublyLinkedList()
    ll.append(6)
    ll.append(12)
    ll.append(76)
    ll.simple_traverse()


main()
