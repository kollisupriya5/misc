"""Implementing stacks and queues."""


# %%
class stack:
    """Stack implementing in python."""

    def __init__(self, size):
        """Stack constructor."""
        self.list = [None] * size
        self.top = 0

    def stack_empty(self):
        """Implement stack empty."""
        if self.top == 0:
            return True
        else:
            return False

    def push(self, x):
        """Implemnet push."""
        self.top = self.top+1
        self.list[self.top] = x

    def pop(self):
        """Implement pop."""
        if self.top == 0:
            return "underflow"
        else:
            self.top = self.top-1
            return self.list[self.top+1]


s = stack(10)
s.stack_empty()
s.push(10)
s.stack_empty()
s.push(20)
s.push(30)
s.pop()


# %%
class queue:
    """Queue implementation in python."""

    def __init__(self, size):
        """Queue constructor."""
        self.size = size
        self.list = [None] * size
        self.head = 0
        self.tail = 0
        self.length = size

    def enqueue(self, x):
        """Implement Enqueue."""
        self.list[self.tail] = x
        if self.tail == self.length:
            self.tail = 1
        else:
            self.tail = self.tail + 1

    def dequeue(self):
        """Implement Dequeue."""
        if self.list[self.head] is not None:
            x = self.list[self.head]
            if self.head == self.length:
                self.head = 1
            else:
                self.head = self.head + 1
            return x
        else:
            return "underflow"

    def enqueue1(self, x):
        """Implement Enqueue."""
        if self.list[self.tail] is None:
            self.list[self.tail] = x
            if self.tail == self.length - 1:
                self.tail = 0
            else:
                self.tail = self.tail + 1
        else:
            return "overflow"


q = queue(5)
q.dequeue()
q.enqueue1(71)
q.enqueue1(7)
q.enqueue1(10)
q.enqueue1(1)
q.enqueue1(1)
q.enqueue1(1)
q.dequeue()
