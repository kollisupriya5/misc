"""Python learning textbook chapter19."""


# %%
# Anonymous Functions : Lambda
def func(x, y, z):
    """Def statement."""
    return x + y + z


func(1, 2, 3)


# %%
f = lambda x, y, z: x + y + z
f(1, 2, 3)

x = (lambda a="fee", b="fie", c="foe": a + b + c)
x("wee")
x("one")


# %%
def knights():
    """Function."""
    title = 'Sir'
    action = (lambda x: title + " " + x)
    return action
 act =knights()
 msg = act('robbin')
 msg
 act


 # %%
 # Why use lambdas?
L = [lambda x: x ** 2,
     lambda x: x ** 3,
     lambda x: x ** 4]

for f in L:
    print(f(2))

print(L[0](3))


# %%
def f1(x): return x ** 2
def f2(x): return x ** 3
def f3(x): return x ** 4

for f in L:
    print(f(2))

L = [f1, f2, f3]
print(L[0](3))


# %%
# Multiway branch switches: The finale
key = 'got'
{'already': (lambda: 2 + 2),
 'got': (lambda: 2 * 4),
 'one': (lambda: 2 ** 6)}[key]()

def f1(): return 2 + 2
def f2(): return 2 * 4
def f3(): return 2 ** 6

key = 'one'
{'already': f1, 'got': f2, 'one': f3}[key]()


# %%
lower = (lambda x, y: x if x < y else y)
lower('bb', 'aa')
lower('aa', 'bb')
lower(1, 2)
lower(6, 5)


# %%
import sys
showall = lambda x: list(map(sys.stdout.write, x))
t = showall(['spam\n', 'toast\n', 'eggs\n'])

showall = lambda x: [sys.stdout.write(line) for line in x]
t = showall(('bright\n', 'side\n', 'of\n', 'life\n'))

showall = lambda x: [print(line, end='') for line in x]
showall = lambda x: print(*x, sep='', end='')


# %%
# Scopes : Lambdas can be nested too
def action(x):
    return (lambda y: x + y)

act = action(99)
act(11)

action = (lamda x: (lambda y: x + y))
act = action(99)
act(3)

((lambda x: (lambda y: x + y))(99))(4)


# %%
# Functional programming Tools
counters = [1, 2, 3, 4]
updated = []
for x in counters:
    updated.append(x + 10)
updated

def inc(x):
    return x + 10

list(map(inc, counters))

list(map((lambda x: x + 3), counters))

def mymap(f, seq):
    res = []
    for x in seq: res.append(f(x))
    return res

list(map(inc, [1, 2, 3]))
mymap(inc, [4, 5, 6])
mymap(inc, [1, 2, 3])

pow(3, 4)
list(map(pow, [1, 2, 3], [2, 3, 4]))
list(map(inc, [1, 2, 3, 4]))
[inc(x) for x in [1, 2, 3, 4]]

# %%
# Selecting the items in iterables:filter
list(range(-5, 5))
list(filter((lambda x: x > 0), range(-5, 5)))

res = []
for x in range(-5, 5):
    if x > 0:
        res.append(x)
res

[x for x in range(-5, 5) if x > 0]

# %%
# Combining items in iterables : reduce
from functools import reduce
reduce((lambda x, y: x + y), [1, 2, 3, 4])
reduce((lambda x, y: x * y), [1, 2, 3, 4])

L = [1, 2, 3, 4]
res = L[0]
for x in L[1:]:
    res = res + x

res

# %%
def myreduce(function, sequence):
    tally = sequence[0]
    for next in sequence[1:]:
        tally = function(tally, next)
    return tally

myreduce((lambda x, y: x + y), [1, 2, 3, 4, 5])
myreduce((lambda x, y: x * y), [1, 2, 3, 4, 5])

import operator, functools
functools.reduce(operator.add,[2, 4, 6])
functools.reduce((lambda x, y: x + y), [2, 4, 6])
