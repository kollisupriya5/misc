
# coding: utf-8

# In[1]:

def sum(lst):
    s = 0
    for e in lst:
        s = s+e
    return s
sum([1,2,3,4])


# In[13]:

def sub(x):
    def square(y):
        return (x-y) ** 2
    return square
sub(5)(2)


# In[3]:

def sqrt(x):
    return x**(0.5)
sqrt(9)


# In[10]:

import math
def euclidean1(p,q):
    res1 = []
    #assert(len(p)==len(q))
    for e in range(len(p)):
        res1.append(sub(p[e])(q[e]))
    return math.sqrt(sum(res1))
euclidean1((1,2),(3,4))


# In[7]:

def manhattan1(p,q):
    res1=[]
    #assert(len(p)==len(q))
    for e in range(len(p)):
        res1.append(abs(p[e]-q[e]))
    return sum(res1)
manhattan1((2,3),(4,1)) 


# In[15]:

def pearson1(p,q):
    n=len(p)
    res=[]
    res1=[]
    res2=[]
    #assert(len(p)==len(q))
    for e in range(len(p)):
        res.append(p[e]*q[e])
    r = (n*(sum(res)))-(sum(p)*sum(q))
    for i in range(len(p)):
        res1.append(p[i]**2)
    d1=(n*(sum(res1)))-(sum(p)*sum(p))
    for j in range(len(q)):
        res2.append(q[j]**2)
    d2=(n*(sum(res2)))-(sum(q)*sum(q))
    d = sqrt(d1*d2)
    return (r/d)
pearson1((63,68,86,72,73,74),(87,100,110,111,70,79))


# In[11]:

def chebyshev(p,q):
    res=[]
    for e in range(len(p)):
        res.append(abs(p[e]-q[e]))
    return max(res)
chebyshev([1,2,3,4],[5,7,2,3])

