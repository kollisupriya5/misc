"""Implementing binary search for linked list."""


# %%
class Linkedcell:
    """Node implmentation for linked cell."""

    def __init__(self, v, n=1):
        """Construct for linkedcell."""
        self.value = v
        self.next = n
        self.length = 1

    def prepend(self, v):
        """Prepend given value to the linked list."""
        newc = Linkedcell(v)
        newc.next = self
        newc.length = self.length + 1
        return newc

    def seek(self, i):
        """Return the value at index."""
        n = self
        if i < 0 or i >= self.length:
            return -1
        while i > 0:
            i = i - 1
            n = n.next
        return n.value

    def binary_search(self, element):
        """Binary search for linked list."""
        low = 0
        high = self.length
        while(high >= low):
            middle = (high + low)//2
            if element == self.seek(middle):
                return middle
            if element < self.seek(middle):
                high = middle - 1
            else:
                low = middle + 1
        return "Not found"


ll = Linkedcell(5)
ll = ll.prepend(4)
ll = ll.prepend(3)
ll = ll.prepend(2)
ll = ll.prepend(1)
ll.binary_search(4)
ll.binary_search(10)
