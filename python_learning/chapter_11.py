"""Python learning textbook chapter10."""

# %%
# Assignment Statement Forms
spam = 'Spam'  # Basic form
spam
spam, ham = 'yum', 'YUM'  # Tupple assignment
spam, ham
[spam, ham] = ['yum', 'YUM']  # List assignment
[spam, ham]
a, b, c, d = 'spam'  # Sequence Assignment
a
a, *b = 'spam'
a, *b
spam = ham = 'lunch'  # Multi target Assignment
spam, ham

# %%
# Sequence Assignments
nudge = 1
wink = 2
A, B = nudge, wink
A, B
[C, D] = [nudge, wink]
C, D
nudge, wink = wink, nudge
nudge, wink
[a, b, c] = (1, 2, 3)
a, c
(a, b, c) = 'abc'
a, c
string = 'SPAM'
a, b, c, d = string
a, b, c = string
a, b, c = string[0], string[1], string[3]
a, b, c
a, b, c = list(string[:2]) + [string[2:]]
a, b, c
(a, b), c = string[:2], string[2:]  # Nested sequences
a, b, c
(a, b) = 'SP'
a, b
red, green, blue = range(3)
red, green
L = [1, 2, 3, 4]
while L:
    front, L = L[0], L[1:]
    print(front, L)
seq = [1, 2, 3, 4]
a, b, c, d = seq
print(a, b, c, d)
a, b = seq
a, *b = seq
a
b
seq
a, *b, c = seq
a
b, c
a, *b = 'spam'
a, b
a, *b, c = 'spam'
a, b, c
a, *b, c = range(4)
a, b, c
S = 'spam'
S[0], S[1:3], S[3]
L = [1, 2, 3, 4]
while L:
    front, *L = L
    print(front, L)
seq
a, b, c, *d = seq
a, b, c, d
a, b, c, d, *e = seq
a, b, c, d, e
seq
a, b, c, *e, d = seq
a, b, c, d, e
a, *b, c, *d = seq
a, b = seq
*a, = seq
a
for all in [(1, 2, 3, 4), (5, 6, 7, 8)]:
    a, b, c = all[0], all[1:3], all[3]
    print(a, b, c)
a = b = c = 'spam'
a, b, c
b = 'spam1'
c = b
a = c
a
a = b = 0
b = b + 1
a, b
a = b = []
b.append(42)
a, b
a = []
b = []
b.append(42)
a,b

# %%
# Agumented Assignments
x = 1
x = x + 1
x
x = 1
x += 1
x
S = "spam"
S += "SPAM"
S
L = [1, 2]
L = L + [3]
L
L.append(4)
L
L = L + [5, 6]
L
L.extend([7, 8, 9])
L
L += [9, 10]
L
L = []
L += 'spam'
L = L + 'spam'
L = [1, 2]
M = L
L = L + [3, 4]
L, M
L = [1, 2]
M = L
L += [3, 4]
L, M
x = print('spam')
print(x)
L = [1, 2]
L.append(3)
print(L)
L = L.append(4)
print(L)
x = 'spam'
y = 99
z = ['eggs']
print(x, y, z, sep='')
print(x, y, z, sep=' , ')
print(x, y, z, end='')
print(x, y, z, end=''); print(x, y, z)
print(x, y, z, end='...\n')
print(x, y, z, sep='...', end='!\n')
print(x, y, z, end='!\n', sep='...')
print(x, y, z, sep='...', file=open('data.txt', 'w'))
print(open('data.txt').read())
text = '%s: %-.4f, %05d' % ('Result', 3.14159, 42)
print(text)
print('%s: %-.4f, %05d' % ('Result', 3.14159, 42))
x = 'a'
y = 'b'
print (x, y)
print('hello world')
'hello world'
