"""Controlling figure aesthetics."""

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# %%
def sinplot(flip=1):
    """Sinplot."""
    x = np.linspace(0, 14, 100)
    for i in range(1, 7):
        plt.plot(x, np.sin(x + 1 * 0.5) * (7 - i) * flip)


sinplot()


# %%
sns.set()
sinplot()

# %%
# Seaborn figure styles
sns.set_style('whitegrid')
data = np.random.normal(size=(20, 6))+np.arange(6) / 2
sns.boxplot(data=data)

# %%
sns.set_style('dark')
sinplot()

sns.set_style('white')
sinplot()

sns.set_style('ticks')
sinplot()

# %%
# Removing axis spines
sinplot()
sns.despine()

# %%
f, ax = plt.subplots()
sns.violinplot(data=data)
sns.despine(trim=True, offset=10)

# %%
sns.set_style('whitegrid')
sns.boxplot(data=data, palette='deep')
sns.despine(left=True)

# %%
# Temporarily setting figure style
with sns.axes_style('darkgrid'):
    plt.subplot(2, 1, 1)
    sinplot()
plt.subplot(2, 1, 2)
sinplot(-1)

# %%
# Overriding elements of the seaborn styles
sns.axes_style()

sns.set_style('darkgrid', {'axes.facecolor': "pink"})
sinplot()

# %%
# Scaling plot elements
sns.set()
sinplot()

# %%
sns.set_context('paper')
sinplot()

# %%
sns.set_context('talk')
sinplot()

# %%
sns.set_context('poster')
sinplot()

# %%
sns.set_context('notebook')
sinplot()

# %%
sns.set_context('notebook', font_scale=1.5, rc={'lines.linewidth':2.5})
sinplot()
