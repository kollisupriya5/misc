"""Function for merge sort."""


def insertion_sort(list):
    """Sort using insertion."""
    if len(list) == 0:
        return None
    for i in range(1, len(list)):
        i = i+1
        j = i-1
        while j > 0:
            if list[j-1] > list[j]:
                list[j-1], list[j] = list[j], list[j-1]
            j = j-1
    return list


def merge(lst1, lst2):
    """Merge two sorted lists."""
    res = []
    i = 0
    j = 0
    while i < len(lst1) and j < len(lst2):
        if lst1[i] > lst2[j]:
            res.append(lst2[j])
            j = j+1
        else:
            res.append(lst1[i])
            i = i+1
    if i >= len(lst1):
        res = res+lst2[j:]
    else:
        res = res+lst1[i:]
    return res


def merge_recursion(lst, l, m, r):
    """Merge sort using recursion."""
    lst1 = lst[l:m]
    lst2 = lst[m:r]
    if len(lst1) > 1:
        lst1 = insertion_sort(lst1)
    if len(lst2) > 1:
        lst2 = insertion_sort(lst2)
    return merge(lst1, lst2)


def merge_sort(lst):
    """Merge sort for a list."""
    if len(lst) == 0:
        return None
    l = 0
    m = len(lst)//2
    r = len(lst)
    return merge_recursion(lst, l, m, r)


def test_mergesort1():
    """Test for sorting elements in the list."""
    assert merge_sort([1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_mergesort2():
    """Test for sorting elements in the list."""
    assert merge_sort([1, 1]) == [1, 1]


def test_mergesort3():
    """Test with empty list."""
    assert merge_sort([]) is None


def test_mergesort4():
    """Test for sorting elements in the list."""
    assert merge_sort([1, 5, 2, 4, 3, 7, 6, 2]) == [1, 2, 2, 3, 4, 5, 6, 7]


def test_mergesort5():
    """Test with negative values."""
    assert merge_sort([1, 2, -4, 5, 0, -6]) == [-6, -4, 0, 1, 2, 5]
