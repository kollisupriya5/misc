"""Classes and objects."""

from datetime import date
import math
from abc import ABCMeta, abstractmethod


# %%
# Changing the string representation of instances
class pair:
    """Implementing class."""

    def __init__(self, x, y):
        """Values."""
        self.x = x
        self.y = y

    def __repr__(self):
        """String."""
        return'Pair({0.x!r}, {0.y!s})'.format(self)

    def __str__(self):
        """Change String."""
        return '({0.x!s}, {0.y!s})'.format(self)


p = pair(3, 4)
p
print(p)
print('p is {0!r}'.format(p))
print('p is {0}'.format(p))


# %%
# Customizing string format
_formats = {'ymd': '{d.year}-{d.month}-{d.day}',
            'mdy': '{d.month}/{d.day}/{d.year}',
            'dmy': '{d.day}/{d.month}/{d.year}'}


class Date:
    """Implementing date class."""

    def __init__(self, year, month, day):
        """Values."""
        self.year = year
        self.month = month
        self.day = day

    def __format__(self, code):
        """Format string."""
        if code == '':
            code = 'ymd'
        fmt = _formats[code]
        return fmt.format(d=self)


d = Date(2018, 6, 25)
format(d)
format(d, 'mdy')
format(d, 'dmy')
'The date is {:ymd}'.format(d)
'The date is {}'.format(d)

d = date(2018, 6, 25)
format(d)
format(d, '%A, %B %d, %Y')
'The end is {:%d %b %Y}.Goodbye'.format(d)


# %%
# Saving memory when creating a large no of instances
class Date:
    """Implementing date class."""

    __slots__ = ['year', 'month', 'day']

    def __init__(self, year, month, day):
        """Instances."""
        self.year = year
        self.month = month
        self.day = day


# %%
# Creating managed attributes
class person:
    """Imlementing person class."""

    def __init__(self):
        """constuctor."""
        self.first_name = ''

    def first_name(self):
        """Getter function."""
        return self.first_name

    def first_name(self, value):
        """Set name function."""
        if not isinstance(value, str):
            raise TypeError('Expected a string')
        self._first_name = value

    def first_name(self):
        """Delete name function."""
        raise AttributeError("cant delete attribute")


x = person()
x.first_name
x.first_name = 'abac'
x.fisrt_name


class Circle:
    """Implementing circle class."""

    def __init__(self, radius):
        """Constructor."""
        self.radius = radius

    @property
    def area(self):
        """Area of a square."""
        return math.pi * self.radius ** 2

    @property
    def perimeter(self):
        """Perimeter of a square."""
        return 2 * math.pi * self.radius


a = Circle(5)
a.area
a.perimeter


# %%
# Calling a method on a parent class
class A:
    """Implementing parent class."""

    def spam(self):
        """Function."""
        print('A.spam')


class B(A):
    """Calling parent spam."""

    def spam(self):
        """Function."""
        print('B.spam')
        super().spam()


A().spam()
B().spam()


class Base:
    """Implementing Base class."""

    def __init__(self):
        """Constructor."""
        print('Base.__init__')


class A(Base):
    """Class."""

    def __init__(self):
        """Constructor."""
        Base.__init__(self)
        print('A.__init__')


class B(Base):
    """Class."""

    def __init__(self):
        """Constructor."""
        Base.__init__(self)
        print('B.__init__')


class C(A, B):
    """Class."""

    def __init__(self):
        """Constructor."""
        A.__init__(self)
        B.__init__(self)
        print('C.__init__')


c = C()


# %%
# Defining an interface or abstract base class
class IStream(metaclass=ABCMeta):
    """Implementing class."""

    @abstractmethod
    def read(self, maxbytes=-1):
        """Read."""
        pass

    @abstractmethod
    def write(self, data):
        """Write."""
        pass
