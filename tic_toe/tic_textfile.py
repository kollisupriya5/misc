"""Tic tac toe game."""

import random


# %%
board = ['_'] * 9


def print_board(board):
    """Board creation."""
    print(board[0] + '|' + board[1] + '|' + board[2])
    print(board[3] + '|' + board[4] + '|' + board[5])
    print(board[6] + '|' + board[7] + '|' + board[8])


# %%
def player_input(player):
    """Player inputs."""
    if player == 'player 1':
        return ('X', 'O')
    else:
        return ('O', 'X')


# %%
def first_player():
    """Who plays first."""
    random_player = 'player {}'.format(random.randint(1, 2))
    return random_player


# %%
def marker_board(board, marker, position):
    """Place the marker on the board."""
    board[position] = marker


# %%
win_combinations = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7),
                    (2, 5, 8), (0, 4, 8), (2, 4, 6)]


def win_check(board):
    """Win checking."""
    return any(''.join([board[a],  board[b], board[c]]) in
               ['XXX', 'OOO'] for a, b, c in win_combinations)


# %%
def file():
    """Values from text file."""
    path = "/home/supriya/python/atom_notebooks/misc/tic_toe/tic_text.txt"
    with open(path, 'r') as f:
        data = f.read()
    x = data.splitlines()
    res = []
    for i in range(len(x)):
        res.append(int(x[i]))
    return res


# %%
def fill_check(board):
    """Check whether the board is filled or not."""
    return all(b != '_' for b in board)


# %%
def replay():
    """Replay."""
    play = input('Do you want to play again?: ')
    if play == 'yes':
        print('Welcome to Tic Tac Toe game')


# %%
def main():
    """Tic tac toe game."""
    board = ['_'] * 9
    first_turn = first_player()
    player1_marker, player2_marker = player_input(first_turn)
    print('{} got first chance to play'.format(first_turn))
    print_board(board)
    x = file()
    for position in x:
        if first_turn == 'player 1':
            print('Board after Player 1 turn')
            marker_board(board, player1_marker, position)
            if win_check(board):
                print_board(board)
                print('Congratulations Player1 won the game')
                break
            else:
                if fill_check(board):
                    print_board(board)
                    print('The game ends in a tie')
                    break
                else:
                    first_turn = 'player 2'
        else:
            print('Board after Player 2 turn')
            marker_board(board, player2_marker, position)
            if win_check(board):
                print_board(board)
                print('Congratulations Player2 won the game')
                break
            else:
                if fill_check(board):
                    print_board(board)
                    print('The game ends in a tie')
                    break
                else:
                    first_turn = 'player 1'
        print_board(board)
        print('\n')


main()
