import seaborn as sns

tips = sns.load_dataset("tips")
tips.head()

tips.pivot_table(index=['day', 'smoker'])
tips.pivot_table(['tip', 'size'], index=['time', 'day'], columns = 'smoker')
tips.pivot_table(['tip', 'size'], index=['time', 'day'], columns = 'smoker', margins=True)
tips.pivot_table('tip', index=['time', 'smoker'], columns = 'day', aggfunc=len, margins=True)
tips.pivot_table('tip', index=['time', 'size', 'smoker'], columns = 'day', aggfunc='mean', fill_value=0)
