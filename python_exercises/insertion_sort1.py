"""Insertion sort."""


def insertion_sort(lst):
    """To sort given list."""
    for j in range(1, len(lst)):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] > key:
            lst[i + 1] = lst[i]
            i = i - 1
        lst[i + 1] = key
    return lst


# %%
lst = [5, 2, 4, 3, 7, 1, 6, 2]
insertion_sort(lst)


# %%
def insertion_reverse(lst):
    """To sort given list in reverse order."""
    for j in range(1, len(lst)):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] < key:
            lst[i + 1] = lst[i]
            i = i - 1
        lst[i + 1] = key
    return lst


insertion_reverse([])


# %%
def linear_search(lst, v):
    """Implement linear search."""
    for i in range(1, len(lst)):
        if lst[i] == v:
            return i
    return None


linear_search([10, 20, 80, 30, 60, 50], 30)
linear_search([10, 20, 80, 30, 60, 50], 100)


# %%
