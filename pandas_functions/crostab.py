"""Practicing pandas crosstab function."""

import pandas as pd
import numpy as np

# %%
a = (np.array(["foo", "foo", "foo", "foo", "bar", "bar",
               "bar", "bar", "foo", "foo", "foo"], dtype=object))
b = (np.array(["one", "one", "one", "two", "one", "one",
              "one", "two", "two", "two", "one"], dtype=object))
c = (np.array(["dull", "dull", "shiny", "dull", "dull", "shiny",
               "shiny", "dull", "shiny", "shiny", "shiny"], dtype=object))

pd.crosstab(a, [b, c], rownames=['a'], colnames=['b', 'c'])
pd.crosstab(a, [b], rownames=['a'], colnames=['b'])
pd.crosstab(b, c, rownames=['b'], colnames=['c'])
pd.crosstab(b, [c, a], rownames=['b'], colnames=['c', 'a'])


# %%
d = np.array(["foo", "foo", "bar", "foo", "bar"])
e = np.array(["one", "two", "one", "two", "one"])
f = np.array(["dull", "shiny", "shiny", "shiny", "dull"])
pd.crosstab(d, [e, f], rownames=['d'], colnames=['e', 'f'])

# %%
foo = pd.Categorical(['a', 'b'], categories=['a', 'b', 'c'])
foo
bar = pd.Categorical(['d', 'e'], categories=['d', 'e', 'f'])
bar
pd.crosstab(foo, bar)

# %%
g = pd.Categorical([1, 2, 3], categories=[1, 2, 3])
h = pd.Categorical([4, 5, 6], categories=[4, 5, 6])
pd.crosstab(g, h, rownames=['g'], colnames=['h'])
i = np.array(["foo", "bar", "foo"], dtype=object)
pd.crosstab(i, g)
