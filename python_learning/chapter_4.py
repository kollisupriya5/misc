"""Python learning textbook chapter_4."""


import numpy as np
import pandas as pd
import math
import random
import re
import struct

# %%
print('hello world')
print(2 ** 100)

# %%
# Numbers
3.1415 * 2
print(3.1415 * 2)
123 + 222
1.5 * 4
2 ** 100
len(str(2 ** 100))
len(str(2 ** 1000000))

# %%
math.pi
np.pi
np.sqrt(25)
math.sqrt(25)

# %%
random.random()
random.choice([1, 2, 3, 4])

# %%
# Strings(sequence operations)
s = 'Spam'
len(s)
s[0]
s[3]
s[-1]
s[-2]
s[len(s)-1]
s[1:3]
s[:]
s[:-1]
s[:3]
s
s+'xyz'
s
'Spam'*8

# %%
# Immutability
s
s[0] = 'Z'
s = 'z'+s[1:]
s1 = list(s)
s1[0] = 'S'
''.join(s1)
s1

# %%
# Type-Specific Methods
s = 'Spam'
s.find('pa')
s.replace('pa', 'XYZ')
s
line = 'aaaa,bbbb,ccc,dd,ee'
line.split(',')
S = 'spam'
S.upper()
S.isalpha()
line = 'aaaa,bbbb,ccc,dd,ee\n'
line.rstrip().split(',')
'%s, eggs, and %s' % ('spam', 'SPAM!')
'%s, onion and %s' % ('bringal', 'Tomato')
'{}, eggs and {}'.format('spam', 'SPAM!')

 # %%
dir(s)
s+'abc'
s.__add__('abc')
help(s.capitalize)

# %%
# Other Ways to Code Strings
S = 'A\nB\tC'
S
len(S)
ord('\t')
S = 'A\0B\0C'
S
'sp\xc4m'
'Spam'.encode('utf8')
'Spam'.encode('utf16')

# %%
match = re.match('Hello[ \t]*(.*)world', 'Hello  Python world')
match.group(1)

# %%
# Lists(sequence operations)
L = [123, 'spam', 1.23]
len(L)
L+[1, 2, 3]
L * 2
L[1:]
L.append('NI')
L
L.pop(2)
L
line = ['bbbb', 'ccc', 'dd', 'aaaa', 'ee']
line.sort()
line
line.reverse()
line

# Bounds checking
L
L[99]
L[99] = 1

# %%
# Nesting
M = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]
M
M[1]
M[1][2]
cols = [row[1] for row in M]
cols
G = {sum(row) for row in M}
G
next(G)
list(map(sum, M))

# Mapping operations
D = {'food': 'Spam', 'quantity': 4, 'color': 'pink'}
D
D['food']
D1 = {}
D1['abc'] = 1
D1['def'] = 2
D1
D11 = dict(name='ABC', job='DEF')
D11
D22 = dict(zip(['name', 'job'], ['ABC', 'DEF']))
D22

# %%
rec = ({'name': {'first': 'Bob', 'last': 'Smith'},
       'jobs': ['dev', 'mgr'], 'age': 40.5}
rec['name']
rec['jobs'][-1]
rec['name']['last']
rec['age']
rec['jobs'].append('janitor')
rec
rec = 0
rec

# %%
# Missing Keys: if Tests
D = {'a': 1, 'b': 2, 'c': 3}
D
D['c']
D['e'] = 100
D
D['f']
'f' in D
if not 'f' in D:
    print('missing')
D.get('x', 0)
D['x'] if 'x' in D else 0
D

# %%
DS = D.keys()
Ds = list(DS)
Ds
for key in DS:
    print(key, '=>', D[key])
for c in 'spam':
    print(c.upper())
squares = [x ** 2 for x in [1, 2, 3, 4, 5]]
squares

# %%
T = (1, 2, 3, 4)
len(T)
T.index(4)
T.count(4)
T+(5,6,7,8)
T = T+(5, 6, 7, 8)
T = (2,)+T[1:]
T

# %%
# Files

f = open('data.txt', 'w')
f.write('Hello\n')
f.write('world\n')
f.close()
f = open('data.txt')
text = f.read()
text
print(text)
for line in open('data.txt'): print(line)
dir(f)
help(f.seek)
help(struct.pack)
packed = struct.pack('>i4sh', 7, b'spam', 8)
packed
