"""Python learning textbook chapter_6."""

import sys
# %%
# shared references
a = 4
b = a
a = 'Spam'
b  # b still references the original object, the integer 3 .

# %%
# Shared References and In-Place Changes
l1 = [1, 3, 5]
l2 = l1
l2
l1[0] = 24
l1
l2
l2 = l1[:]
l2
l1[0] = 1
l2

# %%
# Shared References and Equality
x = 42
x = 'shrubbery'
x
m = x
m
x == m
m is x
L = [1, 2, 3]
M = [1, 2, 3]
L == M
L is M
sys.getrefcount(1)
