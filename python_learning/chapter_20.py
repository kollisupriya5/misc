"""Python learning textbook chapter20."""

import math
# Comprehensions and Generations
# %%
# List comprehensions versus map
ord('s')

res = []
for i in 'spam':
    res.append(ord(i))
res

res = list(map(ord, 'spam'))
res

res = [ord(x) for x in 'spam']
res

[x ** 2 for x in range(10)]

list(map((lambda x: x ** 2), range(10)))

# %%
# Adding tests and nested loops: filter
[x for x in range(5) if x % 2 == 0]

list(filter((lambda x: x % 2 == 0), range(10)))

res = []
for x in range(15):
    if x % 2 == 0:
        res.append(x)
res

[x ** 2 for x in range(5) if x % 2 == 0]

list(map((lambda x: x ** 2), filter((lambda x: x % 2 == 0), range(10))))

# %%
# Formal comprehension syntax
res = [x + y for x in [1, 2, 3] for y in [100, 200, 300]]
res

res = []
for x in [1, 2, 3]:
    for y in [100, 200, 300]:
        res.append(x + y)
res

# %%
[x + y for x in 'abcd' for y in 'ABCD']
[x + y for x in 'spam' if x in 'sm' for y in 'SPAM' if y in 'PA']

[x + y + z for x in 'spam' if x in 'sm'
           for y in 'SPAM' if y in 'PA'
           for z in '123' if z > '1']

# %%
[(x, y) for x in range(5) if x % 2 == 0 for y in range(5) if y % 2 == 1]

res = []
for x in range(5):
    if x % 2 == 0:
        for y in range(5):
            if y % 2 == 1:
                res.append((x, y))
res

# %%
# List COmprehensions and matrices
M = [[1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]]

N = [[1, 1, 1],
     [2, 2, 2],
     [3, 3, 3]]
M[1]
M[2][2]

[row[1] for row in M]
[M[row][1] for row in (0, 1, 2)]

[M[i][i] for i in range(len(M))]  # Diagonals
[M[i][len(M)-1-i] for i in range(len(M))]  # Diagonals

# %%
L = [[1, 2, 3], [4, 5, 6]]
for i in range(len(L)):
    for j in range(len(L[i])):
        L[i][j] += 10

L
# %%
[col + 10 for row in M for col in row]
[[col + 10 for col in row] for row in M]

res = []
for row in M:
    for col in row:
        res.append(col + 10)
res

res = []
for row in M:
    temp = []
    for col in row:
        temp.append(col + 10)
    res.append(temp)

res

# %%
[M[row][col] * N[row][col] for row in range(3) for col in range(3)]
[[M[row][col] * N[row][col] for row in range(3)] for col in range(3)]

res = []
for row in range(3):
    tmp = []
    for col in range(3):
        tmp.append(M[row][col] * N[row][col])
    res.append(tmp)
res

# %%
[[col1 * col2 for (col1, col2) in zip(row1, row2)] for (row1, row2) in zip(M, N)]

res = []
for (row1, row2) in zip(M, N):
    tmp = []
    for (col1, col2) in zip(row1, row2):
        tmp.append(col1 * col2)
    res.append(tmp)

res


# %%
# Generator functions in action
def gensquares(N):
    """Generator function."""
    for i in range(N):
        yield i ** 2


gensquares(5)

for i in gensquares(5):
    print(i, end=':')

x = gensquares(5)
x
next(x)

y = gensquares(5)
iter(y) is y
next(y)


def buildsquares(n):
    res = []
    for i in range(n):
        res.append(i ** 2)
    return res


for x in buildsquares(5):
    print(x, end=" :")

# %%
for x in [n ** 2 for n in range(5)]:
    print(x, end=":")

for x in map((lambda n: n ** 2), range(5)):
    print(x, end=':')


# %%
def ups(line):
    """Substring Generator."""
    for sub in line.split(','):
        yield sub.upper()


tuple(ups('aaa,bbb,ccc'))
{i: s for (i, s) in enumerate(ups('aaa,bbb,ccc'))}


# %%
def gen():
    for i in range(10):
        x = yield i
        print(x)


g = gen()
next(g)
next(g)
g.send(88)
g.send(77)
g.send(99)

# %%
# Generator expressions : Iterables Meet COmprehensions
[x ** 2 for x in range(5)]
(x ** 2 for x in range(5))
list(x ** 2 for x in range(5))
g = (x ** 2 for x in range(5))
iter(g) is g
next(g)
next(g)
g

# %%
for num in (x ** 2 for x in range(5)):
    print('%s, %s' % (num, num / 2.0))

# %%
''.join(x.upper() for x in 'aaa,bbb,ccc'.split(','))
a, b, c = (x + '\n' for x in 'aaa,bbb,ccc'.split(','))
a, c
sum(x ** 2 for x in range(4))
sorted(x ** 2 for x in range(4))
sorted((x ** 2 for x in range(4)), reverse=True)

# %%
# Generator expession versus map
list(map(abs, (-1, -3, -2, 4)))
list(abs(x) for x in (-1, -2, -3, 4))
list(map(lambda x: x * 2, (1, 2, 3, 4)))
list(x * 2 for x in (1, 2, 3, 4))

# %%
line = 'aaa,bbb,ccc'
''.join([x.upper() for x in line.split(',')])
''.join(x.upper() for x in line.split(','))
''.join(map(str.upper, line.split(',')))
''.join(x * 2 for x in line.split(','))
''.join(map(lambda x: x * 2, line.split(',')))

# %%
[x * 2 for x in [abs(x) for x in (-1, -2, 3, -4)]]  # Nested comprehensions
list(map(lambda x: x * 2, map(abs, (-1, -2, 3, -4))))  # Nested maps
list(x * 2 for x in (abs(x) for x in (-1, 2, 3, -4)))

list(map(math.sqrt, (x ** 2 for x in range(4))))

list(map(abs, map(abs, map(abs, (-2, 0, 1)))))
list(abs(x) for x in (abs(x) for x in (abs(x) for x in (-1, 0, 1))))

# %%
list(math.sqrt(x ** 2) for x in range(4))
list(abs(x) for x in (-1, 1, 0))

# %%
# Generator expressions versus filter
line = 'aa bbb cc'
''.join(x for x in line.split() if len(x) > 1)
''.join(filter(lambda x: len(x) > 1, line.split()))
''.join(x.upper() for x in line.split() if len(x) > 1)
''.join(map(str.upper, filter(lambda x: len(x) > 1, line.split(','))))

res = ''
for x in line.split():
    if len(x) > 1:
        res += x.upper()

res

# %%
# Generator function versus generator expressions
G = (c * 4 for c in 'spam')  # Generator expression
list(G)


def timefour(s):
    for c in s:
        yield c * 4


list(timefour('spam'))

i = iter(G)
next(i)
next(i)

H = timefour('spam')
h = iter(H)
next(h)
next(h)

# %%
line = 'aa bbb c'
''.join(x.upper() for x in line.split() if len(x) > 1)


def gensub(line):
    for x in line.split():
        if len(x) > 1:
            yield x.upper()


''.join(gensub(line))

# %%
# Generators are single iteration objects
G = (c * 4 for c in 'spam')
iter(G) is G

i1 = iter(G)
next(i1)
next(i1)
i2 = iter(G)
next(i2)
next(i1)
i3 = iter(G)
next(i3)

i3 = (c * 4 for c in 'spam')
next(i3)

# %%
J = timefour('spam')
iter(J) is J
k1, k2 = iter(J), iter(J)
next(k1)
next(k1)
next(k2)
next(k1)
next(k2)

# %%
L = [1, 2, 3, 4]
i1, i2 = iter(L), iter(L)
next(i1)
next(i2)
del L[2:]
next(i1)

# %%
# Generation in built-in Types, Tools and Classes
D = {'a': 1, 'b': 2, 'c': 3}
x = iter(D)
next(x)
next(x)

for key in D:
    print(key, D[key])

for line in open('script2.py'):
    print(line, end='')


# %%
# Generators and function application
def f(a, b, c):
    print('%s, %s, and %s' % (a, b, c))


f(0, 1, 2)
f(* range(3))
f(*(i for i in range(3)))

D = dict(a='Bob', b='dev', c=40.5)
f(a='Bob', b='dev', c=40.5)
f(**D)
f(*D.values())

for x in 'spam': print(x.upper(), end=' ')
list(print(x.upper(), end=' ') for x in 'spam')
print(*(x.upper() for x in 'spam'))

# %%
# # Scrambled Sequences
L, S = [1, 2, 3], 'spam'
for i in range(len(S)):
    S = S[1:] + S[:1]
    print(S, end=' ')

for i in range(len(L)):
    L = L[1:] + L[:1]
    print(L, end=' ')

for i in range(len(S)):
    x = S[i:] + S[:i]
    print(x, end=' ')


# %%
# Simple functions
def scramble(seq):
    res = []
    for i in range(len(seq)):
        res.append(seq[i:] + seq[:i])
    return res


scramble('spam')


def scrambl1(seq):
    return [seq[i:] + seq[:i] for i in range(len(seq))]


scrambl1('spam')

for x in scrambl1((1, 2, 3)):
    print(x, end=' ')

for x in scramble((1, 2, 3)):
    print(x, end=' ')


# %%
# Generator functions
def scramble(seq):
    for i in range(len(seq)):
        seq = seq[i:] + seq[:i]
        yield seq


list(scramble('spam'))


def scramble1(seq):
    for i in range(len(seq)):
        yield seq[i:] + seq[:i]


list(scramble1('spam'))
list(scramble((1, 2, 3)))

# Generator expressions
S = 'spam'
G = (S[i:] + S[:i] for i in range(len(S)))
list(G)


F = lambda seq: (seq[i:] + seq[:i] for i in range(len(seq)))
list(F(S))
list(F([1, 2, 3]))

# %%
# Emulating zip and map with iteration tools
s1 = 'abc'
s2 = 'xyz123'
list(zip(s1, s2))
list(zip([-2, -1, 0, 1, 2]))
list(map(abs, [-2, -1, 0, 1, 2]))
list(map(pow, [1, 2, 3], [1, 2, 3, 4]))
map(lambda x, y: x + y, open('script2.py'), open('script2.py'))
[x + y for (x, y) in zip(open('script2.py'), open('script2.py'))]


# %%
def mymap(func, *seqs):
    """Map function."""
    res = []
    for args in zip(*seqs):
        res.append(func(*args))
    return res


print(mymap(abs, [-1, -2, 0, 1]))
print(mymap(pow, [1, 2, 3], [4, 5, 6]))


def mymap(func, *seqs):
    """List comprehension."""
    return [func(*args) for args in zip(*seqs)]


print(mymap(abs, [-1, -2, 0, 1]))
print(mymap(pow, [1, 2, 3], [4, 5, 6]))


# %%
# Using generators
def mymap(func, *seqs):
    """Map function using generators."""
    for args in zip(*seqs):
        yield func(*args)


def mymap(func, *args):
    """Map function."""
    return(func(*args) for args in zip(*args))


print(mymap(abs, [-1, -2, 0, 1]))


# %%
def myzip(*seqs):
    """Zip function."""
    seqs = [list(S) for S in seqs]
    res = []
    while all(seqs):
        res.append(tuple(S.pop(0) for S in seqs))
    return res


print(myzip('abc', 'xyz123'))


def mymappad(*seqs, pad=None):
    """Map function."""
    seqs = [list(S) for S in seqs]
    res = []
    while all(seqs):
        res.append(tuple((S.pop(0) if S else pad) for S in seqs))
    return res


print(mymappad('abc', 'xyz123'))
print(mymappad('abc', 'xyz123', pad=1))


# Using generators
def myzip(*seqs):
    """Zip function."""
    seqs = [list(S) for S in seqs]
    while all(seqs):
        yield tuple(tuple(S.pop(0) for S in seqs))


print(list(myzip('abc', 'xyz123')))


def mymappad(*seqs, pad=None):
    """Map function."""
    seqs = [list(S) for S in seqs]
    while all(seqs):
        yield tuple((S.pop(0) if S else pad) for S in seqs)


print(mymappad('abc', 'xyz123', pad=1))


# %%
def myzip(*seqs):
    """Zip function with lengths."""
    minlen = min(len(S) for S in seqs)
    return [tuple(S[i] for S in seqs) for i in range(minlen)]


print(myzip('abc', 'xyz123'))


def mymappad(*seqs, pad=None):
    """Zip function with lengths."""
    maxlen = max(len(S) for S in seqs)
    index = range(maxlen)
    return [tuple((S[i] if len(S) > i else pad) for S in seqs) for i in index]


print(mymappad('abc', 'xyz123', pad=None))
print(mymappad('abc', 'xyz123', pad=99))


# %%
# Using generators
def zip(*seqs):
    """Zip function usng generator."""
    minlen = min(len(S) for S in seqs)
    return (tuple(S[i] for S in seqs) for i in range(minlen))


list(zip('abc', 'xyz123'))

# %%
# Comprehension syntax summary
[x * x for x in range(10)]  # List comprehension

(x * x for x in range(10))  # Generator expression

{x * x for x in range(10)}  # Set comprehensions

{x: x * x for x in range(10)}  # Dictionary comprehensions

# %%
(x for x in range(5))
x

x = 99
[x for x in range(5)]
x

y = 99
for y in range(5):
    pass
y

# %%
x = 'aaa'


def fun():
    """Comprewhension."""
    y = 'bbb'
    print(''.join(z for z in x + y))


fun()

# %%
# Comprehending set and dictionary comprehensions
{x * x for x in range(5)}

set(x * x for x in range(5))

{x: x * x for x in range(5)}

dict((x, x * x) for x in range(10))

# %%
res = set()
for x in range(5):
    res.add(x * x)

res

res = {}
for x in range(5):
    res[x] = x * x

res

x

# %%
G = ((x, x * x) for x in range(5))
next(G)
next(G)

# %5
# Extended comprehension syntax for sets and dictionaries
[x * x for x in range(10) if x % 2 == 0]  # Lists are ordered

{x * x for x in range(10) if x % 2 == 0}  # But sets are not

{x: x * x for x in range(10) if x % 2 == 0}  # neither are dict keys

{x + y for x in 'ab' for y in 'cd'}

{x + y: (ord(x), ord(y)) for x in 'ab' for y in 'cd'}

{k * 2 for k in ['spam', 'ham', 'sausage'] if k[0] == 's'}

{k.upper(): k * 2 for k in ['spam', 'ham', 'sausage'] if k[0] == 's'}
