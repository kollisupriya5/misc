"""Function to find the sum of all the element in the list using recursion."""


def sum_elements(list):
    """Sum of elements in a list."""
    if len(list) == 0:
        return 0
    return list[0]+sum_elements(list[1:])


def test_sum_elements1():
    """Test for sum of elements in a list."""
    assert sum_elements([1, 2, 3, 4, 5]) == 15


def test_sum_elements2():
    """Test with empty list."""
    assert sum_elements([]) == 0


def test_sum_elements3():
    """Test with empty list."""
    assert sum_elements([15]) == 15


def test_sum_elements4():
    """Test with empty list."""
    assert sum_elements([5, 10, 15, 20, 25]) == 75
