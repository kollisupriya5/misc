"""Function to eliminate consecutive duplicates of list elements."""


def duplicates(list):
    """Eliminating duplicates in the list."""
    res = []
    if len(list) != 0:
        for num in list:
            if num not in res:
                res.append(num)
        return res
    return None


duplicates([1,2,3,4,5,5,3,6,1,7,2,3])


def test_duplicates1():
    """Test1 eleminating duplicates in the list."""
    assert duplicates([1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_duplicates2():
    """Test2 eleminating duplicates in the list."""
    assert duplicates([1, 1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_duplicates3():
    """Test3 eleminating duplicates in the list."""
    assert duplicates(['a', 'b', 'c', 'c', 'd']) == ['a', 'b', 'c', 'd']


def test_duplicates4():
    """Test4 with empty list."""
    assert duplicates([]) is None


def test_duplicates5():
    """Test4 with single element."""
    assert duplicates([1]) == [1]


def test_duplicates6():
    """Test4 with single element."""
    assert duplicates(['q']) == ['q']


def test_duplicates7():
    """Test4 with similar elements."""
    assert duplicates(['a', 'a', 'a', 'a']) == ['a']


def test_duplicates8():
    """Test4 with similar elements."""
    assert duplicates(['a', 1, 3, 3, 'a', 1]) == ['a', 1, 3]
