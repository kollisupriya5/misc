"""Implementing Quick sort."""


# %%
def partition(lst, l, r):
    """Prtitioning the list."""
    i = l
    j = r-1
    pivot = lst[r]
    while i <= j:
        while lst[i] < pivot:
            i = i+1
        while lst[j] > pivot:
            j = j-1
        if i <= j:
            lst[j], lst[i] = lst[i], lst[j]
            i = i+1
            j = j-1
    lst[i], lst[r] = lst[r], lst[i]
    return i


def quicksort_recursion(lst, l, r):
    """Quick sort using recursion."""
    if l < r:
        i = partition(lst, l, r)
        quicksort_recursion(lst, l, i-1)
        quicksort_recursion(lst, i+1, r)
    return lst


def quicksort(lst):
    """Implemneting Quick sort."""
    return quicksort_recursion(lst, 0, len(lst)-1)


def test_quicksort1():
    """Test for sorting elements in the list."""
    assert quicksort([1, 2, 3, 4, 5]) == [1, 2, 3, 4, 5]


def test_quicksort2():
    """Test for sorting elements in the list."""
    assert quicksort([10, 30, 40, 50, 70, 80]) == [10, 30, 40, 50, 70, 80]


def test_quicksort3():
    """Test for sorting elements in the list."""
    assert quicksort([1, 5, 2, 4, 3, 7, 6]) == [1, 2, 3, 4, 5, 6, 7]


def test_quicksort4():
    """Test with negative values."""
    assert quicksort([1, 2, -4, 5, 0, -6]) == [-6, -4, 0, 1, 2, 5]


def test_quicksort5():
    """Test for sorting elements in the list."""
    assert quicksort([1, 1]) == [1, 1]


def test_quicksort6():
    """Test for sorting elements in the list."""
    assert quicksort([1, 5, 2, 4, 3, 7, 6, 2]) == [1, 2, 2, 3, 4, 5, 6, 7]
