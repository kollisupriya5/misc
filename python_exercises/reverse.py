"""Function to find the reverse of a list."""


def reverse(list):
    """Reverse of a list."""
    res = []
    for i in range(1, len(list)+1):
        res.append(list[-i])
    return res


def test_reverse1():
    """Test for reverse of a list."""
    assert reverse([1, 2, 3, 4, 5]) == [5, 4, 3, 2, 1]


def test_reverse2():
    """Test for reverse of a list."""
    assert reverse(['a', 'b', 'c', 'd']) == ['d', 'c', 'b', 'a']


def test_reverse3():
    """Test with empty list."""
    assert reverse([]) == []


def test_reverse4():
    """Test with single element."""
    assert reverse([1]) == [1]


def test_reverse5():
    """Test with single element."""
    assert reverse(['a']) == ['a']


def test_reverse6():
    """Test for reverse of a list."""
    assert reverse(['a', 1, 'c', 3]) == [3, 'c', 1, 'a']


def test_reverse7():
    """Test for reverse of a list."""
    assert reverse(['abc', 'a', 123, 1, 'hi1']) == ['hi1', 1, 123, 'a', 'abc']


def test_reverse8():
    """Test for reverse of a list."""
    assert reverse(['1bc', 'abc', 'cd6']) == ['cd6', 'abc', '1bc']
