"""Chess board."""


# %%
class Board:
    """Class for board."""

    def __init__(self):
        """Create board."""
        self.board = {}

    def values_board(self):
        """Board creation."""
        self.board[(1, 'a')] = Rook('white', 'R')
        self.board[(1, 'h')] = Rook('white', 'R')
        self.board[(1, 'b')] = Knight('white', 'N')
        self.board[(1, 'g')] = Knight('white', 'N')
        self.board[(1, 'c')] = Bishop('white', 'B')
        self.board[(1, 'f')] = Bishop('white', 'B')
        self.board[(1, 'd')] = Queen('white', 'Q')
        self.board[(1, 'e')] = King('white', 'K')
        self.board[(8, 'a')] = Rook('black', 'r')
        self.board[(8, 'h')] = Rook('black', 'r')
        self.board[(8, 'b')] = Knight('black', 'n')
        self.board[(8, 'g')] = Knight('black', 'n')
        self.board[(8, 'c')] = Bishop('black', 'b')
        self.board[(8, 'f')] = Bishop('black', 'b')
        self.board[(8, 'd')] = Queen('black', 'q')
        self.board[(8, 'e')] = King('black', 'k')
        for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
            self.board[(2, i)] = Pawn('white', 'P')
            self.board[(7, i)] = Pawn('black', 'p')
        return self.board

    def print_board(self):
            """Print chess Board."""
            print('{:^30}'.format('## WHITE ##'))
            print('  ', end='')
            for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                print('{:^3}'.format(i), end=' ')
            print('\n '+'-'*33)
            for i in range(1, 9):
                print(i, end=' |')
                for j in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                    x = self.board.get((i, j), ' ')
                    if x is not " ":
                        x = x.name
                    d = "{:^3}".format(x)
                    print(d, end='|')
                print()
                print(('  |'), end='')
                for j in range(1, 9):
                    d = "{:^3}".format('---')
                    print(d, end='|')
                print()
            print('  ', end=' ')
            for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                print('{:^3}'.format(i), end=' ')
            print('\n {:^30}'.format('## BLACK ##'))

    def move(self):
        """Move key."""
        key = input('Enter your moves: ')
        start = self.board.get((int(key[1]), key[0]))
        end = self.board.get((int(key[3]), key[2]))
        if start == end:
            print('Invalid move')
            self.move()
            if start.color != end.color:
                self.board[(int(key[3]), key[2])] = start
                self.board[(int(key[1]), key[0])] = ' '
            self.move()


class Piece:
    """Piece class."""

    def __init__(self, color, name):
        """Piece Constructor."""
        self.name = name
        self.color = color


class Rook(Piece):
    """rook class."""

    def __init__(self, color, name):
        """Construct for Rook."""
        Piece.__init__(self, color, name)


class Knight(Piece):
    """rook class."""

    def __init__(self, color, name):
        """Construct for knight."""
        Piece.__init__(self, color, name)


class Bishop(Piece):
    """rook class."""

    def __init__(self, color, name):
        """Construct for Bishop."""
        Piece.__init__(self, color, name)


class Queen(Piece):
    """rook class."""

    def __init__(self, color, name):
        """Construct for queen."""
        Piece.__init__(self, color, name)


class King(Piece):
    """rook class."""

    def __init__(self, color, name):
        """Construct for king."""
        Piece.__init__(self, color, name)


class Pawn(Piece):
    """Pawn class."""

    def __init__(self, color, name):
        """Construct for Pawn."""
        Piece.__init__(self, color, name)


b = Board()
b.values_board()
b.print_board()
b.move()
b.print_board()
