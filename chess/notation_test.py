"""Edward notation."""

from itertools import groupby
from collections import Counter
import unittest

# %%
position = {('a', 0): 'R', ('b', 0): 'N', ('c', 0): 'B', ('d', 0): 'Q',
            ('e', 0): 'K', ('f', 0): 'B', ('g', 0): 'N', ('h', 0): 'R',
            ('a', 1): 'P', ('b', 1): 'P', ('c', 1): 'P', ('d', 1): 'P',
            ('e', 1): 'P', ('f', 1): 'P', ('g', 1): 'P', ('h', 1): 'P',
            ('a', 7): 'r', ('b', 7): 'n', ('c', 7): 'b', ('d', 7): 'q',
            ('e', 7): 'k', ('f', 7): 'b', ('g', 7): 'n', ('h', 7): 'r',
            ('a', 6): 'p', ('b', 6): 'p', ('c', 6): 'p', ('d', 6): 'p',
            ('e', 6): 'p', ('f', 6): 'p', ('g', 6): 'p', ('h', 6): 'p'}

lst = ''
for i in range(7, -1, -1):
    for j in 'abcdefgh':
        lst = lst + position.get((j, i), ' ')
    lst = lst + '/'
lst


def notation():
    """Notation."""
    initial = lst
    new_lst = []
    for i, j in groupby(initial, lambda x: x == ' '):
        new = list(j)
        if i:
            new = [str(len(new))]
        new_lst.extend(new)
    s = ''.join(new_lst)
    print(s)


notation()


# %%
def digit_space(x):
    """Convert digit to space."""
    lst = ''
    for i in x:
        if i.isdigit():
            lst = lst + ' '*int(i)
        else:
            lst = lst + i
    return lst


# %%
def reverse_notation(x):
    """Reversing notation."""
    lst = x.split('/')
    board = list(map(digit_space, lst))
    board = ''.join(board)
    print(len(board) == 64)
    x1 = Counter(board)
    conditions = all([len(board) == 64, x1["p"] <= 8, x1["P"] <= 8,
                      x1["Q"] <= 9, x1["q"] <= 9, x1["N"] <= 9,
                      x1["n"] <= 9, x1["B"] <= 9, x1["b"] <= 9,
                      x1["R"] <= 9, x1["r"] <= 9, x1["K"] == 1,
                      x1["k"] == 1])
    pos = {}
    N = 0
    if conditions:
        for i in range(7, -1, -1):
            for j in 'abcdefgh':
                if board[N] != ' ':
                    pos[(j, i)] = board[N]
                N += 1
        return True
    return False


reverse_notation('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR/')


# %%
class testmethod(unittest.TestCase):
    """Test cases."""

    def test1(self):
        """Test for initial board state."""
        x = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is True

    def test2(self):
        """Test for pawns."""
        x = 'rnbqkbnr/ppppppppp/8/8/8/8/PPPPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is False

    def test3(self):
        """Test for checking Queens."""
        x = 'rnbqkbnr/qqqqqqqq/8/8/8/8/PPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is True

    def test4(self):
        """Test for queens."""
        x = 'rnbqkbnr/qqqqqqqq/3q4/8/8/8/PPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is False

    def test5(self):
        """Test for kings."""
        x = 'rnbqkbnr/pppppppp/3k4/8/8/8/PPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is False

    def test6(self):
        """Test for Rooks."""
        x = 'rnbqkbnr/pppppppR/2R5/RR2RR2/8/2RR4/PPPPPPPP/RNBQKBNR/'
        assert reverse_notation(x) is False

    def test7(self):
        """Test for bishops."""
        x = 'rnBBkbnr/BBpppppp/7B/8/8/8/BPPBBPPP/RNBQKBNR/'
        assert reverse_notation(x) is False

    def test8(self):
        """Test for Knights."""
        x = 'rnbqkbnr/pppppppn/8/n1n1n1n1/8/8/PnPPPPPP/RnBQKBnR/'
        assert reverse_notation(x) is False
