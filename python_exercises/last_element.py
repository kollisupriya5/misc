"""Python function to find the last element in the list using recursion."""


def lastelement(list):
    """Find the last element of a list."""
    if len(list) != 0:
        if len(list) == 1:
            return list[0]
        return lastelement(list[1:])
    return None


def test_lastelement1():
    """Test1 for last element in a list."""
    assert lastelement([1, 2, 3, 4, 5]) == 5


def test_lastelement2():
    """Test2 for last element in a list."""
    assert lastelement(['a', 'b', 'l', 'g', 'h']) == 'h'


def test_lastelement3():
    """Test3 with no element."""
    assert lastelement([]) is None


def test_lastelement4():
    """Test with single element."""
    assert lastelement([1]) == 1


def test_lastelement5():
    """Test5 for last element in a list."""
    assert lastelement(['a', 'b', 'c', 'd', 'e']) == 'e'


def test_lastelement6():
    """Test6 for last element in a list."""
    assert lastelement(['a']) == 'a'


def test_lastelement7():
    """Test7 for last element in a list."""
    assert lastelement(['a', 1, 'b', 'c', 2, 3, 'd']) == 'd'


def test_lastelement8():
    """Test8 for last element in a list."""
    assert lastelement([123, 456, '1bc', 'abc', 'cd6']) == 'cd6'
