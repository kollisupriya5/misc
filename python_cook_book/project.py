"""Generating data."""

import numpy as np
import pandas as pd


df = pd.DataFrame(pd.date_range('20170101', periods=8760, freq='1H'),
                  columns=name)
name = ['Date and time']
time = ('0-1', '1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9', '9-10',
        '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18',
        '18-19', '19-20', '20-21', '21-22', '22-23', '23-0')
df['Time'] = time * 365
df['CG Maida 50Kg'] = 0
df['Bread Maida 50Kg'] = 0
df['G Maida 50Kg'] = np.random.randint(50, 72, 8760)
df['CG Rawa 50Kg'] = np.random.randint(1, 14, 8760)
df['Bell Rawa 50kg'] = 0
df['CG Atta 50kg'] = np.random.randint(1, 3, 8760)
df['Bell Atta 50Kg'] = np.random.randint(3, 13, 8760)
df['G Atta 50Kg'] = np.random.randint(0, 5, 8760)
df['Bell Chiroty 50Kg'] = np.random.randint(0, 11, 8760)
df['CG-C-Atta 25Kg'] = 0
df['CG-C-Atta 50Kg'] = 0
df['Bell Bran 49Kg'] = np.random.randint(6, 19, 8760)
df['CG S/D 45Kg'] = np.random.randint(0, 5, 8760)
df['Bell SF 49Kg'] = np.random.randint(2, 17, 8760)
df['CG Flakes 34Kg'] = np.random.randint(2, 17, 8760)
df['CG Flakes 30Kg'] = np.random.randint(1, 19, 8760)
df
