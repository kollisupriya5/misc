"""Practicing pandas melt function."""

import pandas as pd
df = (pd.DataFrame({'A': {0: 'a', 1: 'b', 2: 'c'},
                    'B': {0: 1, 1: 3, 2: 5},
                    'C': {0: 2, 1: 4, 2: 6}}))
df
pd.melt(df, id_vars=['A'], value_vars=['B'])
pd.melt(df, id_vars=['A'], value_vars=['C'])
pd.melt(df, id_vars=['B'], value_vars=['C'])
pd.melt(df, id_vars=['A'], value_vars=['B', 'C'])
(pd.melt(df, id_vars=['A'], value_vars=['B'],
         var_name='myVarname', value_name='myValname'))

# %%
df.columns = [list('ABC'), list('DEF')]
pd.melt(df, col_level=0, id_vars=['A'], value_vars=['B'])
pd.melt(df, id_vars=[('A', 'D')], value_vars=[('B', 'E')])

# %%
df1 = (pd.DataFrame({'A': {0: 1, 1: 2, 2: 3}, 'B': {0: 4, 1: 5, 2: 6},
                     'C': {0: 7, 1: 8, 2: 9}, 'D': {0: 10, 1: 11, 2: 12}}))
df1
pd.melt(df1, id_vars=['A'])
pd.melt(df1, id_vars=['A'], value_vars=['B'])
pd.melt(df1, id_vars=['A'], value_vars=['B', 'C'])
pd.melt(df1, id_vars=['A'], value_vars=['B', 'C', 'D'])
pd.melt(df1, id_vars=['B'], value_vars=['A'])
(pd.melt(df1, id_vars=['A'], value_vars=['B'],
         var_name='varname', value_name='valuename'))
