import unittest
import regular_functions as regfun

class testmethod(unittest.TestCase):
#test for isphonenumber
    def test1(self):
        self.assertEqual(regfun.isphonenumber('415-555-1234'),True)
    def test2(self):
        self.assertEqual(regfun.isphonenumber('650-555-2345'),True)
    def test3(self):
        self.assertEqual(regfun.isphonenumber('(416)555-3456'),True)
    def test4(self):
        self.assertEqual(regfun.isphonenumber('202 555 4567'),True)
    def test5(self):
        self.assertEqual(regfun.isphonenumber('1 416 555 9292'),True)
    def test6(self):
        self.assertEqual(regfun.isphonenumber('4035555678'),True)
    def test7(self):
        self.assertEqual(regfun.isphonenumber('4567891012'),True)
    def test8(self):
        self.assertEqual(regfun.isphonenumber('123456778bc'),False)
    def test9(self):
        self.assertEqual(regfun.isphonenumber('123abc456'),False)
    def test10(self):
        self.assertEqual(regfun.isphonenumber('456abc789'),False)
    def test11(self):
        self.assertEqual(regfun.isphonenumber('abc1234'),False)
    def test12(self):
        self.assertEqual(regfun.isphonenumber('/adxgfhgf1235'),False)
    def test13(self):
        self.assertEqual(regfun.isphonenumber('123 456 7890'),True)
    def test14(self):
        self.assertEqual(regfun.isphonenumber('145 678-8989'),True)
    def test15(self):
        self.assertEqual(regfun.isphonenumber('(456)555 4560'),True)
#test for isnumber
    def test16(self):
        self.assertEqual(regfun.isnumber('19,69,456'), True)
    def test17(self):
        self.assertEqual(regfun.isnumber('+456412'), True)
    def test18(self):
        self.assertEqual(regfun.isnumber('e123'),True)
    def test19(self):
        self.assertEqual(regfun.isnumber('1e-100'),True)
    def test20(self):
        self.assertEqual(regfun.isnumber('1'),True)
    def test21(self):
        self.assertEqual(regfun.isnumber('e123'),True)
    def test22(self):
        self.assertEqual(regfun.isnumber('3.1425'),True)
    def test23(self):
        self.assertEqual(regfun.isnumber('123,340.00'),True)
    def test24(self):
        self.assertEqual(regfun.isnumber('1e12'),True)
    def test25(self):
        self.assertEqual(regfun.isnumber('-1e12'),True)
    def test26(self):
        self.assertEqual(regfun.isnumber('1.45e'),False)
    def test27(self):
        self.assertEqual(regfun.isnumber('d123'),False)
    def test28(self):
        self.assertEqual(regfun.isnumber('abc123'),False)
    def test29(self):
        self.assertEqual(regfun.isnumber('412bc4'),False)
    def test30(self):
        self.assertEqual(regfun.isnumber('130e+15e+123'),False)
# test case for isemail
    def test31(self):
        self.assertEqual(regfun.isemail('kollisupriya@technoidentity.com'),True)
    def test32(self):
        self.assertEqual(regfun.isemail('kollisupriya5@gmail.com'),True)
    def test33(self):
        self.assertEqual(regfun.isemail('saidurga.chirumamilla@gmail.com'),True)
    def test34(self):
        self.assertEqual(regfun.isemail('sahithiajay@gmail.com'),True)
    def test35(self):
        self.assertEqual(regfun.isemail('vamsi970337@gmail.com'),True)
    def test36(self):
        self.assertEqual(regfun.isemail('kvarshith23@gmail.com'),True)
    def test37(self):
        self.assertEqual(regfun.isemail('sai.chirumamilla@technoidentity.com'),True)
    def test38(self):
        self.assertEqual(regfun.isemail('supriya.kolli@hotmail.com'),True)
    def test39(self):
        self.assertEqual(regfun.isemail('supriya.....@gmail.com'),True)
    def test40(self):
        self.assertEqual(regfun.isemail('sai.durga.chirumamilla@technoidentity.com'),True)
    def test41(self):
        self.assertEqual(regfun.isemail('1234567'),False)
    def test42(self):
        self.assertEqual(regfun.isemail('supriya.kolli@gamil.com13'),False)
    def test43(self):
        self.assertEqual(regfun.isemail('priya+kolli.+supriya@gmail.com'),False)
    def test44(self):
        self.assertEqual(regfun.isemail('sai+supriya+techno@gmail.com'),False)
    def test45(self):
        self.assertEqual(regfun.isemail('supriya  kolli.technoidentity@gmail.com'),False)
# test for isfilename
    def test46(self):
        self.assertEqual(regfun.isfilename('file.jpg'),True)
    def test47(self):
        self.assertEqual(regfun.isfilename('image.jpg'),True)
    def test48(self):
        self.assertEqual(regfun.isfilename('source.png'),True)
    def test49(self):
        self.assertEqual(regfun.isfilename('map.gif'),True)
    def test50(self):
        self.assertEqual(regfun.isfilename('123.jpg'),True)
    def test51(self):
        self.assertEqual(regfun.isfilename('abc.html'),False)
    def test52(self):
        self.assertEqual(regfun.isfilename('priya.lock'),False)
    def test53(self):
        self.assertEqual(regfun.isfilename('.bash_profile'),False)
    def test54(self):
        self.assertEqual(regfun.isfilename('workspace.doc'),False)
    def test55(self):
        self.assertEqual(regfun.isfilename('chandu#.doc'),False)
#iswhitespace
    def test56(self):
         self.assertEqual(regfun.iswhitespace('	   The quick brown fox...'),'The quick brown fox...')
    def test57(self):
         self.assertEqual(regfun.iswhitespace(' 	   jumps over the lazy dog.'),'jumps over the lazy dog.')
    def test58(self):
         self.assertEqual(regfun.iswhitespace('regular     expressions  functions'),'regular     expressions  functions')
    def test59(self):
         self.assertEqual(regfun.iswhitespace('     techno identity'),'techno identity')
    def test60(self):
        self.assertEqual(regfun.iswhitespace('     techno identity   Hyderabad   '),'techno identity   Hyderabad')

#isdata(extarcting data from log file)
    def test61(self):
        self.assertEqual(regfun.isdata('E/( 1553):  at widget.List.fillFrom(ListView.java:709)'),('fillFrom', 'ListView.java', '709'))
    def test62(self):
        self.assertEqual(regfun.isdata(' 	E/( 1553):   at widget.List.fillDown(ListView.java:652)'),('fillDown', 'ListView.java', '652'))
    def test63(self):
        self.assertEqual(regfun.isdata(' 	E/( 1553):   at widget.List.makeView(ListView.java:1727)'),('makeView', 'ListView.java', '1727'))
#url(extracting data from url)
    def test64(self):
        self.assertEqual(regfun.url(' 	ftp://file_server.com:21/top_secret/life_changing_plans.pdf'),('ftp', 'file_server.com', '21'))
    def test65(self):
        self.assertEqual(regfun.url('https://s3cur3-server.com:9999/'),('https', 's3cur3-server.com', '9999'))
    def test66(self):
        self.assertEqual(regfun.url(' 	file://localhost:4040/zip_file'),('file', 'localhost', '4040'))


if __name__ == '__main__':
    unittest.main()


# output
#--------------------------------------------------------------------
# Ran 66 tests in 0.004s

# OK
