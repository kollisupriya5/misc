"""Practicing python learning textbook chapter13."""

import numpy as np
import pandas as pd

# %%
# While loops
while True:
    print('Typre Ctrl-C to stop me')

x = 'spam'
while x:
    print(x, end=' ')
    x = x[1:]


a=0; b=10
while a < b:
    print(a, end=' ')
    a += 1


def func1():
    pass


# %%
# continue
x = 10
while x:
    x = x - 1
    if x % 2 != 0: continue
    print(x, end=' ')

# %%
x = 10
while x:
    x = x - 1
    if x % 2 == 0:
        print(x, end=' ')

# %%
# break
while True:
    name = input('Enter name:')
    if name == 'stop': break
    age = input('Enter age:')
    print('Hello', name, '=>', int(age)**2)

# %%
# Loop else
y = 10
x = y//2
x
while x > 1:
    if y % x == 0:
        print(y, 'has factor', x)
        break
    x = x-1
else:
    print(y, 'is prime')

# %%
# for loops
for x in (['spam', 'eggs', 'ham']):
    print(x, end=' ')
# %%
sum = 0
for x in ([1, 2, 3, 4, 5]):
    sum = sum + x
sum

# %%
prod = 1
for i in ([1, 2, 3, 4, 5]):prod *= i
prod

# %%
# other datatypes
s = 'limberjack'
for x in s:print(x, end=' ')
T = ('and', 'iam', 'lucky')
for i in T:print(i, end=' ')
U = [(1, 2), (3, 4), (5, 6)]
for (a, b) in U:
    print(a, b)
# %%
d = {'a': 1, 'b': 2, 'c': 3}
for keys in d:
    print(keys, '=>', d[keys])

# %%
list(d.items())

for (key, value) in d.items():
    print(key, '=>', value)

# %%
for both in U:
    a, b = both
    print(a, b)

# %%
((a, b), c) = ((1, 2), 3)
a, b, c
for ((a, b), c) in [((1, 2), 3), ((4, 5), 6)]: print(a, b, c)

for ((a, b), c) in [([1, 2], 3), ['XY', 6]]: print(a, b, c)

# %%
for (a, b, c) in [(1, 2, 3), (4, 5, 6)]:
    print(a, b, c)
a, *b, c = (1, 2, 3, 4)
a, b, c
for (a, *b, c) in [(1, 2, 3, 4), (5, 6, 7, 8)]:print(a, b, c)

for all in [(1, 2, 3, 4), (5, 6, 7, 8)]:
    a, b, c = all[0], all[1:3], all[3]
    print(a, b, c)

# %%
# Nested for loops
items = ["aaa", 111, (4, 5), 2.01]
tests = [(4, 5), 3.14]
for key in tests:
    for item in items:
        if item == 'key':
            print(key, 'was found')
            break
    else:
        print(key, 'not found')

# %%
for key in tests:
    if key in items:
        print(key, 'was found')
    else:
        print(key, "not found!")

# %%
seq1 = 'spam'
seq2 = 'scam'
res=[]
for x in seq1:
    if x in seq2:
        res.append(x)
res

[x for x in seq1 if x in seq2]

# %%
# counter range
list(range(5)), list(range(2, 5)), list(range(0, 10, 2))
list(range(-5, 5))
list(range(-5, 5, 1))
for i in range(3):
    print(i, 'python')

# %%
x = 'spam'
i = 0
while i < len(x):
    print(x[i], end=' ')
    i = i+1

# %%
x
len(x)
list(range(len(x)))
for i in range(len(x)):print(x[i], end=' ')

# %%
# Sequence shufflers: range and len
S = 'spam'
for i in range(len(S)):
    S = S[1:]+S[:1]
    print(S, end=' ')

for i in range(len(S)):
    S = S[i:]+S[:1]
    print(S, end='')

L = [1, 2, 3]
for i in range(len(L)):
    X = L[i:]+L[:i]
    print(X, end=' ')

# %%
# range vs slices
S = 'abcdefghijklmnopqrstuvwxyz'
list(range(0, len(S), 2))

for i in range(0, len(S), 2):
    print(S[i], end=' ')

for c in S[::2]:
    print(c, end=' ')

# %%
# Range vs comprehensions
L = [1, 2, 3, 4, 5]
for x in L:
    x = x+1
L
for i in range(len(L)):
    L[i] += 1
L
i = 0
while i < len(L):
    L[i] += 1
    i = i+1
L

# %%
# Zip
L1 = [1, 2, 3]
L2 = [4, 5, 6]
zip(L1, L2)
list(zip(L1, L2))

for(x, y) in zip(L1, L2):
    print(x, y, x+y)

T1, T2, T3 = (1,2,3), (4,5,6), (7,8,9)
list(zip(T1, T2, T3))

# %%
# Map
S1 = 'abc'
S2 = 'xyz123'
map(None, S1, S2)
list(map(ord, 'spam'))

# %%
res = []
for c in 'spam':res.append(ord(c))
res

# Dictionary construction with Zip
D1 = {'spam': 1, 'eggs': 3, 'toast': 5}
D1

keys = ['spam', 'eggs', 'toast']
vals = [1, 3, 5]
list(zip(keys, vals))

D = {}
for (k,v) in zip(keys, vals):D[k]=v
D

# %%
# offsets
S = 'spam'
offset = 0
for item in S:
    print(item, offset)
    offset += 1

for (offset, item) in enumerate(S):
    print(item, offset)

E = enumerate(S)
E
next(E)
[c * i for (i, c) in enumerate(S)]
