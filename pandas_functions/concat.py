"""Practicing merge pandas function."""

import pandas as pd
s1 = pd.Series(['a', 'b'])
s2 = pd.Series(['c', 'd'])
pd.concat([s1, s2])
pd.concat([s1, s2], ignore_index=True)
pd.concat([s1, s2], keys=['s1', 's2'])
pd.concat([s1, s2], keys=['s1', 's2'], names=['Series name', 'Row ID'])
df1 = pd.DataFrame([['a', 1], ['b', 2]], columns=['letter', 'number'])
df1
df2 = pd.DataFrame([['c', 3], ['d', 4]], columns=['letter', 'number'])
df2
pd.concat([df1, df2])
pd.concat([df1, df2], ignore_index=True)
df3 = (pd.DataFrame([['c', 3, 'cat'], ['d', 4, 'dog']],
                    columns=['letter', 'number', 'animal']))
df3
pd.concat([df1, df3])
pd.concat([df1, df3], join='inner')
df4 = (pd.DataFrame([['bird', 'polly'], ['monkey', 'george']],
                    columns=['animal', 'name']))
pd.concat([df1, df4], axis=1)
df5 = pd.DataFrame([1], index=['a'])
df5
df6 = pd.DataFrame([2], index=['a'])
df6
pd.concat([df5, df6])
