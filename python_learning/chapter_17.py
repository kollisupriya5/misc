"""Python learning textbook chapter17."""

import builtins

# %%
# Scopes
x = 99


def fun(y):
    z = x + y
    return z


fun(1)

zip

dir(builtins)
builtins.zip

zip is builtins.zip
len(dir(builtins))
len([x for x in dir(builtins) if not x.startswith(' ')])

x = 88


def func():
    x = 99
    print(x)


func()
print(x)

y, z = 1, 2


def all_global():
    global x
    x = y + z
    print(x)


all_global()
# %%
X = 99


def func1():
    global X
    X = 88
    print(X)
func1()


# %%
# Nested scope examples
X = 99


def f1():
    X = 88
    def f2():
        print(X)
    f2()


f1()


def maker(N):
    def action(X):
        return X ** N
    return action
f = maker(2)
f
f(3)
f(4)
g = maker(3)
g(2)


def maker(N):
    return lambda X: X ** N


h = maker(3)
h(4)


# Lambdas
# %%
def func():
    x = 4
    action = (lambda n: x ** n)
    return action

x = func()
print(x(2))


def makeActions():
    acts = []
    for i in range(5):
        acts.append(lambda x: i ** x)
    return acts


acts = makeActions()
acts[0]
acts[0](1)


def makeActions():
    acts = []
    for i in range(5):
        acts.append(lambda x, i=i: i ** x)
    return acts


acts = makeActions()
acts[0](2)
acts[1](2)


# %%
# Arbitary scope nesting
def f1():
    x = 99
    def f2():
        def f3():
            print(x)
        f3()
    f2()
f1()


# %%
# nonlocal in action
def tester(start):
    state = start
    def nested(label):
        print(label, state)
    return nested


F = tester(0)
F('spam')
F('ham')


def tester(start):
    state = start
    def nested(label):
        nonlocal state
        print(label, state)
        state += 1
    return nested


f = tester(0)
f('spam')
f('ham')
f(1)

def tester(start):
    def nested(label):
        global state
        state = 0
        print(label, state)
    return nested


f = tester(0)
f('abs')
state
