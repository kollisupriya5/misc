"""Practicing pandas dayaframe merge."""

import pandas as pd
# %%
df1 = pd.DataFrame({'key': ['b', 'b', 'a', 'a', 'c'], 'data1': range(5)})
df1

df2 = pd.DataFrame({'key': ['a', 'b', 'c'], 'data2': range(3)})
df2

pd.merge(df1, df2)

# %%
pd.merge(df1, df2, on='key')

# %%
df3 = (pd.DataFrame({'lkey': ['b', 'b', 'a', 'c', 'a', 'a', 'b'],
                    'data1': range(7)}))
df3

df4 = pd.DataFrame({'rkey': ['a', 'b', 'c'], 'data2': range(3)})
df4

pd.merge(df3, df4, left_on='lkey', right_on='rkey')  # if there are no common columns

# %%
pd.merge(df1, df2, how='outer')   # if there are common columns
pd.merge(df1, df2, how='right')

# %%
left = (pd.DataFrame({'key1': ['foo', 'foo', 'bar'],
                     'key2': ['one', 'two', 'one'], 'lval': [1, 2, 3]}))
right = (pd.DataFrame({'key1': ['foo', 'foo', 'bar', 'bar'],
                      'key2': ['one', 'one', 'one', 'two'],
                       'rval': [1, 2, 3, 4]}))
pd.merge(left, right)
pd.merge(left, right, on=['key1','key2'], how='outer')
right1 = pd.DataFrame({'group_val': [3.5, 7]}, index=['a', 'b'])
left1 = pd.DataFrame({'key': ['a', 'b', 'a', 'a', 'b', 'c'], 'value': range(6)})
left1
pd.merge(left1, right1, left_on='key',right_index=True)
