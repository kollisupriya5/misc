"""Practicing matplotlib."""

import numpy as np
# import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# %%
"""Simple plot."""
# Using defaults
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
plt.show()

# %%
# Instantiating defaults
plt.figure(figsize=(8, 6), dpi=80)
plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-")
plt.plot(X, S, color="green", linewidth=1.0, linestyle="--")
plt.xlim(-4.0, 4.0)
plt.ylim(-1, 1)
plt.xticks(np.linspace(-4, 4, 9, endpoint=True))
plt.yticks(np.linspace(-1, 1, 5, endpoint=True))
plt.show()

# %%
# setting limits
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
plt.xlim(X.min()*1.1, X.max()*1.1)
plt.ylim(C.min()*1.1, C.max()*1.1)
plt.show()

# %%
# setting ticks
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi])
plt.yticks([-1, 0, +1])
plt.show()

# %%
# setting tick labels
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
(plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
            [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$']))
plt.yticks([-1, 0, +1], [r'$-1$', r'$0$', r'$1$'])
plt.show()

# %%
# Moving spines
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
ax = plt.gca()
ax.spines['right'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.spines['top'].set_position(('data', 0))
ax.yaxis.set_ticks_position('left')
ax.spines['left'].set_position(('data', 0))

# %%
# Adding legends
X = np.linspace(-np.pi, np.pi, 256)
C, S = np.cos(X), np.sin(X)
plt.plot(X, S)
plt.plot(X, C)
plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-", label='cosine')
plt.plot(X, S, color="green", linewidth=1.0, linestyle="--", label='sine')
plt.legend()

# %%
# Drip drop
fig = plt.figure(figsize=(6, 6), facecolor='white')
ax = fig.add_axes([0, 0, 1, 1], frameon=False, aspect=1)
n = 50
size_min = 50
size_max = 50*50
P = np.random.uniform(0, 1, (n, 2))
C = np.ones((n, 4)) * (0, 0, 0, 1)
C[:, 3] = np.linspace(0, 1, n)
S = np.linspace(size_min, size_max, n)
scat = ax.scatter(P[:, 0], P[:, 1], s=S, lw=0.5,
                  edgecolors=C, facecolors='None')
ax.set_xlim(0, 1), ax.set_xticks([])
ax.set_ylim(0, 1), ax.set_yticks([])

# %%
# Scatter plots
n = 256
X = np.linspace(-np.pi, np.pi, n, endpoint=True)
Y = np.sin(2*X)

plt.plot(X, Y+1, color='blue', alpha=1.00)
plt.fill_between(X, 1, Y+1)
plt.plot(X, Y-1, color='blue', alpha=1.00)
plt.fill_between(X, -1, Y-1, (Y-1) > -1, color='blue', alpha=0.5)
plt.fill_between(X, -1, Y-1, (Y-1) < -1, color='red', alpha=0.5)
plt.show()

# %%
# Scatter plots
n = 1024
X = np.random.normal(0, 1, n)
Y = np.random.normal(0, 1, n)
T = np.arctan2(Y, X)
plt.axes([0.25, 0.25, 0.95, 0.95])
plt.scatter(X, Y, c=T, s=50, alpha=.5)
plt.xlim(-1.5, 1.5)
plt.ylim(-1.5, 1.5)
plt.show()

# %%
# Bar plots
n = 12
X = np.arange(n)
Y1 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
Y2 = (1-X/float(n)) * np.random.uniform(0.5, 1.0, n)
plt.axes([0.025, 0.025, 0.95, 0.95])
plt.bar(X, +Y1, facecolor='#9999ff', edgecolor='white')
plt.bar(X, -Y2, facecolor='#ff9999', edgecolor='white')

for x, y in zip(X, Y1):
    plt.text(x+0.4, y+0.05, '%.2f' % y, ha='center', va='bottom')
for x, y in zip(X, Y1):
    plt.text(x+0.4, -y-0.05, '%.2f' % y, ha='center', va='top')

plt.xlim(-.5, n), plt.xticks([])
plt.ylim(-1.25, +1.25), plt.yticks([])
plt.show()


# %%
# contour plots
def f(x, y):
    """Contour plots."""
    return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)


n = 256
x = np.linspace(-3, 3, n)
y = np.linspace(-3, 3, n)
X, Y = np.meshgrid(x, y)

plt.contourf(X, Y, f(X, Y), 8, alpha=.75, cmap='jet')
C = plt.contour(X, Y, f(X, Y), 8, colors='black', linewidth=.5)
plt.clabel(C, fontsize=10)
plt.show()


# %%
# imshow
def f(x, y):
    """Imshow."""
    return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)


n = 10
x = np.linspace(-3, 3, 4*n)
y = np.linspace(-3, 3, 3*n)
X, Y = np.meshgrid(x, y)
plt.imshow(f(X, Y), interpolation='nearest', cmap='bone', origin='lower')
plt.colorbar(orientation='vertical')
plt.show()

# %%
# Grids
axes = plt.axes([0.25, 0.25, 0.95, 0.95])
axes.set_xlim(0, 4)
axes.set_ylim(0, 3)
axes.xaxis.set_major_locator(plt.MultipleLocator(1.0))
axes.xaxis.set_minor_locator(plt.MultipleLocator(0.1))
axes.yaxis.set_major_locator(plt.MultipleLocator(1.0))
axes.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
(axes.grid(which='major', color='black', axis='both',
           linewidth=0.75, linestyle='-'))
(axes.grid(which='minor', color='black', axis='both',
           linewidth=0.25, linestyle='-'))

axes.set_xticklabels([])
axes.set_yticklabels([])
plt.show()

# %%
# Multiplots
plt.subplot(2, 1, 1)
plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 4)
plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 5)
plt.xticks([]), plt.yticks([])
plt.subplot(2, 3, 6)
plt.xticks([]), plt.yticks([])
plt.show()


# %%
# 3D plots
fig = plt.figure()
ax = Axes3D(fig)
X = np.arange(-4, 4, 0.25)
Y = np.arange(-4, 4, 0.25)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='hot')
ax.contourf(X, Y, Z, cmap='hot', offset=-2)
ax.set_zlim(-2, 2)
plt.show()
