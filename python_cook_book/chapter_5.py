"""Files and I/O."""

import os
import io
import mmap
import time
import glob
from fnmatch import fnmatch
import os.path

# %%
# Reading and Writing Text Data
with open('somefile.txt', 'wt') as f:  # to write a file
    f.write('text1')

with open('somefile.txt', 'rt') as f:  # to read a file
    data = f.read()


# %%
# Printing with a Different Separator or Line Ending
print('ACME', 50, 91.5)
print('ACME', 50, 91.5, sep=',')
print('ACME', 50, 91.5, sep=',', end='!!\n')

for i in range(5):
    print(i)

for i in range(5):
    print(i, end=' ')

row = ('ACME', 50, 91.5)
print(*row, sep=',')

# %%
# Reading and writing binary data
with open('somefile.bin', 'wb') as f:
    f.write(b'Hello World')

with open('somefile.bin', 'rb') as f:
    data = f.read()
data

t = 'Hello world'  # Text string
t[0]

for c in t:
    print(c)

b = b'Hello World'  # Byte string
b[0]

for c in b:
    print(c)

with open('somefile.bin', 'rb') as f:
    data = f.read(16)
    text = data.decode('utf-8')

with open('somefile.bin', 'wb') as f:
    text = 'Hello World'
    f.write(text.encode('utf-8'))

# %%
# Writting to a file that doesn't exist
if not os.path.exists('somefile'):
    with open('somefile', 'wt') as f:
        f.write('Hello\n')
else:
    print('File already exists')

# %%
# Python I/O operations on a string
s = io.StringIO()
s.write('Hello World\n')
print('This is a test', file=s)
s.getvalue()

s = io.StringIO('Hello\nWorld\n')
s.read(4)
s.read()

s = io.BytesIO()
s.write(b'binary data')
s.getvalue()

# %%
# Reading abd writing compressed data files
# TO read the Data
# with gzip.open('somefile.gz', 'rt') as f
# with bz2.open('somefile.bz2', 'rt') as f
# To write the compressed data
# with gzip.open('somefile.gz', 'wt') as f
# with bz2.open('somefile.bz2', 'wt') as f

# %%
# Memory mapping binary files
size = 1000000
with open('data', 'wb') as f:
    f.seek(size-1)
    f.write(b'x00')


def memory_map(filename, access=mmap.ACCESS_WRITE):
    """Memory map function."""
    size = os.path.getsize(filename)
    fd = os.open(filename, os.O_RDWR)
    return mmap.mmap(fd, size, access=access)


m = memory_map('data')
len(m)
m[0:10]
m[0]
m[0:11] = b'Hello World'
m.close()

with open('data', 'rb') as f:
    print(f.read(11))

with memory_map('data') as m:
    print(len(m))
    print(m[0:10])

m.closed

# %%
# Manipulating pathnames
path = '~/Python/atom_notebooks/misc/python_cook/chapter_5.py'
os.path.basename(path)
os.path.dirname(path)
os.path.expanduser(path)
os.path.split(path)

# %%
# Testing for the Existence of a File
os.path.exists('/etc/passwd')
os.path.exists('/tmp/spam')

# Is a regular file
os.path.isfile('/etc/passwd')

# Is a directory
os.path.isdir('/etc/passwd')

# Is a symbolic link
os.path.islink('/usr/local/bin/python3')

# Get the file linked to
os.path.realpath('/usr/local/bin/python3')

os.path.getsize('/etc/passwd')
os.path.getmtime('/etc/passwd')

time.ctime(os.path.getmtime('/etc/passwd'))

names = os.listdir('.')
names

# Get all regular files
names = [name for name in os.listdir('.')
         if os.path.isfile(os.path.join('.', name))]
names

# Get all dirs
dirnames = [name for name in os.listdir('.')
            if os.path.isdir(os.path.join('.', name))]
dirnames

# startswith() and endswith()
pyfiles = [name for name in os.listdir('.')
           if name.endswith('.py')]
pyfiles

# Filename matching
pyfiles = glob.glob('./*.py')
pyfiles

pyfiles = [name for name in os.listdir('.')
           if fnmatch(name, '*.py')]
pyfiles

pyfiles = glob.glob('*.py')

# Getting file sizes and modification dates
name_sz_date = [(name, os.path.getsize(name), os.path.getmtime(name))
                for name in pyfiles]

for name, size, mtime in name_sz_date:
    print(name, size, mtime)

file_metadata = [(name, os.stat(name)) for name in pyfiles]
for name, meta in file_metadata:
    print(name, meta.st_size, meta.st_mtime)
