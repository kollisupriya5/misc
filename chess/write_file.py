"""Implement chess in python."""

import re
import sys
import os
import uuid


# %%


def write_moves(result):
    """Write into file."""
    input1 = ''
    j = 1
    for i in range(0, len(result), 2):
        if len(result[i:i+2]) == 2:
            x1 = '{}. {} {}\n'.format(j, *result[i:i+2])
        else:
            x1 = '{}. {}'.format(j, *result[i:])
        input1 = input1 + x1
        j += 1
    filename = str(uuid.uuid4())
    print(filename+'.pgn')
    a = os.getcwd()
    path = os.path.join(a, filename+'.pgn')
    with open(path, 'wt') as f:
        f.write(input1)


# %%


class Chess_Board:
    """Write class for chess board."""

    def __init__(self):
        """Create board."""
        self.board = {}

    def create_board(self):
        """Write method to create board."""
        for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
            self.board[(1, i)] = Pawn('white', 'P')
            self.board[(6, i)] = Pawn('black', 'p')
        self.board[(0, 'a')] = Rook('white', 'R')
        self.board[(0, 'h')] = Rook('white', 'R')
        self.board[(7, 'a')] = Rook('black', 'r')
        self.board[(7, 'h')] = Rook('black', 'r')
        self.board[(0, 'b')] = Knight('white', 'N')
        self.board[(0, 'g')] = Knight('white', 'N')
        self.board[(7, 'b')] = Knight('black', 'n')
        self.board[(7, 'g')] = Knight('black', 'n')
        self.board[(0, 'c')] = Bishop('white', 'B')
        self.board[(0, 'f')] = Bishop('white', 'B')
        self.board[(7, 'c')] = Bishop('black', 'b')
        self.board[(7, 'f')] = Bishop('black', 'b')
        self.board[(0, 'e')] = King('white', 'K')
        self.board[(0, 'd')] = Queen('white', 'Q')
        self.board[(7, 'e')] = King('black', 'k')
        self.board[(7, 'd')] = Queen('black', 'q')

    def print_board(self):
        """Print the chess board."""
        print("  a | b | c | d | e | f | g | h |")
        for i in range(7, -1, -1):
            print("-"*32)
            print(i+1, end="|")
            for j in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                item = self.board.get((i, j))
                if item is None:
                    print(" " + ' |', end=" ")
                else:
                    print(str(item.name) + ' |', end=" ")
            print()
        print("-"*32)

    def move(self, col, m, pre_st=None, pre_des=None, c_w=None, c_b=None):
        """Move the key."""
        self.col = col
        self.m = m
        self.pre_st = pre_st
        self.pre_des = pre_des
        self.c_b = c_b
        self.c_w = c_w
        d = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}
        x = int(m[1])-1
        y = d[m[0]]
        x1 = int(m[4])-1
        y1 = d[m[3]]
        st_obj = self.board.get((int(m[1])-1, str(m[0])))
        dest_obj = self.board.get((int(m[4])-1, str(m[3])))
        if st_obj is not None:
            if col == st_obj.color:
                if st_obj.valid_moves(x, y, x1, y1, self.board) is True:
                    if dest_obj is None or st_obj.color != dest_obj.color:
                        self.board[(int(m[4])-1, m[3])] = st_obj
                        self.board[(int(m[1])-1, m[0])] = None
                        coin = self.board.get((int(m[4])-1, str(m[3]))).name
                        cond_1 = coin is 'p' and int(m[4]) == 1
                        cond_2 = coin is 'P' and int(m[4]) == 8
                        if cond_1 or cond_2:
                            ob = self.promotion(col)
                            self.board[(int(m[4])-1, m[3])] = ob
                        if c_b is None:
                            c_b = True
                        else:
                            c_b = c_b
                        if c_w is None:
                            c_w = True
                        else:
                            c_w = True
                        c_w, c_b = self.check_castling(c_w, c_b, coin)
                    else:
                        print("Cannot replace with same color")
                        m = self.valid_input()
                        self.move(col, m)
                elif self.en_passant(st_obj, dest_obj, pre_st, pre_des,
                                     x, y, x1, y1):
                    print("enpassant occured")
                else:
                    print("Invalid move")
                    m = self.valid_input()
                    self.move(col, m)
            else:
                print("Move the {}s".format(col))
                m = self.valid_input()
                self.move(col, m)
        else:
            print("Invalid input")
            m = self.valid_input()
            self.move(col, m)
        return col, st_obj, dest_obj, c_w, c_b

    def valid_input(self):
        """Input validation."""
        m = input("Enter a valid input: ")
        while not re.findall(r'[a-h][1-8]-[a-h][1-8]', m) or len(m) != 5:
            print("Invalid input")
            m = input("Enter a valid input: ")
        return m

    def promotion(self, col):
        """Implement promotion as a valid move."""
        self.col = col
        if col == 'white':
            choices = ['N', 'R', 'B', 'Q']
        elif col == 'black':
            choices = ['n', 'r', 'b', 'q']
        else:
            print("no promotion move")
        choice = input("Enter your choice in {}:  ".format(choices))
        while choice not in choices:
            print("Select a valid choice")
            choice = input("Enter your choice in {}:  ".format(choices))
        if choice in ['N', 'n']:
            return Knight(col, choice)
        elif choice in ['Q', 'q']:
            return Queen(col, choice)
        elif choice in ['R', 'r']:
            return Rook(col, choice)
        else:
            return Bishop(col, choice)

    def en_passant(self, st_obj, dest_obj, pre_st, pre_des, x, y, x1, y1):
        """Implement enpassant as valid move."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if pre_st is not None:
            if st_obj.name is 'P' and pre_st.name is 'p':
                if self.board.get((x1-1, n[y1])) is not None:
                    if self.board.get((x1-1, n[y1])).name is 'p':
                        self.board[(x1, n[y1])] = st_obj
                        self.board[(x1-1, n[y1])] = None
                        self.board[(x, n[y])] = None
                        return self.board
                    return False
                return False
            elif st_obj.name is 'p' and pre_st.name is 'P':
                if self.board.get((x1+1, n[y1])) is not None:
                    if self.board.get((x1+1, n[y1])).name is 'P':
                        self.board[(x1, n[y1])] = st_obj
                        self.board[(x1+1, n[y1])] = None
                        self.board[(x, n[y])] = None
                        return self.board
                    return False
                return False
            else:
                return False
        else:
            return False

    def check_castling(self, c_w, c_b, coin):
        """Check for check_castling."""
        self.c_w = c_w
        self.c_b = c_b
        self.coin = coin
        if coin in ['R', 'K'] and c_w is True:
            c_w = False
        if coin in ['r', 'k'] and c_b is True:
            c_b = False
        return c_w, c_b

    def coins_captured(self, board):
        """Count the no of coins captured."""
        count = 0
        for i in self.board.values():
            if i is not None:
                count = count+1
        return 32-count

# %%


class Stack():
    """Implement stack."""

    def __init__(self):
        """Stack."""
        self.list = []

    def push(self, state, color=None, c_w=None, c_b=None, st=None, dest=None):
        """Push function."""
        self.list.append([state, color, c_w, c_b, st, dest])

    def pop(self):
        """Pop function."""
        self.list.pop()

    def top(self):
        """Get the top element."""
        return self.list[-1]


# %%


class Piece:
    """Write a class for piece."""

    def __init__(self, color, name):
        """Write constructor."""
        self.color = color
        self.name = name


class Pawn(Piece):
    """Write a class for pawn."""

    def __init__(self, color, name):
        """Init for Pawn."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate the moves for pawn."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        a = board.get((x, n[y]))
        b = board.get((x+1, n[y+1]))
        c = board.get((x+1, n[y-1]))
        d = board.get((x-1, n[y+1]))
        e = board.get((x-1, n[y-1]))
        if a.color == 'white':
            if x == 1 and (x1, y1) in [(x+1, y), (x+2, y)]:
                return True
            elif (x1, y1) == (x+1, y):
                return board.get((x+1, n[y])) is None
            elif b is not None and (x1, y1) == (x+1, y+1):
                return b.color != a.color
            elif c is not None and (x1, y1) == (x+1, y-1):
                return c.color != a.color
            else:
                return False
        else:
            if x == 6 and (x1, y1) in [(x-1, y), (x-2, y)]:
                return True
            elif (x1, y1) == (x-1, y):
                return board.get((x-1, n[y])) is None
            elif d is not None and (x1, y1) == (x-1, y+1):
                return d.color != a.color
            elif e is not None and (x1, y1) == (x-1, y-1):
                return a.color != e.color
            else:
                return False


class Rook(Piece):
    """Write a class for rook."""

    def __init__(self, color, name):
        """Init for Rook."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate moves for Rook."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if x == x1:
            res = []
            if y > y1:
                while y != y1+1:
                    y = y-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res

            else:
                while y != y1-1:
                    y = y+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
        elif y == y1:
            res = []
            if x > x1:
                while x != x1+1:
                    x = x-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
            else:
                while x != x1-1:
                    x = x+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
        else:
            print("invalid move")


class Knight(Piece):
    """Write a class for Knight."""

    def __init__(self, color, name):
        """Init for Knight."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate the given moves."""
        moves = ([(x+2, y-1), (x+2, y+1), (x+1, y-2), (x+1, y+2), (x-1, y+2),
                 (x-1, y-2), (x-2, y-1), (x-2, y+1)])
        if (x1, y1) in moves:
            return True


class Bishop(Piece):
    """Write a class for bishop."""

    def __init__(self, color, name):
        """Init for Bishop."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate movmes for Bishop."""
        d1 = x-x1
        d2 = y-y1
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if abs(d1) == abs(d2):
            res = []
            while y != y1 and x != x1:
                if d1 > 0:
                    x = x-1
                else:
                    x = x+1
                if d2 > 0:
                    y = y-1
                else:
                    y = y+1
                res.append(board.get((x, n[y])) is None)
            return False not in res


class King(Piece):
    """Write a class for King."""

    def __init__(self, color, name):
        """Init for King."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate the given moves."""
        moves = ([(x+1, y-1), (x+1, y), (x+1, y+1), (x, y-1), (x, y+1),
                 (x-1, y-1), (x-1, y), (x-1, y+1)])
        if (x1, y1) in moves:
            return True


class Queen(Piece):
    """Write a class for Queen."""

    def __init__(self, color, name):
        """Init for Queen."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, board):
        """Validate the moves for Queen."""
        if x == x1 or y == y1:
            return Rook.valid_moves(self, x, y, x1, y1, board)
        elif abs(x-x1) == abs(y-y1):
            return Bishop.valid_moves(self, x, y, x1, y1, board)
        else:
            print("invalid move")

# %%


def main():
    """Write main function."""
    cb = Chess_Board()
    cb.create_board()
    cb.print_board()
    s = Stack()
    s.push(cb.board.copy())
    pieces = cb.board.values()
    left_pieces = []
    for i in pieces:
        if i is not None:
            left_pieces.append(i.name)
    c = 'white'
    st = None
    dest = None
    c_w = None
    c_b = None
    st = None
    dest = None
    result = []
    while str('K') in left_pieces and str('k') in left_pieces:
        print("1-undo, 2-resign, 3-exit, 4-claim_draw, or enter the move")
        opt = (input("enter the move of '{}'s  ".format(c)))
        while ((not re.findall(r'[a-h][1-8]-[a-h][1-8]', opt) or len(opt) != 5)
               and (len(opt) != 1 or not re.findall(r'[1234]', opt))):
            print("invalid input")
            print("1-undo, 2-resign, 3-exit, 4-claim_draw, or enter the move")
            opt = (input("enter the move of '{}'s  ".format(c)))
        result.append(opt)
        if opt == '1':
            if len(s.list) > 1:
                s.pop()
            else:
                print("No more undos")
            a = s.top()
            cb.board, c, c_w, c_b, st, dest = (a[0], a[1], a[2], a[3],
                                               a[4], a[5])
        elif opt == '2':
            break
        elif opt == '3':
            return result
            # sys.exit()
        elif opt == '4':
            l1 = len(s.list)
            last = {}
            for key, value in s.list[l1-1][0].items():
                if value is not None:
                    if value.name in ['p', 'P']:
                        last[key] = value.name
            res = []
            for i in range(l1-100, l1-1):
                d1 = {}
                for key, value in s.list[i][0].items():
                    if value is not None:
                        if value.name in ['p', 'P']:
                            d1[key] = value.name
                res.append(d1 == last)
            print(res)
            if l1 > 100 and (set(s.list[l1-1][0].values()) ==
                             set(s.list[l1-100][0].values())
                             and False not in res):
                print("game is draw")
                sys.exit()
            else:
                print("You cannot draw the game")
                continue
        else:
            c, st, dest, c_w, c_b = cb.move(c, opt, st, dest, c_w, c_b)
            s.push(cb.board.copy(), c, c_w, c_b, st, dest)
        cb.print_board()
        pieces = cb.board.values()
        left_pieces = []
        for i in pieces:
            if i is not None:
                left_pieces.append(i.name)
        if c == 'white':
            c = 'black'
        else:
            c = 'white'
    print("GAME OVER")
    if str('K') not in left_pieces:
        print("********BLACKS WON*********")
    elif str('k') not in left_pieces:
        print("********WHITES WON*********")
    elif str('k') in left_pieces and str('K') in left_pieces:
        if c == 'white':
            c1 = 'black'
        else:
            c1 = 'white'
        print('{}s won the game'.format(c1))
    return result


x = main()
write_moves(x)
