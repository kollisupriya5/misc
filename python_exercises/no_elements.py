"""Function to find the no of element in the list using recursion."""


def nelements(list):
    """No of element in the list."""
    res = 1
    if len(list) != 0:
        if len(list) == 1:
            return res
        return res+nelements(list[1:])
    return 0


def test_nelements1():
    """Test for no of elements in the list."""
    assert nelements([1, 2, 3, 4, 5, 6, 7]) == 7


def test_nelements2():
    """Test for no of elements in the list."""
    assert nelements(['a', 'b', 'l', 'g', 'h']) == 5


def test_nelements3():
    """Test with empty list."""
    assert nelements([]) == 0


def test_nelements4():
    """Test with single element."""
    assert nelements([1]) == 1


def test_nelements5():
    """Test with single element."""
    assert nelements(['a']) == 1


def test_nelements6():
    """Test for no of elements in the list."""
    assert nelements(['a', 'b', 'c', 'd', 'e']) == 5


def test_nelements7():
    """Test for no of elements in the list."""
    assert nelements([123, 456, '1bc', 'abc', 'cd6']) == 5


def test_nelements8():
    """Test7 for no of elements in the list."""
    assert nelements(['a', 1, 'b', 'c', 2, 3, 'd']) == 7
