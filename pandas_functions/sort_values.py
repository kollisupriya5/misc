"""Practicing sort_values pandas function."""
import numpy as np
import pandas as pd

obj = pd.Series([4, 7, -3, 2])
obj
obj.sort_values()
df = pd.Series([4, np.nan, 7, np.nan, -3, 2])
df.sort_values(na_position='first')

df = (pd.DataFrame({'col1': ['A', 'A', 'B', np.nan, 'D', 'C'],
                   'col2': [2, 1, 9, 8, 7, 4],
                    'col3': [0, 1, 9, 4, 2, 3]}))
df
df.sort_values(by=['col1'])
df.sort_values(by=['col1', 'col2'])
df.sort_values(by=['col1'], ascending=True)
df.sort_values(by=['col1'], ascending=False)
df.sort_values(by=['col1'], ascending=False, na_position='first')
